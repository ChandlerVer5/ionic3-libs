import { ElementRef, Renderer } from '@angular/core';
export declare class HighlightDirective {
    private _domElem;
    private _render;
    private _defaultColor;
    constructor(elem: ElementRef, renderer: Renderer);
    highlightColor: string;
    onMouseEnter(): void;
    onMouseLeave(): void;
}
