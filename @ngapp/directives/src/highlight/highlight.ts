import { Directive, ElementRef, Renderer, HostListener, Input } from '@angular/core';


@Directive({
  selector: '[highlight]'
})
export class HighlightDirective {

  private _domElem: ElementRef;
  private _render: Renderer;
  private _defaultColor = 'red';

  constructor(elem: ElementRef, renderer: Renderer) {
    this._domElem = elem.nativeElement
    this._render = renderer;
  }

  @Input('highlight') highlightColor: string;

  @HostListener('mouseenter')
  onMouseEnter() {
      this._render.setElementStyle(this._domElem, 'backgroundColor', this.highlightColor || this._defaultColor);
  }

  @HostListener('mouseleave')
  onMouseLeave() {
      this._render.setElementStyle(this._domElem, 'backgroundColor', null);
  }

}
