var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Directive, ElementRef, Renderer, HostListener, Input } from '@angular/core';
var HighlightDirective = /** @class */ (function () {
    function HighlightDirective(elem, renderer) {
        this._defaultColor = 'red';
        this._domElem = elem.nativeElement;
        this._render = renderer;
    }
    HighlightDirective.prototype.onMouseEnter = function () {
        this._render.setElementStyle(this._domElem, 'backgroundColor', this.highlightColor || this._defaultColor);
    };
    HighlightDirective.prototype.onMouseLeave = function () {
        this._render.setElementStyle(this._domElem, 'backgroundColor', null);
    };
    __decorate([
        Input('highlight'),
        __metadata("design:type", String)
    ], HighlightDirective.prototype, "highlightColor", void 0);
    __decorate([
        HostListener('mouseenter'),
        __metadata("design:type", Function),
        __metadata("design:paramtypes", []),
        __metadata("design:returntype", void 0)
    ], HighlightDirective.prototype, "onMouseEnter", null);
    __decorate([
        HostListener('mouseleave'),
        __metadata("design:type", Function),
        __metadata("design:paramtypes", []),
        __metadata("design:returntype", void 0)
    ], HighlightDirective.prototype, "onMouseLeave", null);
    HighlightDirective = __decorate([
        Directive({
            selector: '[highlight]'
        }),
        __metadata("design:paramtypes", [typeof (_a = typeof ElementRef !== "undefined" && ElementRef) === "function" && _a || Object, typeof (_b = typeof Renderer !== "undefined" && Renderer) === "function" && _b || Object])
    ], HighlightDirective);
    return HighlightDirective;
    var _a, _b;
}());
export { HighlightDirective };
//# sourceMappingURL=highlight.js.map