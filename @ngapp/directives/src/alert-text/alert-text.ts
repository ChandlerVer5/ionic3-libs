import { Directive, HostListener, ElementRef, Input } from '@angular/core';

@Directive({
  selector: '[alert-text]' // Attribute selector
})
export class AlertTextDirective {
  private _domElem: ElementRef;

  constructor(elem: ElementRef) {
    this._domElem = elem;
  }

  @Input('alert-text') inputText:any;

  @HostListener('click')
  onclick() {
      let text = this.inputText || this._domElem.nativeElement.innerText;
      alert(text);
      console.log(text);
      console.log(this._domElem);
  }

}
