import { Directive, HostListener, Input, ElementRef, Output, EventEmitter } from '@angular/core';

/**
 * 发送短信倒计时
 */
@Directive({
    selector: '[sms-timing]' // Attribute selector
})
export class SmsTimingDirective {

    /**
     * 默认发送短信倒计时文字
     */
    @Input() tip: string = '@s';

    @Input('load-time') time: number = 60;

    @Output('sms-timing') smsTiming: EventEmitter<Function> = new EventEmitter();

    timer: any;

    /**
     * 发送消息
     */
    @HostListener('click', ['$event'])
    sendSms(e: Event) {
        e && e.stopPropagation();
        const that = this;
        if (this.timer) {
            return;
        }
        this.smsTiming.emit(that.loading);
    }

    /**
     * 加载
     */
    public loading = () => {
        const that = this,
            text = that.ele.nativeElement.innerHTML;
        let timeNum: number = that.time;
        that.ele.nativeElement.innerHTML = (this.tip.replace('@', timeNum + ''))
        that.timer = setInterval(() => {
            if (timeNum <= 0) {
                clearInterval(that.timer);
                that.timer = null;
                that.ele.nativeElement.innerHTML = text;
                return;
            }
            timeNum--;
            that.ele.nativeElement.innerHTML = (this.tip.replace('@', timeNum + ''))
        }, 1000);
        return () => {
            clearInterval(that.timer);
            that.timer = null;
            that.ele.nativeElement.innerHTML = text;
        }
    }

    constructor(public ele: ElementRef) {

    }

}
