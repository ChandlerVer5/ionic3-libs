import { ElementRef, EventEmitter } from '@angular/core';
/**
 * 发送短信倒计时
 */
export declare class SmsTimingDirective {
    ele: ElementRef;
    /**
     * 默认发送短信倒计时文字
     */
    tip: string;
    time: number;
    smsTiming: EventEmitter<Function>;
    timer: any;
    /**
     * 发送消息
     */
    sendSms(e: Event): void;
    /**
     * 加载
     */
    loading: () => () => void;
    constructor(ele: ElementRef);
}
