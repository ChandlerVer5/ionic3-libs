import { NgModule } from '@angular/core';
import { HighlightDirective } from './highlight/highlight';
import { AlertTextDirective } from './alert-text/alert-text';
import { SmsTimingDirective } from './sms-timing/sms-timing';
@NgModule({
	declarations: [HighlightDirective, AlertTextDirective, SmsTimingDirective],
	imports: [],
	exports: [HighlightDirective, AlertTextDirective, SmsTimingDirective]
})
export class DirectivesModule { }