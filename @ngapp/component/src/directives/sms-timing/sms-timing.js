var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Directive, HostListener, Input, ElementRef, Output, EventEmitter } from '@angular/core';
/**
 * 发送短信倒计时
 */
var SmsTimingDirective = (function () {
    function SmsTimingDirective(ele) {
        var _this = this;
        this.ele = ele;
        /**
         * 默认发送短信倒计时文字
         */
        this.tip = '@s';
        this.time = 60;
        this.smsTiming = new EventEmitter();
        /**
         * 加载
         */
        this.loading = function () {
            var that = _this, text = that.ele.nativeElement.innerHTML;
            var timeNum = that.time;
            that.ele.nativeElement.innerHTML = (_this.tip.replace('@', timeNum + ''));
            that.timer = setInterval(function () {
                if (timeNum <= 0) {
                    clearInterval(that.timer);
                    that.timer = null;
                    that.ele.nativeElement.innerHTML = text;
                    return;
                }
                timeNum--;
                that.ele.nativeElement.innerHTML = (_this.tip.replace('@', timeNum + ''));
            }, 1000);
            return function () {
                clearInterval(that.timer);
                that.timer = null;
                that.ele.nativeElement.innerHTML = text;
            };
        };
    }
    /**
     * 发送消息
     */
    SmsTimingDirective.prototype.sendSms = function (e) {
        e && e.stopPropagation();
        var that = this;
        if (this.timer) {
            return;
        }
        this.smsTiming.emit(that.loading);
    };
    __decorate([
        Input(),
        __metadata("design:type", String)
    ], SmsTimingDirective.prototype, "tip", void 0);
    __decorate([
        Input('load-time'),
        __metadata("design:type", Number)
    ], SmsTimingDirective.prototype, "time", void 0);
    __decorate([
        Output('sms-timing'),
        __metadata("design:type", EventEmitter)
    ], SmsTimingDirective.prototype, "smsTiming", void 0);
    __decorate([
        HostListener('click', ['$event']),
        __metadata("design:type", Function),
        __metadata("design:paramtypes", [Event]),
        __metadata("design:returntype", void 0)
    ], SmsTimingDirective.prototype, "sendSms", null);
    SmsTimingDirective = __decorate([
        Directive({
            selector: '[sms-timing]' // Attribute selector
        }),
        __metadata("design:paramtypes", [ElementRef])
    ], SmsTimingDirective);
    return SmsTimingDirective;
}());
export { SmsTimingDirective };
//# sourceMappingURL=sms-timing.js.map