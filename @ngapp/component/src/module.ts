import { NgModule } from '@angular/core';

import { SmsTimingDirective } from './directives';

@NgModule({
    declarations: [
        SmsTimingDirective
    ],
    exports: [
        SmsTimingDirective
    ]
})
export class ComponentModule { }