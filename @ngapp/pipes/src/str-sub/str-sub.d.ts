import { PipeTransform } from "@angular/core";
export declare class StrSubPipe implements PipeTransform {
    transform(value: string, num: number): string;
}
