import { Pipe, PipeTransform } from "@angular/core";

@Pipe({
  name: "strSub"
})
export class StrSubPipe implements PipeTransform {
  transform(value: string,num:number) {
    let newValue = "";
    if (value) {
      if( value.length > num){
        newValue = value.substring(0,num) + "...";
      }else{
        newValue = value;
      }
    }
    return newValue;
  }
}
