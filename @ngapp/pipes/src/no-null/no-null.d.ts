import { PipeTransform } from "@angular/core";
export declare class NoNull implements PipeTransform {
    transform(value: any): any;
}
