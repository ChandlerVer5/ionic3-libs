import { Pipe, PipeTransform } from "@angular/core";

@Pipe({
    name: "noNull"
})
export class NoNull implements PipeTransform {
    transform(value: any) {
        value = value || '';
        if (value == 'null') {
            return '';
        }
        return value;
    }
}