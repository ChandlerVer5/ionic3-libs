import { NgModule } from '@angular/core';
import { StrSubPipe } from './str-sub/str-sub';
import { NoNull } from './no-null/no-null';
@NgModule({
	declarations: [StrSubPipe, NoNull],
	imports: [],
	exports: [StrSubPipe, NoNull]
})
export class PipesModule { }