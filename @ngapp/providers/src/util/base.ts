/**
 * 原生调用JS方法,返回数据类型
 */
export class XviewBase {
    code: string; //0为成功;-1为失败
    message: string;
    data: any; //返回数据(字符串、JSON对象、字符串数组、JSON对象数组)
}

export interface AlertOptions {
    title?: string;
    message: string;
    fn?: Function;
}
