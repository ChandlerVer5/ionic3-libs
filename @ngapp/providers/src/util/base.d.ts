/**
 * 原生调用JS方法,返回数据类型
 */
export declare class XviewBase {
    code: string;
    message: string;
    data: any;
}
export interface AlertOptions {
    title?: string;
    message: string;
    fn?: Function;
}
