/**
 * xview2.0 JS SDK V2.0.1
 * 1、所有请求参数类型均为JSON字符串
 * 2、所有回调方法返回数据均为JSON对象,除个别特殊说明的方法
 * 3、回调数据中,code对应的0/-1, 0为成功, -1为失败!
 */
export declare class XviewServiceProvider {
    constructor();
    /**
     * 一、融云即时通讯
     */
    Rongcloud: {
        xviewRongCloudLogin: (jsonStr: string) => Promise<any>;
        xviewRongCloudLogout: (jsonStr: string) => Promise<any>;
        xviewCurrentSessionList: (jsonStr: string) => Promise<any>;
        xviewPushSessionView: (jsonStr: string) => Promise<any>;
        xviewDeleteSession: (jsonStr: string) => Promise<any>;
        xviewPop: (jsonStr: string) => Promise<any>;
    };
    /**
     * 融云登录,融云个人账户token需后端调用融云接口获取
     * @param jsonStr {userId:"用户ID",username:"用户名称",portrait:"用户头像的URL",token:"融云个人账户token",callback:"回调方法名"}
     */
    private xviewRongCloudLogin(jsonStr);
    /**
     * 融云退出登录
     * @param jsonStr {callback:'回调方法名'}
     */
    private xviewRongCloudLogout(jsonStr);
    /**
     * 获取当前会话列表
     * @param jsonStr {callback:'回调方法名'}
     */
    private xviewCurrentSessionList(jsonStr);
    /**
     * 跳转到会话界面,用户和群组需调用融云后端接口创建
     * @param jsonStr {"sessionId":"userId不是token","sessionType":"会话类型","title":"会话标题","callback":"回调方法名","path":"点击设置按钮，跳转的路径(选填参数)"}
     */
    private xviewPushSessionView(jsonStr);
    /**
     * 删除会话列表中某一个会话
     * @param jsonStr {sessionId:'会话id',sessionType:'会话类型:P2P-单聊,Team-群聊',callback:'回调方法名'}
     */
    private xviewDeleteSession(jsonStr);
    /**
     * pop到之前原生界面
     * @param jsonStr {number:'1',callback:'回调方法名'}
     */
    private xviewPop(jsonStr);
    /**
     * 接收到消息, 返回会话列表消息给js
     * 注：原生调取js方法
     * xviewRefreshSessionList(data)
     */
    /**
     * 融云账号被退出登录时调用
     * 注：原生调取js方法
     * xviewLoginKicked(data)
     */
    /**
     * 二、极光推送
     */
    Jpush: {
        xviewSetJPushAlias: (jsonStr: string) => Promise<any>;
        xviewCancelJPushAlias: () => Promise<any>;
    };
    /**
     * 极光推送设置alias别名
     * @param jsonStr {alias:'别名'}
     */
    private xviewSetJPushAlias(jsonStr);
    /**
     * 取消极光推送推送设置的别名,当前客户端
     * @param 无参数
     */
    private xviewCancelJPushAlias();
    /**
     * 接收到极光推送消息, 用户并没有点击:
     * xviewListenJPush(data);
     *
     * 接收到极光推送消息, 用户点击:
     * xviewReceiveJPush(data)
     */
    /**
     * 三、支付分享登录
     */
    Paysharelogin: {
        payShareLogin: (jsonStr: string) => Promise<any>;
        xviewIsWXAppInstalled: (jsonStr: string) => Promise<any>;
        xviewPushQQ: (jsonStr: string) => Promise<any>;
    };
    /**
     * @param jsonStr {type:"类型",data:"请求数据",callback:"回调方法名"}
     * 具体请求参数说明：
     *
     * 1、微信登录:{callback:"回调方法名",type:"weixinLogin",data:{wxappid:"微信APPID",wxappsecret:"微信APP秘钥"}}
     * 2、微博登录:{callback:"回调方法名",type:"weiboLogin",data:{wbsecret:'',wbappkey:'',wbredirecturl:''}}
     * 3、qq登录:{callback:"回调方法名",type:"qqLogin",data:{qqappid:''}}
     *
     * 4、支付宝支付:{callback:"回调方法名",type:"aliPay",data:{alipaydata:''}}
     * 5、微信支付:{callback:"回调方法名",type:"weixinPay",data:{"appid":"","noncestr":"","package":"Sign=WXPay","partnerid":"","prepayid":"","sign":"","timestamp":""}}}}
     * 6、连连支付:{callback:"回调方法名",type:"lianlianVerifyPay/lianlianQuickPay - 认证支付/快捷支付",data:{dt_order:''}}
     *
     * 7、分享网页链接给微信好友:{callback:"回调方法名",type:"weixinShare",data:{}}
     * 8、分享网页链接到微信朋友圈:{callback:"回调方法名",type:"weixinCircleShare",data:{}}
     * 9、分享网页链接给QQ好友:{callback:"回调方法名",type:"qqShare",data:{}}
     * 10、分享网页链接到QQ空间:{callback:"回调方法名",type:"qqZoneShare",data:{}}
     * 11、分享网页链接到微博:{callback:"回调方法名",type:"weiboShare",data:{}}
     * 分享data数据格式：{sharetype:"web",title:"分享标题",description:"分享描述",thumburl:"缩略图网址，小于20kb",shareurl:"分享网页地址"}
     */
    private xviewLoginPayShare(jsonStr);
    /**
     * 是否安装了微信客户端
     * @param jsonStr {"callback": "回调方法名"}
     */
    private xviewIsWXAppInstalled(jsonStr);
    /**
     * 跳转到qq中与某一qq号的聊天界面
     * @param jsonStr {"callback": "回调方法名",qq:'qq号'}
     */
    private xviewPushQQ(jsonStr);
    Media: {
        xviewSaveImageToGallery: (jsonStr: string) => Promise<any>;
        xviewScanBarcode: (jsonStr: string) => Promise<any>;
        xviewRecordVideo: (jsonStr: string) => Promise<any>;
        xviewUploadVideo: (jsonStr: string) => Promise<any>;
        xviewRecordAudio: (jsonStr: string) => Promise<any>;
        xviewPlayAudio: (jsonStr: string) => Promise<any>;
        xviewSelectVideoUpload: (jsonStr: string) => Promise<any>;
        xviewUploadImage: (jsonStr: string) => Promise<any>;
        xviewUploadFile: (jsonStr: string) => Promise<any>;
    };
    /**
     * 加水印保存图片,直接保存到相册,不返回新图片数据
     * @param jsonStr {"url":"图片网址","text":"水印文字","callback":"回调方法"};
     */
    private xviewSaveImageToGallery(jsonStr);
    /**
     * 扫码二维码
     * @param jsonStr {callback:'回调方法名'}
     */
    private xviewScanBarcode(jsonStr);
    /**
     * 录制视频
     * @param jsonStr {callback:'回调方法名',time:'录制时间3-30秒'}
     */
    private xviewRecordVideo(jsonStr);
    /**
     * 上传视频
     * @param jsonStr {"video":"视频地址","image":"截图地址","url":"接口","callback":"回调方法"}
     *
     */
    private xviewUploadVideo(jsonStr);
    /**
     * 录制音频
     * @param jsonStr {url:'接口地址',callback:'回调方法'}
     */
    private xviewRecordAudio(jsonStr);
    /**
     * 播放音频
     * @param jsonStr {"state":"play/stop/pause/resume","url":"音频路径","callback":"回调方法"}
     */
    private xviewPlayAudio(jsonStr);
    /**
     * 选取相册视频上传
     * @param jsonStr {url:'接口地址',callback:'回调方法'}
     */
    private xviewSelectVideoUpload(jsonStr);
    /**
     * 多图选择,上传图片数组base64
     * @param jsonStr {callback:'回调方法',maxnumber:'最多选择多少张图片'}
     */
    private xviewUploadImage(jsonStr);
    /**
     * 上传文件
     * @param jsonStr {callback:'回调方法',url:'接口地址接收二进制流',file_path:['文件路径'],otherparamname:'其它参数'}
     */
    private xviewUploadFile(jsonStr);
    /**
     * 五、蓝牙模块
     */
    Bluetooth: {
        xviewLinkBLE: (jsonStr: string) => Promise<any>;
        xviewSendCommandToBLE: (jsonStr: string) => Promise<any>;
    };
    /**
     * 连接蓝牙设备
     */
    private xviewLinkBLE(jsonStr);
    /**
     * 蓝牙数据交互
     */
    private xviewSendCommandToBLE(jsonStr);
    /**
     * 六、Other
     */
    Other: {
        xviewSetStatusBar: (jsonStr: string) => Promise<any>;
        xviewTextToClipboard: (jsonStr: string) => Promise<any>;
        xviewPushWeb: (jsonStr: string) => Promise<any>;
        xviewLocation: (jsonStr: string) => Promise<any>;
        xviewCleanCache: () => Promise<any>;
        xviewCallPhone: (jsonStr: string) => Promise<any>;
        xviewGetAddressBook: (jsonStr: string) => Promise<any>;
        xviewAppVersion: (jsonStr: string) => Promise<any>;
        xviewGetLocaleLanguage: (jsonStr: string) => Promise<any>;
    };
    /**
     * 修改状态栏字体颜色,目前仅IOS可用
     * @param jsonStr {color:'black或white'}
     */
    private xviewSetStatusBar(jsonStr);
    /**
     * 拷贝字符串到粘贴板
     * @param jsonStr {"text":""}
     */
    private xviewTextToClipboard(jsonStr);
    /**
     * 跳转手机浏览器网页或者App内打开一个新网页,导航颜色仅IOS可用
     * @param jsonStr {"type":"Web或App","url":"网址","title":"App内导航栏标题","color":"App内导航栏颜色，十六进制，必须为8位字符串，如0xffffff"}
     */
    private xviewPushWeb(jsonStr);
    /**
     * 获取位置信息
     * @param jsonStr {callback:'回调方法名'}
     */
    private xviewLocation(jsonStr);
    /**
     * 清除缓存
     * @param 无参数
     */
    private xviewCleanCache();
    /**
     * 拨打电话
     * @param jsonStr {tel:'电话号码'}
     */
    private xviewCallPhone(jsonStr);
    /**
     * 获取通讯录列表
     * @param jsonStr {callback:"回调方法名"}
     */
    private xviewGetAddressBook(jsonStr);
    /**
     * 获取app当前版本号
     * @param jsonStr {callback:"回调方法名"}
     */
    private xviewAppVersion(jsonStr);
    /**
     * 获取手机系统语言
     */
    private xviewGetLocaleLanguage(jsonStr);
    /**
     * 安卓特有方法
     */
    AndroidMethod: {
        xviewExitApp: (jsonStr: string) => Promise<any>;
    };
    /**
     * 安卓退出或最小化APP
     * @param jsonStr { type:""} type为finish杀死APP,否则最小化APP
     */
    private xviewExitApp(jsonStr);
    /**
     * 七、微信小程序
     */
    wechatMini: {
        startMiniProgram: (data: {
            userName: string;
            path: string;
            miniProgramType: 0 | 1 | 2;
            wxappid: string;
        }) => Promise<any>;
        shareMiniProgram: (data: {
            webpageUrl: string;
            userName: string;
            path: string;
            title: string;
            description: string;
            thumbBmp: string;
            wxappid: string;
        }) => Promise<any>;
    };
    /**
     * 调起微信小程序
     * @param data
     * { userName = "gh开头的 填小程序原始id",path:拉起小程序页面的可带参路径，不填默认拉起小程序首页,miniProgramType:'0 正式版 1 开发版  2 体验版',wxappid:''}
     */
    private startMiniProgram(data);
    /**
     * 分享微信小程序
     * @param data
     * {webpageUrl:小程序链接,userName:小程序原始id,path:小程序 page路径(可拼接成url带参数 要与小程序商定好),title:分享小程序的title,description:分享小程序描述信息,thumbBmp : 小程序缩略图 小于 128 Kb,wxappid:''}
     */
    private shareMiniProgram(data);
    /**
     * 调用xview有参方法
     * @param jsonStr 请求JSON字符串参数
     * @param xviewFunctionName 方法名称
     */
    private static exeXviewNative(jsonStr, xviewFunctionName);
    /**
     * 调用xview无参方法
     * @param xviewFunctionName 方法名称
     */
    private static exeXviewNativeNoPara(xviewFunctionName);
    /**
     * 调起xview方法，没有传递回调方法时，异步回调封装20180709
     * @param xviewFunctionName
     * @param jsonStr
     */
    private static exeXview(xviewFunctionName, jsonStr?);
}
