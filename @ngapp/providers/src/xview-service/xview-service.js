var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Injectable } from "@angular/core";
/**
 * xview2.0 JS SDK V2.0.1
 * 1、所有请求参数类型均为JSON字符串
 * 2、所有回调方法返回数据均为JSON对象,除个别特殊说明的方法
 * 3、回调数据中,code对应的0/-1, 0为成功, -1为失败!
 */
var XviewServiceProvider = (function () {
    function XviewServiceProvider() {
        /**
         * 一、融云即时通讯
         */
        this.Rongcloud = {
            xviewRongCloudLogin: this.xviewRongCloudLogin,
            xviewRongCloudLogout: this.xviewRongCloudLogout,
            xviewCurrentSessionList: this.xviewCurrentSessionList,
            xviewPushSessionView: this.xviewPushSessionView,
            xviewDeleteSession: this.xviewDeleteSession,
            xviewPop: this.xviewPop //pop到之前原生界面
        };
        /**
         * 接收到消息, 返回会话列表消息给js
         * 注：原生调取js方法
         * xviewRefreshSessionList(data)
         */
        /**
         * 融云账号被退出登录时调用
         * 注：原生调取js方法
         * xviewLoginKicked(data)
         */
        /**
         * 二、极光推送
         */
        this.Jpush = {
            xviewSetJPushAlias: this.xviewSetJPushAlias,
            xviewCancelJPushAlias: this.xviewCancelJPushAlias //取消别名设置
        };
        /**
         * 接收到极光推送消息, 用户并没有点击:
         * xviewListenJPush(data);
         *
         * 接收到极光推送消息, 用户点击:
         * xviewReceiveJPush(data)
         */
        /**
         * 三、支付分享登录
         */
        this.Paysharelogin = {
            payShareLogin: this.xviewLoginPayShare,
            xviewIsWXAppInstalled: this.xviewIsWXAppInstalled,
            xviewPushQQ: this.xviewPushQQ //跳转QQ聊天
        };
        /*
        四、音视频
      */
        this.Media = {
            xviewSaveImageToGallery: this.xviewSaveImageToGallery,
            xviewScanBarcode: this.xviewScanBarcode,
            xviewRecordVideo: this.xviewRecordVideo,
            xviewUploadVideo: this.xviewUploadVideo,
            xviewRecordAudio: this.xviewRecordAudio,
            xviewPlayAudio: this.xviewPlayAudio,
            xviewSelectVideoUpload: this.xviewSelectVideoUpload,
            xviewUploadImage: this.xviewUploadImage,
            xviewUploadFile: this.xviewUploadFile
        };
        /**
         * 五、蓝牙模块
         */
        this.Bluetooth = {
            xviewLinkBLE: this.xviewLinkBLE,
            xviewSendCommandToBLE: this.xviewSendCommandToBLE
        };
        /**
         * 六、Other
         */
        this.Other = {
            xviewSetStatusBar: this.xviewSetStatusBar,
            xviewTextToClipboard: this.xviewTextToClipboard,
            xviewPushWeb: this.xviewPushWeb,
            xviewLocation: this.xviewLocation,
            xviewCleanCache: this.xviewCleanCache,
            xviewCallPhone: this.xviewCallPhone,
            xviewGetAddressBook: this.xviewGetAddressBook,
            xviewAppVersion: this.xviewAppVersion,
            xviewGetLocaleLanguage: this.xviewGetLocaleLanguage
        };
        /**
         * 安卓特有方法
         */
        this.AndroidMethod = {
            xviewExitApp: this.xviewExitApp
        };
        /**
         * 七、微信小程序
         */
        this.wechatMini = {
            startMiniProgram: this.startMiniProgram,
            shareMiniProgram: this.shareMiniProgram
        };
    }
    XviewServiceProvider_1 = XviewServiceProvider;
    /**
     * 融云登录,融云个人账户token需后端调用融云接口获取
     * @param jsonStr {userId:"用户ID",username:"用户名称",portrait:"用户头像的URL",token:"融云个人账户token",callback:"回调方法名"}
     */
    XviewServiceProvider.prototype.xviewRongCloudLogin = function (jsonStr) {
        return XviewServiceProvider_1.exeXviewNative(jsonStr, "xviewRongCloudLogin");
    };
    /**
     * 融云退出登录
     * @param jsonStr {callback:'回调方法名'}
     */
    XviewServiceProvider.prototype.xviewRongCloudLogout = function (jsonStr) {
        return XviewServiceProvider_1.exeXviewNative(jsonStr, "xviewRongCloudLogout");
    };
    /**
     * 获取当前会话列表
     * @param jsonStr {callback:'回调方法名'}
     */
    XviewServiceProvider.prototype.xviewCurrentSessionList = function (jsonStr) {
        return XviewServiceProvider_1.exeXviewNative(jsonStr, "xviewCurrentSessionList");
    };
    /**
     * 跳转到会话界面,用户和群组需调用融云后端接口创建
     * @param jsonStr {"sessionId":"userId不是token","sessionType":"会话类型","title":"会话标题","callback":"回调方法名","path":"点击设置按钮，跳转的路径(选填参数)"}
     */
    XviewServiceProvider.prototype.xviewPushSessionView = function (jsonStr) {
        return XviewServiceProvider_1.exeXviewNative(jsonStr, "xviewPushSessionView");
    };
    /**
     * 删除会话列表中某一个会话
     * @param jsonStr {sessionId:'会话id',sessionType:'会话类型:P2P-单聊,Team-群聊',callback:'回调方法名'}
     */
    XviewServiceProvider.prototype.xviewDeleteSession = function (jsonStr) {
        return XviewServiceProvider_1.exeXviewNative(jsonStr, "xviewDeleteSession");
    };
    /**
     * pop到之前原生界面
     * @param jsonStr {number:'1',callback:'回调方法名'}
     */
    XviewServiceProvider.prototype.xviewPop = function (jsonStr) {
        return XviewServiceProvider_1.exeXviewNative(jsonStr, "xviewPop");
    };
    /**
     * 极光推送设置alias别名
     * @param jsonStr {alias:'别名'}
     */
    XviewServiceProvider.prototype.xviewSetJPushAlias = function (jsonStr) {
        return XviewServiceProvider_1.exeXviewNative(jsonStr, "xviewSetJPushAlias");
    };
    /**
     * 取消极光推送推送设置的别名,当前客户端
     * @param 无参数
     */
    XviewServiceProvider.prototype.xviewCancelJPushAlias = function () {
        return XviewServiceProvider_1.exeXviewNativeNoPara("xviewCancelJPushAlias");
    };
    /**
     * @param jsonStr {type:"类型",data:"请求数据",callback:"回调方法名"}
     * 具体请求参数说明：
     *
     * 1、微信登录:{callback:"回调方法名",type:"weixinLogin",data:{wxappid:"微信APPID",wxappsecret:"微信APP秘钥"}}
     * 2、微博登录:{callback:"回调方法名",type:"weiboLogin",data:{wbsecret:'',wbappkey:'',wbredirecturl:''}}
     * 3、qq登录:{callback:"回调方法名",type:"qqLogin",data:{qqappid:''}}
     *
     * 4、支付宝支付:{callback:"回调方法名",type:"aliPay",data:{alipaydata:''}}
     * 5、微信支付:{callback:"回调方法名",type:"weixinPay",data:{"appid":"","noncestr":"","package":"Sign=WXPay","partnerid":"","prepayid":"","sign":"","timestamp":""}}}}
     * 6、连连支付:{callback:"回调方法名",type:"lianlianVerifyPay/lianlianQuickPay - 认证支付/快捷支付",data:{dt_order:''}}
     *
     * 7、分享网页链接给微信好友:{callback:"回调方法名",type:"weixinShare",data:{}}
     * 8、分享网页链接到微信朋友圈:{callback:"回调方法名",type:"weixinCircleShare",data:{}}
     * 9、分享网页链接给QQ好友:{callback:"回调方法名",type:"qqShare",data:{}}
     * 10、分享网页链接到QQ空间:{callback:"回调方法名",type:"qqZoneShare",data:{}}
     * 11、分享网页链接到微博:{callback:"回调方法名",type:"weiboShare",data:{}}
     * 分享data数据格式：{sharetype:"web",title:"分享标题",description:"分享描述",thumburl:"缩略图网址，小于20kb",shareurl:"分享网页地址"}
     */
    XviewServiceProvider.prototype.xviewLoginPayShare = function (jsonStr) {
        return XviewServiceProvider_1.exeXviewNative(jsonStr, "xviewLoginPayShare");
    };
    /**
     * 是否安装了微信客户端
     * @param jsonStr {"callback": "回调方法名"}
     */
    XviewServiceProvider.prototype.xviewIsWXAppInstalled = function (jsonStr) {
        return XviewServiceProvider_1.exeXviewNative(jsonStr, "xviewIsWXAppInstalled");
    };
    /**
     * 跳转到qq中与某一qq号的聊天界面
     * @param jsonStr {"callback": "回调方法名",qq:'qq号'}
     */
    XviewServiceProvider.prototype.xviewPushQQ = function (jsonStr) {
        return XviewServiceProvider_1.exeXviewNative(jsonStr, "xviewPushQQ");
    };
    /**
     * 加水印保存图片,直接保存到相册,不返回新图片数据
     * @param jsonStr {"url":"图片网址","text":"水印文字","callback":"回调方法"};
     */
    XviewServiceProvider.prototype.xviewSaveImageToGallery = function (jsonStr) {
        return XviewServiceProvider_1.exeXviewNative(jsonStr, "xviewSaveImageToGallery");
    };
    /**
     * 扫码二维码
     * @param jsonStr {callback:'回调方法名'}
     */
    XviewServiceProvider.prototype.xviewScanBarcode = function (jsonStr) {
        return XviewServiceProvider_1.exeXviewNative(jsonStr, "xviewScanBarcode");
    };
    /**
     * 录制视频
     * @param jsonStr {callback:'回调方法名',time:'录制时间3-30秒'}
     */
    XviewServiceProvider.prototype.xviewRecordVideo = function (jsonStr) {
        return XviewServiceProvider_1.exeXviewNative(jsonStr, "xviewRecordVideo");
    };
    /**
     * 上传视频
     * @param jsonStr {"video":"视频地址","image":"截图地址","url":"接口","callback":"回调方法"}
     *
     */
    XviewServiceProvider.prototype.xviewUploadVideo = function (jsonStr) {
        return XviewServiceProvider_1.exeXviewNative(jsonStr, "xviewUploadVideo");
    };
    /**
     * 录制音频
     * @param jsonStr {url:'接口地址',callback:'回调方法'}
     */
    XviewServiceProvider.prototype.xviewRecordAudio = function (jsonStr) {
        return XviewServiceProvider_1.exeXviewNative(jsonStr, "xviewRecordAudio");
    };
    /**
     * 播放音频
     * @param jsonStr {"state":"play/stop/pause/resume","url":"音频路径","callback":"回调方法"}
     */
    XviewServiceProvider.prototype.xviewPlayAudio = function (jsonStr) {
        return XviewServiceProvider_1.exeXviewNative(jsonStr, "xviewPlayAudio");
    };
    /**
     * 选取相册视频上传
     * @param jsonStr {url:'接口地址',callback:'回调方法'}
     */
    XviewServiceProvider.prototype.xviewSelectVideoUpload = function (jsonStr) {
        return XviewServiceProvider_1.exeXviewNative(jsonStr, "xviewSelectVideoUpload");
    };
    /**
     * 多图选择,上传图片数组base64
     * @param jsonStr {callback:'回调方法',maxnumber:'最多选择多少张图片'}
     */
    XviewServiceProvider.prototype.xviewUploadImage = function (jsonStr) {
        return XviewServiceProvider_1.exeXviewNative(jsonStr, "xviewUploadImage");
    };
    /**
     * 上传文件
     * @param jsonStr {callback:'回调方法',url:'接口地址接收二进制流',file_path:['文件路径'],otherparamname:'其它参数'}
     */
    XviewServiceProvider.prototype.xviewUploadFile = function (jsonStr) {
        return XviewServiceProvider_1.exeXviewNative(jsonStr, "xviewUploadFile");
    };
    /**
     * 连接蓝牙设备
     */
    XviewServiceProvider.prototype.xviewLinkBLE = function (jsonStr) {
        return XviewServiceProvider_1.exeXviewNative(jsonStr, "xviewLinkBLE");
    };
    /**
     * 蓝牙数据交互
     */
    XviewServiceProvider.prototype.xviewSendCommandToBLE = function (jsonStr) {
        return XviewServiceProvider_1.exeXviewNative(jsonStr, "xviewSendCommandToBLE");
    };
    /**
     * 修改状态栏字体颜色,目前仅IOS可用
     * @param jsonStr {color:'black或white'}
     */
    XviewServiceProvider.prototype.xviewSetStatusBar = function (jsonStr) {
        return XviewServiceProvider_1.exeXviewNative(jsonStr, "xviewSetStatusBar");
    };
    /**
     * 拷贝字符串到粘贴板
     * @param jsonStr {"text":""}
     */
    XviewServiceProvider.prototype.xviewTextToClipboard = function (jsonStr) {
        return XviewServiceProvider_1.exeXviewNative(jsonStr, "xviewTextToClipboard");
    };
    /**
     * 跳转手机浏览器网页或者App内打开一个新网页,导航颜色仅IOS可用
     * @param jsonStr {"type":"Web或App","url":"网址","title":"App内导航栏标题","color":"App内导航栏颜色，十六进制，必须为8位字符串，如0xffffff"}
     */
    XviewServiceProvider.prototype.xviewPushWeb = function (jsonStr) {
        return XviewServiceProvider_1.exeXviewNative(jsonStr, "xviewPushWeb");
    };
    /**
     * 获取位置信息
     * @param jsonStr {callback:'回调方法名'}
     */
    XviewServiceProvider.prototype.xviewLocation = function (jsonStr) {
        return XviewServiceProvider_1.exeXviewNative(jsonStr, "xviewLocation");
    };
    /**
     * 清除缓存
     * @param 无参数
     */
    XviewServiceProvider.prototype.xviewCleanCache = function () {
        return XviewServiceProvider_1.exeXviewNativeNoPara("xviewCleanCache");
    };
    /**
     * 拨打电话
     * @param jsonStr {tel:'电话号码'}
     */
    XviewServiceProvider.prototype.xviewCallPhone = function (jsonStr) {
        return XviewServiceProvider_1.exeXviewNative(jsonStr, "xviewCallPhone");
    };
    /**
     * 获取通讯录列表
     * @param jsonStr {callback:"回调方法名"}
     */
    XviewServiceProvider.prototype.xviewGetAddressBook = function (jsonStr) {
        return XviewServiceProvider_1.exeXviewNative(jsonStr, "xviewGetAddressBook");
    };
    /**
     * 获取app当前版本号
     * @param jsonStr {callback:"回调方法名"}
     */
    XviewServiceProvider.prototype.xviewAppVersion = function (jsonStr) {
        return XviewServiceProvider_1.exeXviewNative(jsonStr, "xviewAppVersion");
    };
    /**
     * 获取手机系统语言
     */
    XviewServiceProvider.prototype.xviewGetLocaleLanguage = function (jsonStr) {
        return XviewServiceProvider_1.exeXviewNative(jsonStr, "xviewGetLocaleLanguage");
    };
    /**
     * 安卓退出或最小化APP
     * @param jsonStr { type:""} type为finish杀死APP,否则最小化APP
     */
    XviewServiceProvider.prototype.xviewExitApp = function (jsonStr) {
        return XviewServiceProvider_1.exeXviewNative(jsonStr, "xviewExitApp");
    };
    /**
     * 调起微信小程序
     * @param data
     * { userName = "gh开头的 填小程序原始id",path:拉起小程序页面的可带参路径，不填默认拉起小程序首页,miniProgramType:'0 正式版 1 开发版  2 体验版',wxappid:''}
     */
    XviewServiceProvider.prototype.startMiniProgram = function (data) {
        var json = {
            type: "startMiniProgram",
            callback: "",
            data: data
        };
        return XviewServiceProvider_1.exeXviewNative(JSON.stringify(json), "xviewLoginPayShare");
    };
    /**
     * 分享微信小程序
     * @param data
     * {webpageUrl:小程序链接,userName:小程序原始id,path:小程序 page路径(可拼接成url带参数 要与小程序商定好),title:分享小程序的title,description:分享小程序描述信息,thumbBmp : 小程序缩略图 小于 128 Kb,wxappid:''}
     */
    XviewServiceProvider.prototype.shareMiniProgram = function (data) {
        var json = {
            type: "shareMiniProgram",
            callback: "",
            data: data
        };
        return XviewServiceProvider_1.exeXviewNative(JSON.stringify(json), "xviewLoginPayShare");
    };
    /**
     * 调用xview有参方法
     * @param jsonStr 请求JSON字符串参数
     * @param xviewFunctionName 方法名称
     */
    XviewServiceProvider.exeXviewNative = function (jsonStr, xviewFunctionName) {
        return this.exeXview(xviewFunctionName, jsonStr);
    };
    /**
     * 调用xview无参方法
     * @param xviewFunctionName 方法名称
     */
    XviewServiceProvider.exeXviewNativeNoPara = function (xviewFunctionName) {
        return this.exeXview(xviewFunctionName);
    };
    /**
     * 调起xview方法，没有传递回调方法时，异步回调封装20180709
     * @param xviewFunctionName
     * @param jsonStr
     */
    XviewServiceProvider.exeXview = function (xviewFunctionName, jsonStr) {
        var promise = new Promise(function () { });
        if (jsonStr) {
            var json_1 = JSON.parse(jsonStr || "{}");
            //传递callback方法名时不走promise回调！
            if (!json_1["callback"]) {
                json_1["callback"] = xviewFunctionName + "CallbackFun";
                jsonStr = JSON.stringify(json_1);
                promise = new Promise(function (resolve, reject) {
                    try {
                        window[json_1["callback"]] = function (xviewData) {
                            resolve(xviewData);
                        };
                    }
                    catch (error) {
                        reject(error);
                    }
                });
            }
        }
        if (window["xview"]) {
            if (jsonStr) {
                window["xview"][xviewFunctionName](jsonStr);
            }
            else {
                window["xview"][xviewFunctionName]();
            }
        }
        else if (window["webkit"]) {
            if (jsonStr) {
                window["webkit"].messageHandlers[xviewFunctionName].postMessage(jsonStr);
            }
            else {
                window["webkit"].messageHandlers[xviewFunctionName].postMessage();
            }
        }
        else {
            console.error("xview异常：无xview或webkit。");
            console.error("xview方法：" + xviewFunctionName);
            console.error("xview参数：" + jsonStr);
        }
        return promise;
    };
    XviewServiceProvider = XviewServiceProvider_1 = __decorate([
        Injectable(),
        __metadata("design:paramtypes", [])
    ], XviewServiceProvider);
    return XviewServiceProvider;
    var XviewServiceProvider_1;
}());
export { XviewServiceProvider };
//# sourceMappingURL=xview-service.js.map