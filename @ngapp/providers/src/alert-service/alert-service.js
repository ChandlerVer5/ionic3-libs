var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { AlertController, ToastController, LoadingController } from "ionic-angular";
import { Injectable } from "@angular/core";
/**
 * 弹出框方法
 */
var AlertServiceProvider = (function () {
    function AlertServiceProvider(loadingCtrl, alertCtrl, toastCtrl) {
        this.loadingCtrl = loadingCtrl;
        this.alertCtrl = alertCtrl;
        this.toastCtrl = toastCtrl;
    }
    AlertServiceProvider.prototype.alert = function (tip) {
        if (typeof tip == "string") {
            tip = { message: tip };
        }
        var alert = this.alertCtrl.create({
            title: tip.title || "提示",
            message: tip.message,
            buttons: [
                {
                    text: "确定",
                    handler: function () {
                        if (tip.fn) {
                            return tip.fn();
                        }
                        return true;
                    }
                }
            ]
        });
        alert.present();
    };
    /**
     * 打开确认取消弹出框
     */
    AlertServiceProvider.prototype.confirm = function (message, okFun, cancelFun) {
        var alert = this.alertCtrl.create({
            title: "提示信息",
            message: message || "确认操作吗?",
            buttons: [
                {
                    text: "取消",
                    role: "cancel",
                    handler: function () {
                        cancelFun && cancelFun();
                    }
                },
                {
                    text: "确认",
                    handler: function () {
                        okFun && okFun();
                    }
                }
            ]
        });
        alert.present();
    };
    /**
     * 显示Toast提示
     * @param position bottom,top,middle
     */
    AlertServiceProvider.prototype.showToast = function (message, position, showCloseButton, dismissHandler) {
        var _this = this;
        if (message === void 0) { message = "提示信息"; }
        if (position === void 0) { position = "bottom"; }
        if (showCloseButton === void 0) { showCloseButton = false; }
        if (dismissHandler === void 0) { dismissHandler = this.destroyToast; }
        if (this.toastObj) {
            this.toastObj.setMessage(message);
            this.toastObj.setPosition(position);
        }
        else {
            this.toastObj = this.toastCtrl.create({
                message: message,
                position: position,
                duration: 3000,
                showCloseButton: showCloseButton,
                closeButtonText: "Ok"
            });
            this.toastObj.present();
        }
        this.toastObj &&
            this.toastObj.onDidDismiss(function () {
                dismissHandler;
                _this.destroyToast();
            });
    };
    /**
     * 吐司水印提示框
     * @param message
     */
    AlertServiceProvider.prototype.tipToast = function (message, duration) {
        if (message === void 0) { message = "提示信息"; }
        if (duration === void 0) { duration = 2000; }
        if (this.toastObj) {
            this.toastObj.setMessage(message);
            this.toastObj.setDuration(duration);
        }
        else {
            this.toastObj = this.toastCtrl.create({
                message: message,
                position: "middle",
                duration: duration,
                cssClass: "toast-tip"
            });
            this.toastObj.present();
        }
        this.toastObj && this.toastObj.onDidDismiss(this.destroyToast);
    };
    /**
     * 显示loading小提示
     * @param message
     * @param duration
     */
    AlertServiceProvider.prototype.tipLoading = function (message, duration) {
        if (message === void 0) { message = "提示信息"; }
        if (duration === void 0) { duration = 2000; }
        if (this.loadingObj) {
            this.loadingObj.setContent(message);
            this.loadingObj.setDuration(duration);
        }
        else {
            this.loadingObj = this.loadingCtrl.create({
                spinner: "hide",
                content: message,
                duration: duration,
                cssClass: "loading-tip"
            });
            this.loadingObj.present();
        }
        this.loadingObj && this.loadingObj.onDidDismiss(this.destroyLoading);
    };
    /**
     * 显示loading动画
     * @param message
     * @param duration
     * @param spinner ios ios-small bubbles	circles	crescent dots
     */
    AlertServiceProvider.prototype.showLoading = function (message, duration, spinner) {
        if (message === void 0) { message = "玩命加载中..."; }
        if (duration === void 0) { duration = 2000; }
        if (spinner === void 0) { spinner = "crescent"; }
        if (this.loadingObj) {
            this.loadingObj.setContent(message);
            this.loadingObj.setDuration(duration);
            this.loadingObj.setSpinner(spinner);
        }
        else {
            this.loadingObj = this.loadingCtrl.create({
                spinner: spinner,
                content: message,
                duration: duration,
                cssClass: "loading-tip"
            });
            this.loadingObj.present();
        }
        this.loadingObj && this.loadingObj.onDidDismiss(this.destroyLoading);
    };
    AlertServiceProvider.prototype.destroyToast = function () {
        this.toastObj = null;
    };
    AlertServiceProvider.prototype.destroyLoading = function () {
        this.loadingObj = null;
    };
    AlertServiceProvider = __decorate([
        Injectable(),
        __metadata("design:paramtypes", [LoadingController,
            AlertController,
            ToastController])
    ], AlertServiceProvider);
    return AlertServiceProvider;
}());
export { AlertServiceProvider };
//# sourceMappingURL=alert-service.js.map