import { AlertController, ToastController, LoadingController, Loading, Toast } from "ionic-angular";
import { AlertOptions } from "../util/base";
/**
 * 弹出框方法
 */
export declare class AlertServiceProvider {
    loadingCtrl: LoadingController;
    private alertCtrl;
    private toastCtrl;
    toastObj: Toast;
    loadingObj: Loading;
    constructor(loadingCtrl: LoadingController, alertCtrl: AlertController, toastCtrl: ToastController);
    /**
     * 打开弹出框
     */
    alert(tip: string): void;
    alert(tip: AlertOptions): void;
    /**
     * 打开确认取消弹出框
     */
    confirm(message: any, okFun: any, cancelFun?: any): void;
    /**
     * 显示Toast提示
     * @param position bottom,top,middle
     */
    showToast(message?: string, position?: any, showCloseButton?: boolean, dismissHandler?: (data: any, role: string) => void): void;
    /**
     * 吐司水印提示框
     * @param message
     */
    tipToast(message?: string, duration?: number): void;
    /**
     * 显示loading小提示
     * @param message
     * @param duration
     */
    tipLoading(message?: string, duration?: number): void;
    /**
     * 显示loading动画
     * @param message
     * @param duration
     * @param spinner ios ios-small bubbles	circles	crescent dots
     */
    showLoading(message?: string, duration?: number, spinner?: string): void;
    private destroyToast();
    private destroyLoading();
}
