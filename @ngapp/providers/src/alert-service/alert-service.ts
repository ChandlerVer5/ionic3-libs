import {
    AlertController,
    Alert,
    ToastController,
    LoadingController,
    Loading,
    Toast
} from "ionic-angular";
import { Injectable } from "@angular/core";
import { AlertOptions } from "../util/base";

/**
 * 弹出框方法
 */
@Injectable()
export class AlertServiceProvider {
    toastObj: Toast;
    loadingObj: Loading;

    constructor(
        public loadingCtrl: LoadingController,
        private alertCtrl: AlertController,
        private toastCtrl: ToastController
    ) {}

    /**
     * 打开弹出框
     */
    alert(tip: string): void;

    alert(tip: AlertOptions): void;

    alert(tip: any): void {
        if (typeof tip == "string") {
            tip = { message: tip };
        }

        let alert: Alert = this.alertCtrl.create({
            title: tip.title || "提示",
            message: tip.message,
            buttons: [
                {
                    text: "确定",
                    handler: () => {
                        if (tip.fn) {
                            return tip.fn();
                        }
                        return true;
                    }
                }
            ]
        });
        alert.present();
    }

    /**
     * 打开确认取消弹出框
     */
    confirm(message: any, okFun: any, cancelFun?: any) {
        let alert = this.alertCtrl.create({
            title: "提示信息",
            message: message || "确认操作吗?",
            buttons: [
                {
                    text: "取消",
                    role: "cancel",
                    handler: () => {
                        cancelFun && cancelFun();
                    }
                },
                {
                    text: "确认",
                    handler: () => {
                        okFun && okFun();
                    }
                }
            ]
        });
        alert.present();
    }

    /**
     * 显示Toast提示
     * @param position bottom,top,middle
     */
    showToast(
        message: string = "提示信息",
        position: any = "bottom",
        showCloseButton: boolean = false,
        dismissHandler: (data: any, role: string) => void = this.destroyToast
    ) {
        if (this.toastObj) {
            this.toastObj.setMessage(message);
            this.toastObj.setPosition(position);
        } else {
            this.toastObj = this.toastCtrl.create({
                message: message,
                position: position,
                duration: 3000,
                showCloseButton: showCloseButton,
                closeButtonText: "Ok"
            });
            this.toastObj.present();
        }
        this.toastObj &&
            this.toastObj.onDidDismiss(() => {
                dismissHandler;
                this.destroyToast();
            });
    }

    /**
     * 吐司水印提示框
     * @param message
     */
    tipToast(message: string = "提示信息", duration: number = 2000) {
        if (this.toastObj) {
            this.toastObj.setMessage(message);
            this.toastObj.setDuration(duration);
        } else {
            this.toastObj = this.toastCtrl.create({
                message: message,
                position: "middle",
                duration: duration,
                cssClass: "toast-tip"
            });
            this.toastObj.present();
        }
        this.toastObj && this.toastObj.onDidDismiss(this.destroyToast);
    }

    /**
     * 显示loading小提示
     * @param message
     * @param duration
     */
    tipLoading(message: string = "提示信息", duration: number = 2000) {
        if (this.loadingObj) {
            this.loadingObj.setContent(message);
            this.loadingObj.setDuration(duration);
        } else {
            this.loadingObj = this.loadingCtrl.create({
                spinner: "hide",
                content: message,
                duration: duration,
                cssClass: "loading-tip"
            });
            this.loadingObj.present();
        }
        this.loadingObj && this.loadingObj.onDidDismiss(this.destroyLoading);
    }

    /**
     * 显示loading动画
     * @param message
     * @param duration
     * @param spinner ios ios-small bubbles	circles	crescent dots
     */
    showLoading(
        message: string = "玩命加载中...",
        duration: number = 2000,
        spinner: string = "crescent"
    ) {
        if (this.loadingObj) {
            this.loadingObj.setContent(message);
            this.loadingObj.setDuration(duration);
            this.loadingObj.setSpinner(spinner);
        } else {
            this.loadingObj = this.loadingCtrl.create({
                spinner: spinner,
                content: message,
                duration: duration,
                cssClass: "loading-tip"
            });
            this.loadingObj.present();
        }
        this.loadingObj && this.loadingObj.onDidDismiss(this.destroyLoading);
    }

    private destroyToast() {
        this.toastObj = null;
    }

    private destroyLoading() {
        this.loadingObj = null;
    }
}
