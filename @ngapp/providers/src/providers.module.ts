import { NgModule } from "@angular/core";
import { XviewServiceProvider } from "./xview-service/xview-service";
import { AlertServiceProvider } from "./alert-service/alert-service";

@NgModule({
    providers: [XviewServiceProvider, AlertServiceProvider]
})
export class ProvidersModule {}
