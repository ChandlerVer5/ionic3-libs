import { XViewConfig } from './../xview-config/XViewConfig';
import { XView } from '../XView';
import { AppVersion, Result, AddressLocation, NavigationType } from '../../constants/index';
import { Subject, Observable } from 'rxjs';
import { Injectable } from '@angular/core';

@Injectable()
export class PhoneUtilsService extends XView {

    private xviewObserveAppState: Subject<Result<{
        state: 'active' | 'background'
    }>> = new Subject<Result<{
        state: 'active' | 'background'
    }>>();


    constructor(config: XViewConfig) {
        super(config.appConfig);

        const that = this;
        /**
         * 监听点击推送时间
         */
        window['observeAppState'] = function (param: Result<{ state: 'active' | 'background' }>) {
            that.xviewObserveAppState.next(param);
        }
    }

    /**
     * 清除缓存
     */
    public xviewCleanCache(): void {
        this.xview('xviewCleanCache');
    }

    /**
     * 调用手机号码
     * @param phone  手机号码
     */
    public xviewCallPhone(tel: string): void {
        this.xviewNative('xviewCallPhone', { tel });
    }

    /**
     * 修改字体颜色
     * @param parameter  "white/black"
     */
    public xviewSetStatusBar(color: string): void {
        this.xviewNative('xviewSetStatusBar', { color })
    }

    /**
     * 地理定位，获取当前位置
     */
    public xviewLocation(): Promise<Result<AddressLocation>> {
        return this.xviewNative('xviewLocation', {});
    }

    /**
     * 持续定位
     */
    public xviewOpenKeepLocation(json: {
        locationUrl: string;// 接口地址
        timer?: string; // 持续时间 单位min 不传默认一直开启
        refreshTime: string; // 时间间隔 单位 ms
        timeOut: string; //超时 
    } | { [key: string]: string }): Promise<Result<any>> {
        return this.xviewNative('xviewOpenKeepLocation', json);
    }

    /**
     * 关闭持续定位
     */
    public xviewStopKeepLocation() {
        this.xviewNative('xviewStopKeepLocation');
    }

    private inFirstTimeToEnter: boolean = false;

    /**
     * 回调APP 进去前台或者后台
     * active - 后台进入前台 ;background - 前台进入后台
     */
    public xviewObserveAppState$() {
        if (!this.inFirstTimeToEnter) {
            this.xviewNative('xviewObserveAppState');
            this.inFirstTimeToEnter = true;
        }
        return this.xviewObserveAppState.asObservable();
    }


    /**
     * 复制到剪切板
     * @param param 
     */
    public xviewTextToClipboard(param: { text: string }) {
        this.xviewNative('xviewTextToClipboard', param);
    }

    /**
     * 开车导航
     * @param data 
     */
    public xviewNavigationDriving(data: {
        startLongitude: number | string,
        startLatitude: number | string,
        endLongitude: number | string,
        endLatitude: number | string
    }): Promise<Result<any>> {
        return this.xviewNavigation(data, 'driving');
    }

    /**
     * 骑车导航
     * @param data 
     */
    public xviewNavigationRiding(data: {
        startLongitude: number | string,
        startLatitude: number | string,
        endLongitude: number | string,
        endLatitude: number | string
    }): Promise<Result<any>> {
        return this.xviewNavigation(data, 'riding');
    }

    /**
     * 不行导航
     * @param data 
     */
    public xviewNavigationWalking(data: {
        startLongitude: number | string,
        startLatitude: number | string,
        endLongitude: number | string,
        endLatitude: number | string
    }): Promise<Result<any>> {
        return this.xviewNavigation(data, 'walking');
    }


    /**
     * 导航 
     * @param data 
     * @param type driving/riding/walking
     */
    public xviewNavigation(data: {
        startLongitude: number | string,
        startLatitude: number | string,
        endLongitude: number | string,
        endLatitude: number | string
    }, type: NavigationType = 'driving'): Promise<Result<any>> {
        return this.xviewNative('xviewNavigation', { data: { ...data, ...(this._config.xunFei) }, type });
    }


    /**
     * 获取版本信息 
     */
    public getAppVersion(): Promise<Result<AppVersion>> {
        return this.xviewNative('xviewAppVersion', {});
    }

    /**
     * 跳转到web view
     * @param param 
     */
    public pushWebView(param: {
        "url": string;
        "type": 'Web' | 'App';
        "title": string;
        "color": string;
    }) {
        this.xviewNative('xviewPushWeb', { ...param })
    }

    /**
     * 退出app
     * @param type  finish 结束APP 否则最小化 默认 finish
     */
    public xviewExitApp(type: string = 'finish') {
        this.xviewNative('xviewExitApp', { type });
    }

    /**
     * 获取电话本
     */
    public xviewGetAddressBook(): Promise<Result<
        Array<{
            name: string;
            phone: string;
        }>>> {
        return this.xviewNative('xviewGetAddressBook', {});
    }


    /**
     * 监听安卓物理返回键
     * @param appCtrl 
     * @param callback  return true 不返回  false 返回 
     */
    public registerBackEvent(appCtrl, callback?: Function) {
        window['xviewWebBack'] = () => {
            if (callback) {
                if (callback()) {
                    return;
                }
            }

            let promise = appCtrl.goBack();
            if (promise) {
                promise.then(info => {
                    if (!info) {
                        let a = appCtrl._appRoot._getPortal();
                        a.pop();
                    }
                });
            } else {
                this.xviewExitApp();
            }
        }
    }
}