import { Result } from './../../constants/constants';
import { XView } from './../XView';
import { XViewConfig } from './../xview-config/XViewConfig';
import { Injectable } from "@angular/core";



/**
 * 蓝牙
 */

@Injectable()
export class BlueToothService extends XView {

    constructor(config: XViewConfig) {
        super(config.appConfig);
    }

    /**
     * 连接蓝牙
     */
    xviewLinkBLE(json: {
        "deviceName": string;//"MC_CUP_ ",
        "deviceUUID": string;//"FFF0",
        "notifyUUID": string;//"FFF1",
        "writeUUID": string;//"FFF1",
        "bytes": string | Array<string>;//"11&6&1&1&1&1&1&1&1&1&1&1&1&1&1", //命令字节间通过&拼接
    }): Promise<Result<{ result: string }>> {
        if (json.bytes instanceof Array) {
            json.bytes = json.bytes.join("&");
        }
        return this.xviewNative('xviewLinkBLE', json);
    }

    /**
     * xviewSendCommandToBLE 发送命令
     */
    xviewSendCommandToBLE(json: {
        "bytes": string | Array<string>; "1&1&1&1", //命令字节间通过&拼接
    }): Promise<Result<{ result: string }>> {
        if (json.bytes instanceof Array) {
            json.bytes = json.bytes.join("&");
        }
        return this.xviewNative('xviewSendCommandToBLE', json);
    }
}