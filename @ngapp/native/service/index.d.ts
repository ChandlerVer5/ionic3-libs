export { XViewConfig } from './xview-config/XViewConfig';
export { PhoneUtilsService } from './phone-service/phoneUtils-service';
export { PayShareLogin } from './pay-share-login/pay-share-login';
export { MediaService } from './media-service/media-service';
export { JPushService } from './jpush-service/jpush-service';
export { RongCloudService } from './rong-cloud/rong-cloud';
export { BlueToothService } from './blue-tooth/blue-tooth';
export { XView } from './XView';
