/**
 * Native 交互方法
 */
var XView = (function () {
    function XView(config) {
        this._config = config;
    }
    XView.prototype.xviewNative = function (methodName, param) {
        var that = this;
        if (param) {
            !param.callback && (param.callback = methodName + 'callback');
            var promise = new Promise(function (resolve, reject) {
                try {
                    window[param.callback] = function (result) {
                        var _result = result;
                        if (typeof _result == 'object') {
                            _result.success = (_result.code == '0');
                        }
                        resolve(_result);
                        // delete window[param.callback];
                    };
                }
                catch (e) {
                    resolve({ code: '-2', message: "\u8C03\u7528 window." + param.callback + " \u53D1\u751F\u5F02\u5E38,\u9519\u8BEF\u4FE1\u606F" + e, data: {}, success: false });
                    console.log('====================================');
                    console.log(e.message);
                    console.log('====================================');
                }
            });
            if (that._config.isDebug) {
                console.log('====================================');
                console.log("methodName " + methodName);
                console.log(param);
                console.log('====================================');
            }
            that.xview(methodName, param);
            return promise;
        }
        else {
            that.xview(methodName);
        }
    };
    XView.prototype.xview = function (methodName, param) {
        var _this = this;
        // const isIOS = true;
        try {
            if (window['webkit']) {
                window['webkit'].messageHandlers[methodName].postMessage(JSON.stringify(param || {}));
            }
            else {
                if (param) {
                    window['xview'][methodName](JSON.stringify(param));
                }
                else {
                    window['xview'][methodName]();
                }
            }
        }
        catch (e) {
            console.warn("Not Found Method " + methodName);
            console.warn(e.message);
            if (param && this._config.isDebug && param.callback) {
                setTimeout(function () {
                    if (_this._config.callback && _this._config.callback[methodName]) {
                        //自定义回调
                        window[methodName + 'callback'](_this._config.callback[methodName]);
                    }
                    else {
                        window[methodName + 'callback']({
                            code: '-2',
                            message: 'debug 调试',
                            data: {
                                result: e.message
                            }
                        });
                    }
                }, 500);
            }
        }
    };
    return XView;
}());
export { XView };
//# sourceMappingURL=XView.js.map