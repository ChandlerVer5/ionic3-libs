var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { XViewConfig } from './../xview-config/XViewConfig';
import { XView } from '../XView';
import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
/**
 * 极光推送
 */
var JPushService = (function (_super) {
    __extends(JPushService, _super);
    function JPushService(config) {
        var _this = _super.call(this, config.appConfig) || this;
        _this.xviewListenJPush = new Subject();
        _this.xviewReceiveJPush = new Subject();
        var that = _this;
        /**
         * 监听点击推送时间
         */
        window['xviewListenJPush'] = function (param) {
            that.xviewListenJPush.next(param);
        };
        /**
         * 监听推送并点击事件
         */
        window['xviewReceiveJPush'] = function (param) {
            that.xviewReceiveJPush.next(param);
        };
        return _this;
    }
    /**
     * 订阅: 接收到极光推送消息, 用户并没有点击,触发
     */
    JPushService.prototype.xviewListenJPush$ = function () {
        return this.xviewListenJPush.asObservable();
    };
    // /**
    //  * 更新新值到store
    //  * 
    //  * @param param 
    //  */
    // public setListenJPush(param: any): void {
    //     window['xviewListenJPush'](param);
    // }
    /**
     * 接收到极光推送消息, 用户点击触发.
     */
    JPushService.prototype.xviewReceiveJPush$ = function () {
        return this.xviewReceiveJPush.asObservable();
    };
    // /**
    //  * 更新新值到store
    //  * @param param 
    //  */
    // public setReceiveJPush(param: any): void {
    //     window['xviewReceiveJPush'](param);;
    // }
    /**
     * 设置别名
     * @param alias
     */
    JPushService.prototype.jiGuangTuiSong = function (alias) {
        if (typeof alias == 'object') {
            this.xviewNative('xviewSetJPushAlias', alias);
        }
        else {
            this.xviewNative('xviewSetJPushAlias', { alias: alias });
        }
    };
    /**
     * 清除别名
     * @param alias
     */
    JPushService.prototype.cancelJPushAlias = function () {
        this.xviewNative('xviewCancelJPushAlias');
    };
    JPushService = __decorate([
        Injectable(),
        __metadata("design:paramtypes", [XViewConfig])
    ], JPushService);
    return JPushService;
}(XView));
export { JPushService };
//# sourceMappingURL=jpush-service.js.map