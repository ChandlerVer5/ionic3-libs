import { Result, JPushParams } from './../../constants/constants';
import { XViewConfig } from './../xview-config/XViewConfig';
import { XView } from '../XView';
import { Observable } from 'rxjs';
/**
 * 极光推送
 */
export declare class JPushService extends XView {
    private xviewListenJPush;
    private xviewReceiveJPush;
    constructor(config: XViewConfig);
    /**
     * 订阅: 接收到极光推送消息, 用户并没有点击,触发
     */
    xviewListenJPush$(): Observable<Result<JPushParams>>;
    /**
     * 接收到极光推送消息, 用户点击触发.
     */
    xviewReceiveJPush$(): Observable<Result<JPushParams>>;
    /**
     * 设置别名
     * @param alias
     */
    jiGuangTuiSong(alias: object | string): void;
    /**
     * 清除别名
     * @param alias
     */
    cancelJPushAlias(): void;
}
