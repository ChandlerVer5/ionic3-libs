var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { XViewConfig } from './../xview-config/XViewConfig';
import { XView } from './../XView';
/**
 * 融云
 */
var RongCloudService = (function (_super) {
    __extends(RongCloudService, _super);
    function RongCloudService(config) {
        var _this = _super.call(this, config.appConfig) || this;
        _this.xviewRefreshSessionList = new Subject();
        _this.xviewLoginKicked = new Subject();
        /**
         * 储存融云登陆信息
         */
        _this.rongCloud = null;
        var that = _this;
        /**
         * 原生调用js方法, 接收到消息, 返回会话列表消息给js
         */
        window['xviewRefreshSessionList'] = function (param) {
            that.xviewRefreshSessionList.next(param);
        };
        /**
         * 融云账号被退出登录时, 原生调用js方法
         */
        window['xviewLoginKicked'] = function (param) {
            that.rongCloud = null;
            that.xviewLoginKicked.next(param);
        };
        return _this;
    }
    /**
     * 原生调用js方法, 接收到消息, 返回会话列表消息给js
     */
    RongCloudService.prototype.xviewRefreshSessionList$ = function () {
        return this.xviewRefreshSessionList.asObservable();
    };
    /**
     * 融云账号被退出登录时, 原生调用js方法
     */
    RongCloudService.prototype.xviewLoginKicked$ = function () {
        return this.xviewLoginKicked.asObservable();
    };
    /**
     * 手动触发 刷新会话列表
     */
    RongCloudService.prototype.setXviewRefreshSessionList = function (param) {
        window['xviewRefreshSessionList']({ code: '0', message: '手动触发刷新方法', data: param });
    };
    /**
     * 融云登陆
     *
     * @param params 参数
     * @param isValidate  是否登陆校验
     */
    RongCloudService.prototype.xviewRongCloudLogin = function (params, isValidate) {
        var _this = this;
        if (isValidate === void 0) { isValidate = true; }
        if (isValidate && this.rongCloud && params.token == this.rongCloud.token) {
            console.log('====================================');
            console.log("融云已登陆,无需重复调用");
            console.log({ code: '-2', message: '重复调用', data: { result: '' } });
            console.log('====================================');
            return Promise.resolve({ code: '-2', message: '重复调用', data: { result: '' } });
        }
        return this.xviewNative('xviewRongCloudLogin', params).then(function (result) {
            if (result.code == '0') {
                _this.rongCloud = params;
            }
            return Promise.resolve(result);
        }, function (error) {
            return Promise.resolve(error);
        });
    };
    /**
     * 退出融云
     */
    RongCloudService.prototype.xviewRongCloudLogout = function () {
        this.rongCloud = null;
        return this.xviewNative('xviewRongCloudLogout', {}).then(function (result) {
            return Promise.resolve(result);
        }, function (error) {
            return Promise.resolve(error);
        });
    };
    /**
     * 获取会话列表
     */
    RongCloudService.prototype.xviewCurrentSessionList = function () {
        return this.xviewNative('xviewCurrentSessionList', {});
    };
    /**
     * 跳到原生界面
     * @param params
     */
    RongCloudService.prototype.xviewPushSessionView = function (params) {
        if (!params.title) {
            params.title = params.nickname;
        }
        return this.xviewNative('xviewPushSessionView', params);
    };
    /**
     * 删除会话
     * @param params
     */
    RongCloudService.prototype.xviewDeleteSession = function (params) {
        return this.xviewNative('xviewDeleteSession', params);
    };
    /**
     * pop到之前原生界面
     * @param params default { number: '1' }
     */
    RongCloudService.prototype.xviewPop = function (params) {
        if (params === void 0) { params = { number: '1' }; }
        return this.xviewNative('xviewPop', params);
    };
    RongCloudService = __decorate([
        Injectable(),
        __metadata("design:paramtypes", [XViewConfig])
    ], RongCloudService);
    return RongCloudService;
}(XView));
export { RongCloudService };
//# sourceMappingURL=rong-cloud.js.map