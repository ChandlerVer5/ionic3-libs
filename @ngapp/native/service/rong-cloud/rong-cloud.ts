import { Injectable } from '@angular/core';
import { Subject, Observable } from 'rxjs';
import { XViewConfig } from './../xview-config/XViewConfig';
import { RongCloud, RongCloudCurrentSessionList, Result, RongCloudSession, RongCloudSessionPane } from './../../constants';
import { XView } from './../XView';


/**
 * 融云
 */
@Injectable()
export class RongCloudService extends XView {

    private xviewRefreshSessionList: Subject<any> = new Subject<any>();

    private xviewLoginKicked: Subject<any> = new Subject<any>();

    /**
     * 储存融云登陆信息
     */
    private rongCloud: RongCloud = null;

    constructor(config: XViewConfig) {
        super(config.appConfig);


        const that = this;
        /**
         * 原生调用js方法, 接收到消息, 返回会话列表消息给js
         */
        window['xviewRefreshSessionList'] = function (param) {
            that.xviewRefreshSessionList.next(param);
        }


        /**
         * 融云账号被退出登录时, 原生调用js方法
         */
        window['xviewLoginKicked'] = function (param) {
            that.rongCloud = null;
            that.xviewLoginKicked.next(param);
        }
    }

    /**
     * 原生调用js方法, 接收到消息, 返回会话列表消息给js
     */
    public xviewRefreshSessionList$(): Observable<Result<RongCloudCurrentSessionList>> {
        return this.xviewRefreshSessionList.asObservable();
    }

    /**
     * 融云账号被退出登录时, 原生调用js方法
     */
    public xviewLoginKicked$(): Observable<Result<{ result: string }>> {
        return this.xviewLoginKicked.asObservable();
    }

    /**
     * 手动触发 刷新会话列表
     */
    public setXviewRefreshSessionList(param: RongCloudCurrentSessionList) {
        window['xviewRefreshSessionList']({ code: '0', message: '手动触发刷新方法', data: param });
    }



    /**
     * 融云登陆
     * 
     * @param params 参数
     * @param isValidate  是否登陆校验
     */
    xviewRongCloudLogin(params: RongCloud, isValidate: boolean = true): Promise<Result<{ result: string }>> {
        if (isValidate && this.rongCloud && params.token == this.rongCloud.token) {
            console.log('====================================');
            console.log("融云已登陆,无需重复调用");
            console.log({ code: '-2', message: '重复调用', data: { result: '' } });
            console.log('====================================');
            return Promise.resolve({ code: '-2', message: '重复调用', data: { result: '' } } as Result<{ result: string }>);
        }
        return this.xviewNative<{ result: string }>('xviewRongCloudLogin', params).then(result => {
            if (result.code == '0') {
                this.rongCloud = params;
            }
            return Promise.resolve(result);
        }, error => {
            return Promise.resolve(error);
        });
    }

    /**
     * 退出融云
     */
    xviewRongCloudLogout(): Promise<Result<{ result: string }>> {
        this.rongCloud = null;
        return this.xviewNative<{ result: string }>('xviewRongCloudLogout', {}).then(result => {
            return Promise.resolve(result);
        }, error => {
            return Promise.resolve(error);
        });
    }


    /**
     * 获取会话列表
     */
    xviewCurrentSessionList(): Promise<Result<RongCloudCurrentSessionList>> {
        return this.xviewNative('xviewCurrentSessionList', {});
    }

    /**
     * 跳到原生界面
     * @param params 
     */
    xviewPushSessionView(params: RongCloudSessionPane): Promise<Result<{ result: string }>> {
        if (!params.title) {
            params.title = params.nickname;
        }
        return this.xviewNative('xviewPushSessionView', params);
    }

    /**
     * 删除会话
     * @param params 
     */
    xviewDeleteSession(params: RongCloudSession): Promise<Result<{ result: string }>> {
        return this.xviewNative('xviewDeleteSession', params);
    }

    /**
     * pop到之前原生界面
     * @param params default { number: '1' }
     */
    xviewPop(params: { number: string } = { number: '1' }): Promise<Result<{ result: string }>> {
        return this.xviewNative('xviewPop', params);
        
    }

}