var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { XViewConfig } from './../xview-config/XViewConfig';
import { XView } from '../XView';
import { Injectable } from '@angular/core';
var MediaService = (function (_super) {
    __extends(MediaService, _super);
    function MediaService(config) {
        return _super.call(this, config.appConfig) || this;
    }
    /**
     * 扫描二维码
     */
    MediaService.prototype.scanQRCode = function () {
        return this.xviewNative("xviewScanBarcode", {});
    };
    /**
     * 水印图片
     * @param json
     */
    MediaService.prototype.xviewSaveImageToGallery = function (json) {
        return this.xviewNative('xviewSaveImageToGallery', json);
    };
    /**
     * 播放音频
     * @param json
     */
    MediaService.prototype.xviewPlayAudio = function (json) {
        return this.xviewNative('xviewPlayAudio', json);
    };
    /**
     * 录制音频
     * @param json
     */
    MediaService.prototype.xviewRecordAudio = function (json) {
        if (json === void 0) { json = { time: '60' }; }
        return this.xviewNative('xviewRecordAudio', json);
    };
    /**
     * 录制视频
     * @param json
     */
    MediaService.prototype.xviewRecordVideo = function (json) {
        return this.xviewNative('xviewRecordVideo', json).then(function (result) {
            if ((typeof result == 'object') && result.code == '0') {
                var _a = result.data, video = _a.file_path_video, image = _a.file_path_image;
                video && (result.data.video = video);
                image && (result.data.image = image);
            }
            return result;
        });
    };
    /**
     * 上传视频
     * @param json
     */
    MediaService.prototype.xviewUploadVideo = function (json) {
        return this.xviewNative('xviewUploadVideo', json);
    };
    /**
     * 从相册中选择视频上传
     * @param json
     */
    MediaService.prototype.xviewSelectVideoUpload = function (json) {
        return this.xviewNative('xviewSelectVideoUpload', json);
    };
    /**
     * 上传图片
     * @param json
     */
    MediaService.prototype.xviewUploadImage = function (json) {
        return this.xviewNative('xviewUploadImage', json);
    };
    /**
     * 上传文件
     * @param json
     */
    MediaService.prototype.xviewUploadFile = function (json) {
        return this.xviewNative('xviewUploadFile', json);
    };
    MediaService = __decorate([
        Injectable(),
        __metadata("design:paramtypes", [XViewConfig])
    ], MediaService);
    return MediaService;
}(XView));
export { MediaService };
//# sourceMappingURL=media-service.js.map