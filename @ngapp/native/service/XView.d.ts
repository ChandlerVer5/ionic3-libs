import { Result, Config } from './../constants/constants';
/**
 * Native 交互方法
 */
export declare class XView {
    _config: Config;
    constructor(config: Config);
    xviewNative<T>(methodName: string, param?: any): Promise<Result<T>>;
    xview(methodName: string, param?: any): void;
}
