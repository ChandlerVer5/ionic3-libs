var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
import { XVIEW_CONFIG } from './../../constants/config';
import { Injectable, Inject } from '@angular/core';
var XViewConfig = (function () {
    function XViewConfig(config) {
        this.config = Object.assign({ isDebug: true }, config);
    }
    Object.defineProperty(XViewConfig.prototype, "appConfig", {
        get: function () {
            return this.config;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(XViewConfig.prototype, "wechatConfig", {
        get: function () {
            return this.config.weChat;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(XViewConfig.prototype, "qqConfig", {
        get: function () {
            return this.config.qq;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(XViewConfig.prototype, "weiboConfig", {
        get: function () {
            return this.config.weiBo;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(XViewConfig.prototype, "xunFeiConfig", {
        get: function () {
            return this.config.xunFei;
        },
        enumerable: true,
        configurable: true
    });
    XViewConfig = __decorate([
        Injectable(),
        __param(0, Inject(XVIEW_CONFIG)),
        __metadata("design:paramtypes", [Object])
    ], XViewConfig);
    return XViewConfig;
}());
export { XViewConfig };
//# sourceMappingURL=XViewConfig.js.map