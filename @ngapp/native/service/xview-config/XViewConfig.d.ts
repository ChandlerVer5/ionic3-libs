import { Config, QQ, WeChat, SinaWeibo, XunFei } from './../../constants/constants';
export declare class XViewConfig {
    config: Config;
    constructor(config: Config);
    readonly appConfig: Config;
    readonly wechatConfig: WeChat;
    readonly qqConfig: QQ;
    readonly weiboConfig: SinaWeibo;
    readonly xunFeiConfig: XunFei;
}
