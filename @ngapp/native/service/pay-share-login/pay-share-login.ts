
import { XViewConfig } from './../xview-config/XViewConfig';
import { Injectable } from '@angular/core';
import { ShareType, Result, LoginInfo, AliPayCb, LianLianPay, WeChatPay, ShareWeb, ShareAudio, ShareImage, ShareText, ShareVideo } from './../../constants/constants';
import { XView } from './../XView';

@Injectable()
export class PayShareLogin extends XView {

    constructor(config: XViewConfig) {
        super(config.config);
    }

    /**
     * 微信登陆
     */
    weChatLogin(): Promise<Result<LoginInfo>> {
        return this.payShareLogin("weixinLogin", this._config.weChat);
    }

    /**
     * 微博登陆
     */
    weiboLogin(): Promise<Result<LoginInfo>> {
        return this.payShareLogin("weiboLogin", this._config.weiBo);
    }

    /**
     * QQ 登陆
     */
    qqLogin(): Promise<Result<LoginInfo>> {
        return this.payShareLogin("qqLogin", this._config.qq);
    }

    /**
     * 微信分享 网页
     * 
     * @param param 
     */
    wechatShareWebUrlToFriend(param: ShareWeb) {
        return this.wechatShareToFriend(param, 'web');
    }

    /**
     * 微信 文本分享
     * @param param 
     */
    wechatShareTextToFriend(param: ShareText) {
        return this.wechatShareToFriend(param, 'text');
    }

    /**
     * 图片分享
     * 
     * @param param 
     */
    wechatShareImageToFriend(param: ShareImage) {
        return this.wechatShareToFriend(param, 'image');
    }

    /**
     * 分享音乐
     * @param param 
     */
    wechatShareAudioToFriend(param: ShareAudio) {
        return this.wechatShareToFriend(param, 'audio');
    }

    /**
     * 分享视频
     * @param param 
     */
    wechatShareVideoToFriend(param: ShareVideo) {
        return this.wechatShareToFriend(param, 'video');
    }


    /**
     * 分享给朋友
     * @param param 
     * @param sharetype 
     */
    wechatShareToFriend(param: ShareWeb | ShareAudio | ShareImage | ShareText | ShareVideo, sharetype: ShareType) {
        return this.share("weixinShare", { ...param, ...(this._config.weChat), sharetype });
    }


    /**
    * 微信朋友圈 网页分享
    * 
    * @param param 
    */
    wechatShareWebUrlToCircle(param: ShareWeb) {
        return this.wechatShareToCircle(param, 'web');
    }

    /**
     * 微信朋友圈 文本分享
     * @param param 
     */
    wechatShareTextToCircle(param: ShareText) {
        return this.wechatShareToCircle(param, 'text');
    }

    /**
     * 图片分享
     * 
     * @param param 
     */
    wechatShareImageToCircle(param: ShareImage) {
        return this.wechatShareToCircle(param, 'image');
    }

    /**
     * 分享音乐
     * @param param 
     */
    wechatShareAudioToCircle(param: ShareAudio) {
        return this.wechatShareToCircle(param, 'audio');
    }

    /**
     * 分享视频
     * @param param 
     */
    wechatShareVideoToCircle(param: ShareVideo) {
        return this.wechatShareToCircle(param, 'video');
    }

    /**
     * 朋友圈
     * @param param 
     * @param sharetype 
     */
    wechatShareToCircle(param: ShareWeb | ShareAudio | ShareImage | ShareText | ShareVideo, sharetype: ShareType) {
        return this.share("weixinCircleShare", { ...param, sharetype, ...(this._config.weChat) });
    }

    /**
    * qq 网页分享
    * 
    * @param param 
    */
    qqShareWebUrlToFriend(param: ShareWeb) {
        return this.qqShareToFriend(param, 'web');
    }

    /**
     * qq 文本分享
     * @param param 
     */
    qqShareTextToFriend(param: ShareText) {
        return this.qqShareToFriend(param, 'text');
    }

    /**
     * qq 图片分享
     * 
     * @param param 
     */
    qqShareImageToFriend(param: ShareImage) {
        return this.qqShareToFriend(param, 'image');
    }

    /**
     * qq 分享音乐
     * @param param 
     */
    qqShareAudioToFriend(param: ShareAudio) {
        return this.qqShareToFriend(param, 'audio');
    }

    /**
     * qq 分享视频
     * @param param 
     */
    qqShareVideoToFriend(param: ShareVideo) {
        return this.qqShareToFriend(param, 'video');
    }

    qqShareToFriend(param: ShareWeb | ShareAudio | ShareImage | ShareText | ShareVideo, sharetype: ShareType) {
        return this.share("qqShare", { ...param, ...(this._config.qq), sharetype });
    }

    /**
    * qq 网页分享
    * 
    * @param param 
    */
    qqShareWebUrlToZone(param: ShareWeb) {
        return this.qqShareToZone(param, 'web');
    }

    /**
     * qq 文本分享
     * @param param 
     */
    qqShareTextToZone(param: ShareText) {
        return this.qqShareToZone(param, 'text');
    }

    /**
     * qq 图片分享
     * 
     * @param param 
     */
    qqShareImageToZone(param: ShareImage) {
        return this.qqShareToZone(param, 'image');
    }

    /**
     * qq 分享音乐
     * @param param 
     */
    qqShareAudioToZone(param: ShareAudio) {
        return this.qqShareToZone(param, 'audio');
    }

    /**
     * qq 分享视频
     * @param param 
     */
    qqShareVideoToZone(param: ShareVideo) {
        return this.qqShareToZone(param, 'video');
    }

    /**
     * qq 空间
     * @param param 
     * @param sharetype 
     */
    qqShareToZone(param: ShareWeb | ShareAudio | ShareImage | ShareText | ShareVideo, sharetype: string) {
        return this.share("qqZoneShare", { ...param, sharetype, ...(this._config.qq) });
    }

    /**
    * weibo 网页分享
    * 
    * @param param 
    */
    weiboShareWebUrlTo(param: ShareWeb) {
        return this.weiboShareTo(param, 'web');
    }

    /**
     * weibo 文本分享
     * @param param 
     */
    weiboShareTextTo(param: ShareText) {
        return this.weiboShareTo(param, 'text');
    }

    /**
     * weibo 图片分享
     * 
     * @param param 
     */
    weiboShareImageTo(param: ShareImage) {
        return this.weiboShareTo(param, 'image');
    }

    /**
     * weibo 分享音乐
     * @param param 
     */
    weiboShareAudioTo(param: ShareAudio) {
        return this.weiboShareTo(param, 'audio');
    }

    /**
     * weibo 分享视频
     * @param param 
     */
    weiboShareVideoTo(param: ShareVideo) {
        return this.weiboShareTo(param, 'video');
    }

    /**
     * 微博分享
     * @param param 
     * @param sharetype 
     */
    weiboShareTo(param: ShareWeb | ShareAudio | ShareImage | ShareText | ShareVideo, sharetype: ShareType) {
        return this.share("weiboShare", { ...param, ...(this._config.weiBo), sharetype });
    }


    private share(shareType: string, param: any): Promise<Result<{
        "result": string;
        "type": string;
    }>> {
        return this.payShareLogin(shareType, param);
    }

    /**
     * 支付支付
     * @param alipaydata 
     */
    aliPay(alipaydata: string): Promise<Result<AliPayCb>> {
        return this.payShareLogin("aliPay", { alipaydata });
    }

    /**
     * 不传appid 默认使用 配置文件中wechat的appid
     * @param param 
     */
    weChatPay(param: WeChatPay): Promise<Result<{
        "result": string;//"success/失败原因"
        "type": string;//"请求xview方法时的type类型" 
    }>> {
        if (!param.appid) {
            param.appid = this._config.weChat.wxappid;
        }
        return this.payShareLogin("weixinPay", { ...param, ...(this._config.weChat) });
    }

    /**
     * 连连支付
     * @param param 
     */
    lianlianPay(param: LianLianPay): Promise<Result<{
        "ret_code": string;//"错误码",
        "ret_msg": string;//"错误信息", 
        "type": string;//"请求xview方法时的type类型"
    }>> {
        return this.payShareLogin("lianlianVerifyPay", param);
    }

    /**
     * 跳到 QQ 界面
     * @param qq 
     */
    xviewPushQQ(qq: string): Promise<Result<{
        "result": string;
        "type": string;
    }>> {
        return this.xviewNative("xviewPushQQ", { qq });
    }


    /**
     * 是否安装微信
     */
    xviewIsWXAppInstalled(): Promise<Result<{
        "result": string;// "success/未安装微信客户端",
        "type": string;// "请求xview方法时的type类型"
    }>> {
        return this.xviewNative("xviewIsWXAppInstalled", {});
    }


    /**
     * 
     * @param type 
     * @param data 
     */
    private payShareLogin<T>(type: string, data: any): Promise<Result<T>> {
        return this.xviewNative('xviewLoginPayShare', { type, data })
    }
}