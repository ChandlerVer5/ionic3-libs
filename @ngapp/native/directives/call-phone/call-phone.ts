import { PhoneUtilsService } from './../../service/phone-service/phoneUtils-service';

import { Directive, Output, HostListener, EventEmitter, Input } from '@angular/core';

/**
 * 拨打电话
 */
@Directive({
    selector: '[call-phone]'
})
export class CallPhone {

    @Output('call-phone') callPhone: EventEmitter<Function> = new EventEmitter<Function>();

    @Input() phone: string;

    constructor(public phoneUtils: PhoneUtilsService) { }

    @HostListener('click', ['$event'])
    callback(e: Event) {
        e && e.stopPropagation();
        const that = this;
        this.callPhone.emit((phone: string = this.phone) => {
            that.phoneUtils.xviewCallPhone(phone);
        });
    }

}