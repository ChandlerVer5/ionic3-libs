var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { RongCloudService } from './../../service/rong-cloud/rong-cloud';
import { Directive, Output, EventEmitter, HostListener, Input } from '@angular/core';
var PushView = (function () {
    function PushView(rongCloudService) {
        this.rongCloudService = rongCloudService;
        this.pushView = new EventEmitter();
        this._panelType = '0'; //1: 融云
    }
    Object.defineProperty(PushView.prototype, "rongCloud", {
        get: function () {
            return this._rongCloud;
        },
        set: function (_rongCloud) {
            this._panelType = '1';
            var sessionId = _rongCloud.sessionId, sessionType = _rongCloud.sessionType, title = _rongCloud.title, nickname = _rongCloud.nickname, path = _rongCloud.path;
            this._rongCloud = { sessionId: sessionId, sessionType: sessionType, title: title, nickname: nickname, path: path };
        },
        enumerable: true,
        configurable: true
    });
    PushView.prototype.callback = function (e) {
        var _this = this;
        e && e.stopPropagation();
        switch (this._panelType) {
            case '1':
                {
                    this.pushView.emit(function () {
                        _this.rongCloudService.xviewPushSessionView(_this._rongCloud);
                    });
                }
                ;
                break;
            default:
                break;
        }
    };
    __decorate([
        Output('push-view'),
        __metadata("design:type", EventEmitter)
    ], PushView.prototype, "pushView", void 0);
    __decorate([
        Input('rong-cloud'),
        __metadata("design:type", Object),
        __metadata("design:paramtypes", [Object])
    ], PushView.prototype, "rongCloud", null);
    __decorate([
        HostListener('click', ['$event']),
        __metadata("design:type", Function),
        __metadata("design:paramtypes", [Event]),
        __metadata("design:returntype", void 0)
    ], PushView.prototype, "callback", null);
    PushView = __decorate([
        Directive({
            selector: '[push-view]'
        }),
        __metadata("design:paramtypes", [RongCloudService])
    ], PushView);
    return PushView;
}());
export { PushView };
//# sourceMappingURL=push-view.js.map