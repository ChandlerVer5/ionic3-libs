import { RongCloudSessionPane } from './../../constants';
import { RongCloudService } from './../../service/rong-cloud/rong-cloud';
import { Directive, Output, EventEmitter, HostListener, Input } from '@angular/core';


@Directive({
    selector: '[push-view]'
})
export class PushView {

    @Output('push-view') pushView: EventEmitter<Function> = new EventEmitter<Function>();

    _rongCloud: RongCloudSessionPane;

    _panelType: string = '0'; //1: 融云

    @Input('rong-cloud')
    set rongCloud(_rongCloud: RongCloudSessionPane) {
        this._panelType = '1';
        const { sessionId, sessionType, title, nickname, path } = _rongCloud;
        this._rongCloud = { sessionId, sessionType, title, nickname, path };
    }

    get rongCloud() {
        return this._rongCloud;
    }

    constructor(public rongCloudService: RongCloudService) {

    }

    @HostListener('click', ['$event'])
    callback(e: Event) {
        e && e.stopPropagation();
        switch (this._panelType) {
            case '1': {
                this.pushView.emit(() => {
                    this.rongCloudService.xviewPushSessionView(this._rongCloud);
                });
            }; break;
            default:
                break;
        }
    }

}