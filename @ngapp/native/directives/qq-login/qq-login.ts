import { PayShareLogin } from './../../service/pay-share-login/pay-share-login';
import { Directive, EventEmitter, HostListener, Output, } from '@angular/core';
@Directive({
    selector: '[qq-login]'
})
export class QQLogin {

    @Output('qq-login') qqLogin: EventEmitter<any> = new EventEmitter<any>();

    constructor(public payShareLogin: PayShareLogin) { }


    @HostListener('click', ['$event'])
    login(e: Event) {
        e && e.stopPropagation();
        const that = this;
        that.payShareLogin.qqLogin().then(info => {
            that.qqLogin.emit(info);
        });
    }

}