import { PhoneUtilsService } from './../../service/phone-service/phoneUtils-service';

import { Directive, Output, HostListener, EventEmitter } from '@angular/core';

/**
 * 拨打电话
 */
@Directive({
    selector: '[clean-cache]'
})
export class CleanCache {

    @Output('clean-cache') cleanCache: EventEmitter<Function> = new EventEmitter<Function>();

    constructor(public phoneUtils: PhoneUtilsService) {

    }

    @HostListener('click', ['$event'])
    callback(e: Event) {
        e && e.stopPropagation();
        const that = this;
        this.cleanCache.emit(() => {
            that.phoneUtils.xviewCleanCache();
        });
    }

}