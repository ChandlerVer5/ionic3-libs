var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { PhoneUtilsService } from './../../service/phone-service/phoneUtils-service';
import { Directive, Output, HostListener, EventEmitter } from '@angular/core';
/**
 * 拨打电话
 */
var CleanCache = (function () {
    function CleanCache(phoneUtils) {
        this.phoneUtils = phoneUtils;
        this.cleanCache = new EventEmitter();
    }
    CleanCache.prototype.callback = function (e) {
        e && e.stopPropagation();
        var that = this;
        this.cleanCache.emit(function () {
            that.phoneUtils.xviewCleanCache();
        });
    };
    __decorate([
        Output('clean-cache'),
        __metadata("design:type", EventEmitter)
    ], CleanCache.prototype, "cleanCache", void 0);
    __decorate([
        HostListener('click', ['$event']),
        __metadata("design:type", Function),
        __metadata("design:paramtypes", [Event]),
        __metadata("design:returntype", void 0)
    ], CleanCache.prototype, "callback", null);
    CleanCache = __decorate([
        Directive({
            selector: '[clean-cache]'
        }),
        __metadata("design:paramtypes", [PhoneUtilsService])
    ], CleanCache);
    return CleanCache;
}());
export { CleanCache };
//# sourceMappingURL=clean-cache.js.map