import { MediaService } from './../../service/media-service/media-service';

import { Directive, Output, HostListener, EventEmitter } from '@angular/core';

/**
 * 扫描二维码
 */
@Directive({
    selector: '[scan-QRcode]'
})
export class ScanQRCode {

    @Output('scan-QRcode') scanQRCode: EventEmitter<any> = new EventEmitter<any>();

    constructor(public mediaService: MediaService) { }

    @HostListener('click', ['$event'])
    callBack(e: Event) {
        e && e.stopPropagation();
        const that = this;
        that.mediaService.scanQRCode().then(info => {
            that.scanQRCode.emit(info);
        });
    }

}