import { RongCloudSession } from './../../constants';
import { RongCloudService } from './../../service/rong-cloud/rong-cloud';
import { Directive, Output, EventEmitter, HostListener, Input } from '@angular/core';


@Directive({
    selector: '[pop-view]'
})
export class PopView {

    @Output('pop-view') popView: EventEmitter<Function> = new EventEmitter<Function>();

    _rongCloud: RongCloudSession;

    _panelType: string = '0'; //1: 融云

    @Input('rong-cloud')
    set rongCloud(_rongCloud: RongCloudSession) {
        this._panelType = '1';
        const { sessionId, sessionType } = _rongCloud;
        this._rongCloud = { sessionId, sessionType };
    }

    get rongCloud() {
        return this._rongCloud;
    }

    constructor(public rongCloudService: RongCloudService) {

    }

    @HostListener('click', ['$event'])
    callback(e: Event) {
        e && e.stopPropagation();
        switch (this._panelType) {
            case '1': {
                this.popView.emit(() => {
                    this.rongCloudService.xviewDeleteSession(this._rongCloud);
                });
            }; break;
            default:
                break;
        }
    }

}