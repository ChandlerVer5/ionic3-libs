import { RongCloudSession } from './../../constants';
import { RongCloudService } from './../../service/rong-cloud/rong-cloud';
import { EventEmitter } from '@angular/core';
export declare class PopView {
    rongCloudService: RongCloudService;
    popView: EventEmitter<Function>;
    _rongCloud: RongCloudSession;
    _panelType: string;
    rongCloud: RongCloudSession;
    constructor(rongCloudService: RongCloudService);
    callback(e: Event): void;
}
