import { PayShareLogin, PhoneUtilsService, JPushService, XViewConfig, MediaService, RongCloudService, BlueToothService } from './service';
import { IosBarColor, CallPhone, CleanCache, ScanQRCode, WeiboLogin, QQLogin, WechatLogin, PushView, PopView } from './directives';
import { XVIEW_CONFIG, xViewConfigFactory, Config } from './constants';
import { NgModule, ModuleWithProviders } from '@angular/core';




@NgModule({
    declarations: [
        CallPhone,
        CleanCache,
        IosBarColor,
        ScanQRCode,
        WeiboLogin,
        QQLogin,
        WechatLogin,
        PushView,
        PopView,
    ],
    exports: [
        CallPhone,
        CleanCache,
        IosBarColor,
        ScanQRCode,
        WeiboLogin,
        QQLogin,
        WechatLogin,
        PushView,
        PopView,
    ]
})
export class NativeModule {

    static config(ops: Config): ModuleWithProviders {
        return {
            ngModule: NativeModule,
            providers: [
                { provide: XVIEW_CONFIG, useValue: ops },
                { provide: XViewConfig, useFactory: xViewConfigFactory, deps: [XVIEW_CONFIG] },
                JPushService,
                PhoneUtilsService,
                MediaService,
                PayShareLogin,
                RongCloudService,
                BlueToothService,
            ]
        }
    }
}
