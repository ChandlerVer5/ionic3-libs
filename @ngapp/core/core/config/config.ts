import { Observable } from 'rxjs/Observable';
import { MockHttp, ConfigToken, MOCK_HTTP_TOKEN } from './../core/other';
import { Constants, HttpCode, Config } from './../constants/constatns';
import { Injectable, Inject, Optional } from '@angular/core';
import { isUndefined } from '../../utils/utils';

/**
 * 默认值
 */
const _initConfig: Config = {
    "BASE_DOMAIN": 'http://test.com/',
    "BASE_API": 'api/',
    "LOADING_TEXT": 'loading...',
    "TOKEN_NAME": 'TOKEN',
    "HTTP_DEBUG": false,
    "EXTRA_HEARDE": true,
    "SUCCESS_CODE": "200",
    "HTTP_OPTIONS": {
        "withCredentials": false
    },
    "HTTP_TIME_OUT": 15000
}

@Injectable()
export class AppConfig {
    _config: Config;

    providers: MockHttp[] = [];

    constructor( @Inject(ConfigToken) config: Config, @Optional() @Inject(MOCK_HTTP_TOKEN) providers: MockHttp[]) {
        console.log("AppConfig");
        this._config = Object.assign(_initConfig, config);
        if (!this.getVersion()) {
            this.getAppVersion().then(version => {
                console.log("获取版本信息");
                console.log(JSON.stringify(version));
                this._config[Constants.APP_VERSION] = version.data.versionCode;
            });
        }

        if (providers) { // 过滤
            this.providers = providers.filter((info) => info.get || info.post);
        }

    }


    /**
     * 获取版本号
     */
    public getAppVersion(): Promise<{
        code: string;
        data: {
            versionCode: string;// "版本号 - android 升级判断使用",
            versionName: string;
        };
        message: string
    }> {
        return new Promise(resolve => {
            let json = {
                "callback": "xviewInitAppVersionCallback"
            }
            window['xviewInitAppVersionCallback'] = function (param) {
                resolve(param);
            }
            try {// 获取当前APP版本号
                if (window['webkit']) {
                    window['webkit'].messageHandlers['xviewAppVersion'].postMessage(JSON.stringify(json));
                } else {
                    window['xview'].xviewAppVersion(JSON.stringify(json));
                }
            } catch (e) {
                console.warn("Not Found xviewAppVersion Method");
                window['xviewInitAppVersionCallback']({ code: '0', data: { version: '1.0.0', bundle: '测试版本' }, message: '测试版本' });
            }
        });
    }

    /**
    * 处理数据
    * @param httpCode 
    */
    private handleResultData(httpCode: HttpCode<any>) {
        return Observable.create((observer) => {
            setTimeout(() => {
                observer.next(httpCode);
                observer.complete();
            }, 500);
        });
    }

    /**
     * GET 
     * @param url 
     * @param param 
     */
    get(url, param): Observable<HttpCode<any>> {
        let httpCode: HttpCode<any>;
        for (let provider of this.providers) {
            if (provider.get) {
                httpCode = provider.get(url, param);
                if (httpCode) {
                    return this.handleResultData(httpCode);
                }
            }
        }
        return null;
    }

    /**
     * POST
     * @param url 
     * @param param 
     */
    post(url, param): Observable<HttpCode<any>> {
        let httpCode: HttpCode<any>;
        for (let provider of this.providers) {
            if (provider.post) {
                httpCode = provider.post(url, param);
                if (httpCode) {
                    return this.handleResultData(httpCode);
                }
            }
        }
        return null;
    }

    domain() {
        return this._config.BASE_DOMAIN;
    }

    API_Path(): string {
        return this.domain() + this._config.BASE_API;
    }

    getDebug(): boolean {
        return this._config.HTTP_DEBUG;
    }

    loadingText() {
        return this._config.LOADING_TEXT;
    }

    tokenName() {
        return this._config.TOKEN_NAME;
    }

    getValue(key: string, defaultValue?: any): any {
        if (isUndefined(this._config[key])) return defaultValue;
        return this._config[key];
    }

    getVersion() {
        return this._config.APP_VERSION;
    }

    getExtraHeader() {
        return this._config.EXTRA_HEARDE;
    }

    getHttpOptions() {
        return this._config.HTTP_OPTIONS;
    }

    get SuccessCode() {
        return this._config.SUCCESS_CODE;
    }

    get HttpTimeOut() {
        return this._config.HTTP_TIME_OUT;
    }
}