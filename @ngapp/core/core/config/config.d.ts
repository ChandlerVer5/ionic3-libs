import { Observable } from 'rxjs/Observable';
import { MockHttp } from './../core/other';
import { HttpCode, Config } from './../constants/constatns';
export declare class AppConfig {
    _config: Config;
    providers: MockHttp[];
    constructor(config: Config, providers: MockHttp[]);
    /**
     * 获取版本号
     */
    getAppVersion(): Promise<{
        code: string;
        data: {
            versionCode: string;
            versionName: string;
        };
        message: string;
    }>;
    /**
    * 处理数据
    * @param httpCode
    */
    private handleResultData(httpCode);
    /**
     * GET
     * @param url
     * @param param
     */
    get(url: any, param: any): Observable<HttpCode<any>>;
    /**
     * POST
     * @param url
     * @param param
     */
    post(url: any, param: any): Observable<HttpCode<any>>;
    domain(): string;
    API_Path(): string;
    getDebug(): boolean;
    loadingText(): string;
    tokenName(): string;
    getValue(key: string, defaultValue?: any): any;
    getVersion(): string;
    getExtraHeader(): boolean;
    getHttpOptions(): {
        "withCredentials"?: boolean;
    };
    readonly SuccessCode: string;
    readonly HttpTimeOut: number;
}
