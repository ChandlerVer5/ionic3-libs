var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { AlertService } from './../alert-service/alert-service';
import { UtilsService } from './../utils-service/utils-service';
import { Injectable } from '@angular/core';
var FormValidate = (function () {
    function FormValidate(utilsService, alertService) {
        var _this = this;
        this.utilsService = utilsService;
        this.alertService = alertService;
        /**
         * 校验 根据属性名校验
         * @param param
         * @param validate
         */
        this.validate = function (param) {
            var validate = [];
            for (var _i = 1; _i < arguments.length; _i++) {
                validate[_i - 1] = arguments[_i];
            }
            var value, validateValue, params;
            var that = _this;
            for (var _a = 0, validate_1 = validate; _a < validate_1.length; _a++) {
                var _info = validate_1[_a];
                value = param[_info.name];
                validateValue = _info.toChecked;
                for (var _b = 0, validateValue_1 = validateValue; _b < validateValue_1.length; _b++) {
                    var _item = validateValue_1[_b];
                    params = _item.param;
                    if (!that.validateFn(_item.fn).apply(void 0, [value].concat(params))) {
                        that.alert(_item.tip);
                        return true;
                    }
                }
            }
            return false;
        };
        /**
         * 根据表单值检验
         * @param validateAttr
         */
        this.validateValue = function () {
            var validateAttr = [];
            for (var _i = 0; _i < arguments.length; _i++) {
                validateAttr[_i] = arguments[_i];
            }
            var validateValue, params;
            var that = _this;
            for (var _a = 0, validateAttr_1 = validateAttr; _a < validateAttr_1.length; _a++) {
                var _info = validateAttr_1[_a];
                validateValue = _info.toChecked;
                for (var _b = 0, validateValue_2 = validateValue; _b < validateValue_2.length; _b++) {
                    var _item = validateValue_2[_b];
                    params = _item.param;
                    if (!that.validateFn(_item.fn).apply(void 0, [_info.value].concat(params))) {
                        that.alert(_item.tip);
                        return true;
                    }
                }
            }
            return false;
        };
        /**
         * 判断是不是空
         * @param value
         * @return false 为空
         */
        this.isNotBlanK = function (value) {
            return _this.utilsService.isNotEmpty(value);
        };
        /**
         * 判断是不是手机号
         */
        this.isPhone = function (value) {
            return _this.utilsService.validatePhone(value);
        };
        /**
         * 判断是不是密码  6-18
         */
        this.isPassWord = function (value, min, max) {
            if (min === void 0) { min = 6; }
            if (max === void 0) { max = 18; }
            return _this.utilsService.validatePassWord(value, min, max);
        };
        /**
         * 判断array 大小
         */
        this.arrayLength = function (value, op, length) {
            if (op === void 0) { op = '>'; }
            if (length === void 0) { length = 0; }
            switch (op) {
                case '>':
                    return value.length > length;
                case '=':
                    return value.length == length;
                case '>=':
                    return value.length >= length;
                default:
                    return value.length > length;
            }
        };
        /**
         * 等于
         */
        this.equals = function (value, another) {
            return value == another;
        };
        /**
         * 校验车牌号
         */
        this.isVehicleNumber = function (carNum) {
            return _this.utilsService.isVehicleNumber(carNum);
        };
    }
    /**
     * 空方法
     */
    FormValidate.prototype.nullFun = function () {
        console.error("this is null function");
        return false;
    };
    /**
     *  检验制造商
     * @param type
     * @return 函数制造商 返回true 表示校验通过 否则 不通过 提示不通过原因
     */
    FormValidate.prototype.validateFn = function (type) {
        switch (type) {
            case ValidateType.NotBlank:
                return this.isNotBlanK;
            case ValidateType.isPhone:
                return this.isPhone;
            case ValidateType.isPassWord:
                return this.isPassWord;
            case ValidateType.arrayLength:
                return this.arrayLength;
            case ValidateType.equals:
                return this.equals;
            case ValidateType.isVehicleNumber:
                return this.isVehicleNumber;
            default:
                return this.nullFun;
        }
    };
    /**
     * 弹框
     *
     * @private
     * @param {string} message
     * @param {string} [title='警告']
     *
     */
    FormValidate.prototype.alert = function (message, title) {
        if (title === void 0) { title = '警告'; }
        this.alertService.alert({ message: message, title: title });
    };
    FormValidate = __decorate([
        Injectable(),
        __metadata("design:paramtypes", [UtilsService, AlertService])
    ], FormValidate);
    return FormValidate;
}());
export { FormValidate };
export var ValidateType;
(function (ValidateType) {
    /**
     * 不等于空
     */
    ValidateType[ValidateType["NotBlank"] = 0] = "NotBlank";
    /**
     * 是不是手机号
     */
    ValidateType[ValidateType["isPhone"] = 1] = "isPhone";
    /**
     * 是不是密码
     */
    ValidateType[ValidateType["isPassWord"] = 2] = "isPassWord";
    /**
     * 数组大小是否大于0
     */
    ValidateType[ValidateType["arrayLength"] = 3] = "arrayLength";
    /**
     * 是否等于某个值
     */
    ValidateType[ValidateType["equals"] = 4] = "equals";
    /**
     * 是否是车牌号
     */
    ValidateType[ValidateType["isVehicleNumber"] = 5] = "isVehicleNumber";
})(ValidateType || (ValidateType = {}));
//# sourceMappingURL=form-validate.js.map