import { AlertService } from './../alert-service/alert-service';
import { ValidateAttr } from './form-validate';
import { UtilsService } from './../utils-service/utils-service';
export declare class FormValidate {
    private utilsService;
    private alertService;
    constructor(utilsService: UtilsService, alertService: AlertService);
    /**
     * 校验 根据属性名校验
     * @param param
     * @param validate
     */
    validate: (param: Object, ...validate: Validate[]) => boolean;
    /**
     * 根据表单值检验
     * @param validateAttr
     */
    validateValue: (...validateAttr: ValidateAttr[]) => boolean;
    /**
     * 判断是不是空
     * @param value
     * @return false 为空
     */
    private isNotBlanK;
    /**
     * 判断是不是手机号
     */
    private isPhone;
    /**
     * 判断是不是密码  6-18
     */
    private isPassWord;
    /**
     * 判断array 大小
     */
    private arrayLength;
    /**
     * 等于
     */
    private equals;
    /**
     * 校验车牌号
     */
    private isVehicleNumber;
    /**
     * 空方法
     */
    private nullFun();
    /**
     *  检验制造商
     * @param type
     * @return 函数制造商 返回true 表示校验通过 否则 不通过 提示不通过原因
     */
    private validateFn(type);
    /**
     * 弹框
     *
     * @private
     * @param {string} message
     * @param {string} [title='警告']
     *
     */
    private alert(message, title?);
}
export declare enum ValidateType {
    /**
     * 不等于空
     */
    NotBlank = 0,
    /**
     * 是不是手机号
     */
    isPhone = 1,
    /**
     * 是不是密码
     */
    isPassWord = 2,
    /**
     * 数组大小是否大于0
     */
    arrayLength = 3,
    /**
     * 是否等于某个值
     */
    equals = 4,
    /**
     * 是否是车牌号
     */
    isVehicleNumber = 5,
}
export interface Validate {
    name: string;
    toChecked: ValidateValue[];
}
export interface ValidateAttr {
    value: string | number | Array<any>;
    toChecked: ValidateValue[];
}
export interface ValidateValue {
    fn: ValidateType;
    tip: string;
    param?: any[];
}
