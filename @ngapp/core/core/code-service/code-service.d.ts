import { HttpCode } from './../constants/constatns';
import { AlertService } from './../alert-service/alert-service';
import 'rxjs/add/operator/debounceTime';
export interface CodeHander {
    httpCodeHandle(error: HttpCode<any>): any;
}
/**
 * Http 状态
 */
export declare class CodeService implements CodeHander {
    alertService: AlertService;
    private subject$;
    constructor(alertService: AlertService);
    /**
     * 处理Http 返回码
     * @param code
     */
    httpCodeHandle(error: HttpCode<any>): void;
}
