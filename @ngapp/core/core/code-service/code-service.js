var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { Subject } from 'rxjs/Subject';
import { AlertService } from './../alert-service/alert-service';
import { Injectable } from '@angular/core';
import 'rxjs/add/operator/debounceTime';
/**
 * Http 状态
 */
var CodeService = (function () {
    function CodeService(alertService) {
        var _this = this;
        this.alertService = alertService;
        this.subject$ = new Subject();
        this.subject$.asObservable().debounceTime(800).subscribe(function (error) {
            switch (error.code) {
                case '':
                case '0':
                case '404':
                case '500':
                case '506':
                case '506':
                case '600':
                    _this.alertService.toast(error.message || '服务器数据异常');
                    break;
                default:
                    _this.alertService.alert(error.message);
                    break;
            }
            ;
        });
    }
    /**
     * 处理Http 返回码
     * @param code
     */
    CodeService.prototype.httpCodeHandle = function (error) {
        this.subject$.next(error);
    };
    CodeService = __decorate([
        Injectable(),
        __metadata("design:paramtypes", [AlertService])
    ], CodeService);
    return CodeService;
}());
export { CodeService };
//# sourceMappingURL=code-service.js.map