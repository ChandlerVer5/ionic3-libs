import { Subject } from 'rxjs/Subject';
import { HttpCode } from './../constants/constatns';
import { AlertService } from './../alert-service/alert-service';
import { Injectable } from '@angular/core';
import 'rxjs/add/operator/debounceTime'


export interface CodeHander {

    httpCodeHandle(error: HttpCode<any>);
}
/**
 * Http 状态
 */
@Injectable()
export class CodeService implements CodeHander {

    private subject$: Subject<any> = new Subject<any>();

    constructor(public alertService: AlertService) {
        this.subject$.asObservable().debounceTime(800).subscribe(error => {
            switch (error.code) {
                case '':
                case '0':
                case '404':
                case '500':
                case '506':
                case '506':
                case '600':
                    this.alertService.toast(error.message || '服务器数据异常');
                    break;
                default:
                    this.alertService.alert(error.message);
                    break;
            };
        })
    }

    /**
     * 处理Http 返回码
     * @param code 
     */
    public httpCodeHandle(error: HttpCode<any>) {
        this.subject$.next(error);
    }
}