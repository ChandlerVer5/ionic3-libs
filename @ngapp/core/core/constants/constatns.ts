
/**
 * 返回码
 */
export class HttpCode<T> {
    code: string;
    message?: string;
    data?: T;
    success?: boolean;

}

/**
 * 分页
 */
export class Page<T> {
    page: number;
    pageNumber: number;
    list: T[];
    firstPage: boolean;
    lastPage: boolean;
    pageSize: number;
    totalPage: number;
    totalRow: number;

    constructor() {
        this.page = 1;
        this.pageNumber = 1;
        this.list = [];
        this.firstPage = true;
        this.lastPage = true;
        this.pageSize = 10;
        this.totalPage = 1;
        this.totalRow = 0;
    }
}



export interface Config {
    "BASE_DOMAIN"?: string;
    "BASE_API"?: string;
    "LOADING_TEXT"?: string;
    "TOKEN_NAME"?: string;
    "HTTP_DEBUG"?: boolean;
    "EXTRA_HEARDE"?: boolean;
    "APP_VERSION"?: string;
    "SUCCESS_CODE"?: string;
    "HTTP_OPTIONS"?: {
        "withCredentials"?: boolean;
    },
    "HTTP_TIME_OUT"?: number;
}


/**
 * 数据处理工具
 */
export class DateUtils {

    /**
     * 处理分页数据
     * 
     * 
     * 
     * 
     * @param oldPage 老分页数据
     * @param newPage 新的分页数据
     */
    static renderPageData<T>(oldPage: Page<T>, newPage: Page<T>): Page<T> {
        let { list, page, pageNumber, pageSize, firstPage, lastPage, totalPage, totalRow } = newPage;
        oldPage = oldPage || new Page<T>();
        isNaN(page) && (page = pageNumber);
        if (1 == page) {
            oldPage.list = [];
        }
        if (oldPage.page == page) {

        }
        return {
            ...oldPage,
            list: oldPage.list.concat(list),
            page,
            pageNumber, pageSize, firstPage, lastPage, totalPage, totalRow
        }
    }

    /**
     * 处理分页(并将数组内的元素转为对应类型)
     * 
     * @param oldPage 
     * @param newPage 
     */
    static renderPage<T>(oldPage: Page<T>, newPage: Page<T>, newInatance: { new(): T }): Page<T> {
        let { list, pageNumber, pageSize, firstPage, lastPage, totalPage, totalRow } = newPage;
        oldPage = oldPage || new Page<T>();
        if (1 == pageNumber) {
            oldPage.list = new Array<T>();
        }
        if (oldPage.pageNumber == pageNumber) {

        }
        return {
            ...oldPage,
            list: oldPage.list.concat(list.reduce(function (prev, cur) {
                prev.push(Object.assign(new newInatance(), cur));
                return prev;
            }, [])),
            pageNumber, pageSize, firstPage, lastPage, totalPage, totalRow
        }
    }
}


export class Constants {

    /**
     * 服务器地址
     */
    static BASE_DOMAIN = "BASE_DOMAIN";

    /**
     * 接口地址
     */
    static BASE_API = 'BASE_API';

    /**
     * 加载框文字
     */
    static LOADING_TEXT = "LOADING_TEXT";

    /**
     * POST 默认TOKEN的名字
     * 
     * POST 默认传递参数 [TOKEN_NAME]=window.localStorage[TOKEN_NAME]
     */
    static TOKEN_NAME = "TOKEN_NAME";

    /**
     * debug
     */
    static HTTP_DEBUG = "HTTP_DEBUG";


    /**
     * 版本号
     */
    static APP_VERSION = "APP_VERSION";

    /**
     * 是否启用额外请求头
     */
    static EXTRA_HEARDE = "EXTRA_HEARDE";

}