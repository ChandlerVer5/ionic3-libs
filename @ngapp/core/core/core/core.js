var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
import { FormValidate } from './../form-validate/form-validate';
import { CodeService } from './../code-service/code-service';
import { HttpService } from './../http-service/http-service';
import { AlertService } from './../alert-service/alert-service';
import { UtilsService } from './../utils-service/utils-service';
import { ConfigToken } from './other';
import { AppConfig } from './../config/config';
import { HttpModule } from '@angular/http';
import { NgModule } from '@angular/core';
import { IonicModule } from 'ionic-angular';
var AppCoreModule = (function () {
    function AppCoreModule() {
    }
    AppCoreModule_1 = AppCoreModule;
    /**
     * 项目配置
     * @param options
     */
    AppCoreModule.config = function (options) {
        if (options === void 0) { options = {}; }
        return {
            ngModule: AppCoreModule_1,
            providers: [
                { provide: ConfigToken, useValue: options },
                AppConfig,
                UtilsService,
                AlertService,
                FormValidate,
                // MiddlewareStoreService,
                HttpService,
                CodeService,
            ]
        };
    };
    AppCoreModule = AppCoreModule_1 = __decorate([
        NgModule({
            imports: [
                HttpModule,
                IonicModule
            ]
        })
    ], AppCoreModule);
    return AppCoreModule;
    var AppCoreModule_1;
}());
export { AppCoreModule };
//# sourceMappingURL=core.js.map