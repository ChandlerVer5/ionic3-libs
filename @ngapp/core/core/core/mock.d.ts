import { ModuleWithProviders } from '@angular/core';
export declare class ChildMockHttpProvides {
    /**
     * 添加mock 模拟数据源
     * @param mockHttpProvides
     */
    static childMock(mockHttpProvides: Function): ModuleWithProviders;
}
