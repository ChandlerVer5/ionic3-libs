import { MOCK_HTTP_TOKEN } from './other';
import { ModuleWithProviders, NgModule } from '@angular/core';

@NgModule()
export class ChildMockHttpProvides {

    /**
     * 添加mock 模拟数据源
     * @param mockHttpProvides 
     */
    static childMock(mockHttpProvides: Function): ModuleWithProviders {
        return {
            ngModule: ChildMockHttpProvides,
            providers: [
                {
                    provide: MOCK_HTTP_TOKEN,
                    useFactory: mockHttpProvides,
                    multi: true
                }
            ]
        }
    }
}