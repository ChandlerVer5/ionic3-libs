import { InjectionToken } from '@angular/core';
import 'rxjs/add/operator/debounceTime';
export var MOCK_HTTP_TOKEN = new InjectionToken('MockHttpToken');
export var ConfigToken = new InjectionToken('DON_APPTOKEN');
var MockHttp = (function () {
    function MockHttp() {
    }
    return MockHttp;
}());
export { MockHttp };
// export function appConfigFactory(config, providers) {
//     return new AppConfig(config, providers);
// }
// const ERROR_HANDLE: string = 'ERROR_HANDLE';
// /**
//  * 中间数据流件  
//  */
// @Injectable()
// export class MiddlewareStoreService {
//     private middleware$: Map<string, Subject<any>> = new Map<string, Subject<any>>();
//     constructor() {
//         this.middleware$.set(ERROR_HANDLE, new Subject<HttpCode<any>>());
//     }
//     dispatchErrorHandle(error: HttpCode<any>) {
//         this.getSubject(ERROR_HANDLE).next(error);
//     }
//     public errorHandle$(time: number): Observable<any> {
//         return this.getSubject(ERROR_HANDLE).asObservable().debounceTime(time);
//     }
//     private getSubject(key: string) {
//         return this.middleware$.get(key);
//     }
// }
// /**
//  * 中间件
//  */
// @Injectable()
// export class MiddlewareService {
//     constructor(codeService: CodeService, store: MiddlewareStoreService) {
//         console.log("MiddlewareService");
//         store.errorHandle$(800).subscribe(info => {
//             console.log("MiddlewareService");
//             codeService.httpCodeHandle(info);
//         })
//     }
// }
export function nullMockHttpProvides() {
    return {};
}
//# sourceMappingURL=other.js.map