import { Config } from './../constants/constatns';
import { ModuleWithProviders } from '@angular/core';
export declare class AppCoreModule {
    /**
     * 项目配置
     * @param options
     */
    static config(options?: Config): ModuleWithProviders;
}
