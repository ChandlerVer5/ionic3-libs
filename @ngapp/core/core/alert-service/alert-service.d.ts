import { AlertController, Alert, ToastController, LoadingController, Loading } from 'ionic-angular';
/**
 * 弹出框方法
 */
export declare class AlertService {
    private alertCtrl;
    private toastCtrl;
    private loadingCtrl;
    constructor(alertCtrl: AlertController, toastCtrl: ToastController, loadingCtrl: LoadingController);
    /**
     * 加载框
     * @param param
     */
    loading(param: string): Loading;
    loading(param: {
        content: string;
        duration?: number;
    }): any;
    /**
     * 吐司
     *
     * @param tip
     */
    toast(tip: string): void;
    toast(tip: {
        duration?: number;
        message: string;
        position?: "top" | "middle" | "bottom";
        fn?: Function;
    }): void;
    _alert: Alert;
    /**
     * 弹出框
     */
    alert(tip: string): void;
    alert(tip: {
        title?: string;
        message: string;
        fn?: {
            text: string;
            fn?: Function;
        } | Function;
    }): void;
    alert(tip: {
        title?: string;
        message: string;
        success: {
            text: string;
            fn?: Function;
        } | Function;
        error?: {
            text: string;
            fn?: Function;
        } | Function;
    }): any;
}
