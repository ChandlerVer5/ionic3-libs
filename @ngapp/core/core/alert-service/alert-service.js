var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { AlertController, ToastController, LoadingController } from 'ionic-angular';
import { Injectable } from '@angular/core';
/**
 * 弹出框方法
 */
var AlertService = (function () {
    function AlertService(alertCtrl, toastCtrl, loadingCtrl) {
        this.alertCtrl = alertCtrl;
        this.toastCtrl = toastCtrl;
        this.loadingCtrl = loadingCtrl;
        this._alert = null;
    }
    AlertService.prototype.loading = function (param) {
        if (typeof param == 'string') {
            param = { content: param, duration: 3000 };
        }
        var loading = this.loadingCtrl.create(param);
        loading.present();
        return loading;
    };
    AlertService.prototype.toast = function (tip) {
        if (typeof tip == 'string') {
            tip = { message: tip, duration: 3000 };
        }
        var toast = this.toastCtrl.create({
            message: tip.message,
            duration: (typeof tip.duration == undefined) ? 3000 : tip.duration,
            position: tip.position || 'top'
        });
        toast.onDidDismiss(function () {
            tip.fn && tip.fn();
        });
        toast.present();
    };
    AlertService.prototype.alert = function (tip) {
        var buttons = [];
        if (typeof tip != 'object') {
            tip = { message: tip };
            buttons.push({ text: '确定', handler: function () { return true; } });
        }
        else {
            var fn_1 = tip.fn, success_1 = tip.success, error_1 = tip.error;
            if (typeof success_1 != 'undefined') {
                var _isSuccessFunction_1 = (typeof success_1 == 'function');
                buttons.push({
                    text: _isSuccessFunction_1 ? '确定' : (success_1.text),
                    handler: function () {
                        if (_isSuccessFunction_1) {
                            return success_1();
                        }
                        else {
                            if (success_1.fn) {
                                return success_1.fn();
                            }
                        }
                        return true;
                    }
                });
                var _isErrorFunction_1 = (typeof error_1 == 'function' || typeof error_1 == 'undefined');
                buttons.push({
                    text: _isErrorFunction_1 ? '取消' : (error_1.text),
                    handler: function () {
                        if (_isErrorFunction_1) {
                            if (error_1)
                                return error_1();
                        }
                        else {
                            if (error_1.fn) {
                                return error_1.fn();
                            }
                        }
                        return true;
                    }
                });
            }
            else {
                var _isFunction_1 = (typeof fn_1 == 'function' || typeof fn_1 == 'undefined');
                buttons.push({
                    text: _isFunction_1 ? '确定' : fn_1.text,
                    handler: function () {
                        if (_isFunction_1) {
                            if (fn_1)
                                return fn_1();
                        }
                        else {
                            if (fn_1.fn)
                                return fn_1.fn();
                        }
                        return true;
                    }
                });
            }
        }
        if (this._alert) {
            this._alert.dismiss();
            this._alert = null;
        }
        this._alert = this.alertCtrl.create({
            title: tip.title || '提示',
            message: tip.message,
            enableBackdropDismiss: false,
            buttons: buttons
        });
        this._alert.present();
    };
    AlertService = __decorate([
        Injectable(),
        __metadata("design:paramtypes", [AlertController, ToastController, LoadingController])
    ], AlertService);
    return AlertService;
}());
export { AlertService };
//# sourceMappingURL=alert-service.js.map