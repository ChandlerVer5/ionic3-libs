import { AlertController, Alert, Toast, ToastController, LoadingController, Loading } from 'ionic-angular';
import { Injectable } from '@angular/core';


/**
 * 弹出框方法
 */
@Injectable()
export class AlertService {



    constructor(private alertCtrl: AlertController, private toastCtrl: ToastController, private loadingCtrl: LoadingController) {
    }

    /**
     * 加载框
     * @param param 
     */
    loading(param: string): Loading;

    loading(param: { content: string, duration?: number })

    loading(param: any): Loading {
        if (typeof param == 'string') {
            param = { content: param, duration: 3000 };
        }
        let loading = this.loadingCtrl.create(param);
        loading.present();
        return loading;
    }

    /**
     * 吐司
     * 
     * @param tip 
     */
    toast(tip: string): void;

    toast(tip: {
        duration?: number;
        message: string;
        position?: "top" | "middle" | "bottom";
        fn?: Function;
    }): void;

    toast(tip: any) {
        if (typeof tip == 'string') {
            tip = { message: tip, duration: 3000 };
        }
        let toast: Toast = this.toastCtrl.create({
            message: tip.message,
            duration: (typeof tip.duration == undefined) ? 3000 : tip.duration,
            position: tip.position || 'top'
        });
        toast.onDidDismiss(() => {
            tip.fn && tip.fn();
        });
        toast.present();
    }

    _alert: Alert = null;

    /**
     * 弹出框
     */
    alert(tip: string): void;

    alert(tip: {
        title?: string;
        message: string;
        fn?: { text: string, fn?: Function } | Function;
    }): void;

    alert(tip: {
        title?: string;
        message: string;
        success: { text: string, fn?: Function } | Function;
        error?: { text: string, fn?: Function } | Function;
    })

    alert(tip: any): void {
        const buttons = [];

        if (typeof tip != 'object') {
            tip = { message: tip };
            buttons.push({ text: '确定', handler: () => true })
        } else {
            const { fn, success, error } = tip;
            if (typeof success != 'undefined') {
                const _isSuccessFunction = (typeof success == 'function');
                buttons.push({
                    text: _isSuccessFunction ? '确定' : (success.text),
                    handler: () => {
                        if (_isSuccessFunction) {
                            return success();
                        } else {
                            if (success.fn) {
                                return success.fn();
                            }
                        }
                        return true;
                    }
                });
                const _isErrorFunction = (typeof error == 'function' || typeof error == 'undefined');
                buttons.push({
                    text: _isErrorFunction ? '取消' : (error.text),
                    handler: () => {
                        if (_isErrorFunction) {
                            if (error)
                                return error();
                        } else {
                            if (error.fn) {
                                return error.fn();
                            }
                        }
                        return true;
                    }
                });
            } else {
                const _isFunction = (typeof fn == 'function' || typeof fn == 'undefined');
                buttons.push({
                    text: _isFunction ? '确定' : fn.text,
                    handler: () => {
                        if (_isFunction) {
                            if (fn)
                                return fn();
                        } else {
                            if (fn.fn)
                                return fn.fn();
                        }
                        return true;
                    }
                });
            }
        }

        if (this._alert) {
            this._alert.dismiss();
            this._alert = null;
        }
        this._alert = this.alertCtrl.create({
            title: tip.title || '提示',
            message: tip.message,
            enableBackdropDismiss: false,
            buttons
        });
        this._alert.present();
    }

}

