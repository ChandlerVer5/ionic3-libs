var __assign = (this && this.__assign) || Object.assign || function(t) {
    for (var s, i = 1, n = arguments.length; i < n; i++) {
        s = arguments[i];
        for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
            t[p] = s[p];
    }
    return t;
};
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { CodeService } from './../code-service/code-service';
import { AppConfig } from '../config/config';
import { Http, Headers } from '@angular/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { LoadingController, Platform } from 'ionic-angular';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/timeout';
var HttpService = (function () {
    function HttpService(config, http, loadingCtrl, plt, 
        // private middleware$: MiddlewareStoreService,
        codeService) {
        var _this = this;
        this.config = config;
        this.http = http;
        this.loadingCtrl = loadingCtrl;
        this.plt = plt;
        this.codeService = codeService;
        this.loader = null;
        /**
         *  http error  默认处理方式
         */
        this.errorHandler = function (error) {
            // this.middleware$.dispatchErrorHandle(error);
            _this.codeService.httpCodeHandle(error); //根据返回码响应
        };
        this._ApiPath = this.config.API_Path();
        this._loadingText = this.config.loadingText();
        this._TokenName = this.config.tokenName();
        this._debug = this.config.getDebug();
        var _initHeader = new Headers();
        if (this.config.getExtraHeader()) {
            _initHeader.append("version", this.config.getVersion()); //版本号
            _initHeader.append('platform', this.getPlatform()); // 平台标识
        }
        this._headers = new Headers(_initHeader); //普通请求头
        this._formDataHeaders = new Headers(_initHeader); // formdata 请求头
    }
    /**
     * 获取平台标识 1: android 2:ios 3:window
     */
    HttpService.prototype.getPlatform = function () {
        var _this = this;
        var platform = ['android', 'ios', 'window'].reduce(function (prev, cur, index) { return _this.plt.is(cur) ? (index + 1) : prev; }, 0);
        return "" + platform;
    };
    /**
     * 改变loading状态
     *
     * @param name 改变loading状态
     * @param status
     */
    HttpService.prototype.changeLoadingStatus = function (name, status) {
        if (status) {
            this.loader && this.loader.dismiss();
            this.loader = this.loadingCtrl.create({
                content: this._loadingText
            });
            this.loader.present();
        }
        else {
            if (this.loader) {
                this.loader.dismiss();
                this.loader = null;
            }
        }
    };
    /**
     * 上传文件流
     * @param url  请求地址
     * @param param  参数 FormData
     * @param token  是否携带TOKEN  默认false
     * @param loading 是否显示loading框  默认false
     * @param timeout  超时时间  15 * 1000 ms
     */
    HttpService.prototype.httpUpload = function (url, param, token, loading, timeout) {
        var _this = this;
        if (token === void 0) { token = false; }
        if (loading === void 0) { loading = false; }
        if (timeout === void 0) { timeout = this.config.HttpTimeOut; }
        loading && this.changeLoadingStatus(url, true); //加载中
        if (this.config.getExtraHeader()) {
            // this._headers.delete(this._TokenName);
            token && (this._formDataHeaders.set(this._TokenName, window.localStorage[this._TokenName]));
        }
        else {
            token && (param.append(this._TokenName, window.localStorage[this._TokenName]));
        }
        this._headers.set('Content-Type', 'multipart/form-data');
        return this.http.post(this._ApiPath + url, param, __assign({ headers: this._formDataHeaders }, (this.config.getHttpOptions()))).map(function (response) { return _this.responseHandle(response, loading, url); })
            .timeout(timeout).catch(function (error) { return _this.backErrorMsgHandle(error, loading, url); });
    };
    /**
    * post 请求
    * @param url  请求地址
    * @param param  参数
    * @param token  是否携带TOKEN  默认false
    * @param loading 是否显示loading框  默认false
    * @param timeout  超时时间  15 * 1000 ms
    */
    HttpService.prototype.httpPost = function (url, param, token, loading, timeout) {
        var _this = this;
        if (param === void 0) { param = {}; }
        if (token === void 0) { token = false; }
        if (loading === void 0) { loading = false; }
        if (timeout === void 0) { timeout = this.config.HttpTimeOut; }
        loading && this.changeLoadingStatus(url, true); //加载中
        if (this.config.getExtraHeader()) {
            // this._headers.delete(this._TokenName);
            token && (this._headers.set(this._TokenName, window.localStorage[this._TokenName]));
        }
        else {
            token && (param[this._TokenName] = window.localStorage[this._TokenName]); // 是否添加TOKEN    
        }
        if (this._debug) {
            var result = this.config.post(url, Object.assign({}, param, { token: window.localStorage[this._TokenName] }));
            if (result) {
                return result.map(function (response) { return _this.responseHandle({ json: function () { return response; } }, loading, url); })
                    .catch(function (error) { return _this.backErrorMsgHandle(error, loading, url); });
            }
        }
        this._headers.set('Content-Type', 'application/json');
        return this.http.post(this._ApiPath + url, param, __assign({ headers: this._headers }, (this.config.getHttpOptions()))).map(function (response) { return _this.responseHandle(response, loading, url); })
            .timeout(timeout)
            .catch(function (error) { return _this.backErrorMsgHandle(error, loading, url); });
    };
    /**
    * get 请求
    * @param url  请求地址
    * @param token  是否携带TOKEN  默认false 只有添加额外请求的时候才会加
    * @param loading 是否显示loading框  默认false
    * @param timeout  超时时间  15 * 1000 ms
    */
    HttpService.prototype.httpGet = function (url, token, loading, timeout) {
        var _this = this;
        if (token === void 0) { token = false; }
        if (loading === void 0) { loading = false; }
        if (timeout === void 0) { timeout = this.config.HttpTimeOut; }
        loading && this.changeLoadingStatus(url, true); //加载中
        if (this.config.getExtraHeader()) {
            // this._headers.delete(this._TokenName);
            token && (this._headers.set(this._TokenName, window.localStorage[this._TokenName]));
        }
        if (this._debug) {
            var result = this.config.get(url, {});
            if (result) {
                return result.map(function (response) { return _this.responseHandle({ json: function () { return response; } }, loading, url); })
                    .catch(function (error) { return _this.backErrorMsgHandle(error, loading, url); });
            }
        }
        return this.http.get(this._ApiPath + url, {
            headers: this._headers
        }).map(function (response) { return _this.responseHandle(response, loading, url); })
            .timeout(timeout)
            .catch(function (error) { return _this.backErrorMsgHandle(error, loading, url); });
    };
    /**
     * 判断 HTTP 错误类型
     *
     * @param error
     */
    HttpService.prototype.backErrorMsgHandle = function (error, loading, url) {
        loading && this.changeLoadingStatus(url, false); //加载中
        if (error.error) {
            return Observable.throw(error.error);
        }
        var data = { status: error.status, error: error };
        if (error.status == '404') {
            return Observable.throw({ code: '404', message: '加载数据异常', data: data });
        }
        if (error.status == '500') {
            return Observable.throw({ code: '500', message: '服务器异常', data: data });
        }
        if (error.status == '0') {
            return Observable.throw({ code: '0', message: '网络异常', data: data });
        }
        if (error.name == "TimeoutError") {
            return Observable.throw({ code: '506', message: '服务器请求超时', data: data });
        }
        return Observable.throw({ code: '600', message: '服务器或者网络出现异常', data: data });
    };
    /**
     * Http 返回数据处理
     * @param response
     * @param loading
     * @param url
     */
    HttpService.prototype.responseHandle = function (response, loading, url) {
        loading && this.changeLoadingStatus(url, false); //加载中
        var httpCode = response.json();
        if (httpCode.code != this.config.SuccessCode) {
            throw Observable.throw(httpCode);
        }
        return httpCode;
    };
    HttpService = __decorate([
        Injectable(),
        __metadata("design:paramtypes", [AppConfig,
            Http,
            LoadingController,
            Platform,
            CodeService])
    ], HttpService);
    return HttpService;
}());
export { HttpService };
//# sourceMappingURL=http-service.js.map