import { HttpCode } from './../constants/constatns';
import { CodeService } from './../code-service/code-service';
import { AppConfig } from '../config/config';
import { Http, Headers } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { LoadingController, Loading, Platform } from 'ionic-angular';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/timeout';
export declare class HttpService {
    config: AppConfig;
    http: Http;
    loadingCtrl: LoadingController;
    plt: Platform;
    codeService: CodeService;
    _ApiPath: string;
    _loadingText: string;
    loader: Loading;
    _TokenName: string;
    _debug: boolean;
    _headers: Headers;
    _formDataHeaders: Headers;
    constructor(config: AppConfig, http: Http, loadingCtrl: LoadingController, plt: Platform, codeService: CodeService);
    /**
     * 获取平台标识 1: android 2:ios 3:window
     */
    private getPlatform();
    /**
     * 改变loading状态
     *
     * @param name 改变loading状态
     * @param status
     */
    private changeLoadingStatus(name, status);
    /**
     * 上传文件流
     * @param url  请求地址
     * @param param  参数 FormData
     * @param token  是否携带TOKEN  默认false
     * @param loading 是否显示loading框  默认false
     * @param timeout  超时时间  15 * 1000 ms
     */
    httpUpload(url: any, param: FormData, token?: boolean, loading?: boolean, timeout?: number): Observable<HttpCode<any>>;
    /**
    * post 请求
    * @param url  请求地址
    * @param param  参数
    * @param token  是否携带TOKEN  默认false
    * @param loading 是否显示loading框  默认false
    * @param timeout  超时时间  15 * 1000 ms
    */
    httpPost(url: any, param?: any, token?: boolean, loading?: boolean, timeout?: number): Observable<HttpCode<any>>;
    /**
    * get 请求
    * @param url  请求地址
    * @param token  是否携带TOKEN  默认false 只有添加额外请求的时候才会加
    * @param loading 是否显示loading框  默认false
    * @param timeout  超时时间  15 * 1000 ms
    */
    httpGet(url: any, token?: boolean, loading?: boolean, timeout?: number): Observable<HttpCode<any>>;
    /**
     * 判断 HTTP 错误类型
     *
     * @param error
     */
    private backErrorMsgHandle(error, loading, url);
    /**
     * Http 返回数据处理
     * @param response
     * @param loading
     * @param url
     */
    private responseHandle(response, loading, url);
    /**
     *  http error  默认处理方式
     */
    errorHandler: (error: HttpCode<any>) => void;
}
