// import { MiddlewareStoreService } from './../core/other';
import { HttpCode } from './../constants/constatns';
import { CodeService } from './../code-service/code-service';
import { AppConfig } from '../config/config';
import { Http, URLSearchParams, Headers } from '@angular/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';

import { LoadingController, Loading, Platform } from 'ionic-angular';

import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/timeout';


@Injectable()
export class HttpService {

    _ApiPath: string;
    _loadingText: string;
    loader: Loading = null;
    _TokenName: string;
    _debug: boolean;
    _headers: Headers;
    _formDataHeaders: Headers;

    constructor(
        public config: AppConfig,
        public http: Http,
        public loadingCtrl: LoadingController,
        public plt: Platform,
        // private middleware$: MiddlewareStoreService,
        public codeService: CodeService
    ) {

        this._ApiPath = this.config.API_Path();
        this._loadingText = this.config.loadingText();
        this._TokenName = this.config.tokenName();
        this._debug = this.config.getDebug();
        const _initHeader = new Headers();
        if (this.config.getExtraHeader()) {
            _initHeader.append("version", this.config.getVersion());//版本号
            _initHeader.append('platform', this.getPlatform()); // 平台标识
        }

        this._headers = new Headers(_initHeader);//普通请求头
        this._formDataHeaders = new Headers(_initHeader);// formdata 请求头
    }

    /**
     * 获取平台标识 1: android 2:ios 3:window
     */
    private getPlatform():string{
        const platform = ['android', 'ios', 'window'].reduce((prev, cur, index) => this.plt.is(cur) ? (index + 1) : prev, 0);
        return `${platform}`;
    }

    /**
     * 改变loading状态
     * 
     * @param name 改变loading状态
     * @param status 
     */
    private changeLoadingStatus(name: string, status: boolean) {
        if (status) {
            this.loader && this.loader.dismiss();
            this.loader = this.loadingCtrl.create({
                content: this._loadingText
            });
            this.loader.present();
        } else {
            if (this.loader) {
                this.loader.dismiss();
                this.loader = null;
            }
        }
    }

    /**
     * 上传文件流
     * @param url  请求地址
     * @param param  参数 FormData
     * @param token  是否携带TOKEN  默认false
     * @param loading 是否显示loading框  默认false
     * @param timeout  超时时间  15 * 1000 ms
     */
    public httpUpload(url, param: FormData, token: boolean = false, loading: boolean = false, timeout: number = this.config.HttpTimeOut): Observable<HttpCode<any>> {
        loading && this.changeLoadingStatus(url, true);//加载中
        if (this.config.getExtraHeader()) { //额外请求头
            // this._headers.delete(this._TokenName);
            token && (this._formDataHeaders.set(this._TokenName, window.localStorage[this._TokenName]));
        } else {
            token && (param.append(this._TokenName, window.localStorage[this._TokenName]));
        }

        this._headers.set('Content-Type', 'multipart/form-data');
        
        return this.http.post(this._ApiPath + url, param, {
            headers: this._formDataHeaders,
            ...(this.config.getHttpOptions())
        }).map(response => this.responseHandle(response, loading, url))
            .timeout(timeout).catch(error => this.backErrorMsgHandle(error, loading, url));
    }

    /**
    * post 请求
    * @param url  请求地址
    * @param param  参数
    * @param token  是否携带TOKEN  默认false
    * @param loading 是否显示loading框  默认false
    * @param timeout  超时时间  15 * 1000 ms
    */
    public httpPost(url, param: any = {}, token: boolean = false, loading: boolean = false, timeout: number = this.config.HttpTimeOut): Observable<HttpCode<any>> {
        loading && this.changeLoadingStatus(url, true);//加载中

        if (this.config.getExtraHeader()) { //额外请求头
            // this._headers.delete(this._TokenName);
            token && (this._headers.set(this._TokenName, window.localStorage[this._TokenName]));
        } else {
            token && (param[this._TokenName] = window.localStorage[this._TokenName]); // 是否添加TOKEN    
        }

        if (this._debug) {
            let result = this.config.post(url, Object.assign({}, param, { token: window.localStorage[this._TokenName] }));
            if (result) {
                return result.map(response => this.responseHandle({ json: () => response }, loading, url))
                    .catch(error => this.backErrorMsgHandle(error, loading, url));
            }

        }

        this._headers.set('Content-Type', 'application/json');

        return this.http.post(this._ApiPath + url, param, {
            headers: this._headers,
            ...(this.config.getHttpOptions())
        }).map(response => this.responseHandle(response, loading, url))
            .timeout(timeout)
            .catch(error => this.backErrorMsgHandle(error, loading, url));
    }

    /**
    * get 请求
    * @param url  请求地址
    * @param token  是否携带TOKEN  默认false 只有添加额外请求的时候才会加 
    * @param loading 是否显示loading框  默认false
    * @param timeout  超时时间  15 * 1000 ms
    */
    public httpGet(url, token: boolean = false, loading: boolean = false, timeout: number = this.config.HttpTimeOut): Observable<HttpCode<any>> {
        loading && this.changeLoadingStatus(url, true);//加载中

        if (this.config.getExtraHeader()) {//额外请求头
            // this._headers.delete(this._TokenName);
            token && (this._headers.set(this._TokenName, window.localStorage[this._TokenName]));
        }

        if (this._debug) {
            let result = this.config.get(url, {});
            if (result) {
                return result.map(response => this.responseHandle({ json: () => response }, loading, url))
                    .catch(error => this.backErrorMsgHandle(error, loading, url));
            }
        }
        return this.http.get(this._ApiPath + url, {
            headers: this._headers
        }).map(response => this.responseHandle(response, loading, url))
            .timeout(timeout)
            .catch(error => this.backErrorMsgHandle(error, loading, url));
    }




    /**
     * 判断 HTTP 错误类型
     * 
     * @param error 
     */
    private backErrorMsgHandle(error: any, loading: boolean, url: string) {
        loading && this.changeLoadingStatus(url, false);//加载中
        if (error.error) {
            return Observable.throw(error.error);
        }
        const data = { status: error.status, error };
        if (error.status == '404') {
            return Observable.throw({ code: '404', message: '加载数据异常', data });
        }
        if (error.status == '500') {
            return Observable.throw({ code: '500', message: '服务器异常', data });
        }
        if (error.status == '0') {
            return Observable.throw({ code: '0', message: '网络异常', data });
        }
        if (error.name == "TimeoutError") {
            return Observable.throw({ code: '506', message: '服务器请求超时', data });
        }

        return Observable.throw({ code: '600', message: '服务器或者网络出现异常', data });
    }

    /**
     * Http 返回数据处理
     * @param response 
     * @param loading 
     * @param url 
     */
    private responseHandle(response, loading, url) {
        loading && this.changeLoadingStatus(url, false);//加载中
        let httpCode = response.json() as HttpCode<any>;
        if (httpCode.code != this.config.SuccessCode) {
            throw Observable.throw(httpCode);
        }
        return httpCode;
    }



    /**
     *  http error  默认处理方式
     */
    public errorHandler = (error: HttpCode<any>) => {
        // this.middleware$.dispatchErrorHandle(error);
        this.codeService.httpCodeHandle(error);//根据返回码响应
    }
}
