/** @hidden */
export function isBoolean(val) { return typeof val === 'boolean'; }
/** @hidden */
export function isString(val) { return typeof val === 'string'; }
/** @hidden */
export function isNumber(val) { return typeof val === 'number'; }
/** @hidden */
export function isFunction(val) { return typeof val === 'function'; }
/** @hidden */
export function isDefined(val) { return typeof val !== 'undefined'; }
/** @hidden */
export function isUndefined(val) { return typeof val === 'undefined'; }
/** @hidden */
export function isPresent(val) { return val !== undefined && val !== null; }
/** @hidden */
export function isBlank(val) { return val === undefined || val === null; }
/** @hidden */
export function isObject(val) { return typeof val === 'object'; }
/** @hidden */
export function isArray(val) { return Array.isArray(val); }
//# sourceMappingURL=utils.js.map