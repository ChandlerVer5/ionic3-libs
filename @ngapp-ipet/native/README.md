ShareType
# @ngapp/native

## 安装

    npm i @ngapp/native --save

## 使用

在app.module.ts 导入 NativeModule

     imports: [
        NativeModule.config({
	      qq: {
	        qqappid: ''
	      },//非必须
	      weChat: {
	        wxappsecret: '',
	        wxappid: ''
	      },//非必须
	      weiBo: {
	        wbappkey: '',
	        wbsecret: ''
	      },//非必须
		  isDebug:true, // 默认true 浏览器调试会打印回调虚假数据
		  callback: { //isDebug:true, 默认回调数据 
        	xviewLocation: { 'code': '0', data: { longitude: '0.00', latitude: '0.00' } },
		  }
	    }),
     ]

>  使用指令交互,需要在交互页面的 page.module.ts 需要引入 NativeModule

	imports: [
        NativeModule,
    ]

	 

## 1.极光 JPushService

    1.xviewListenJPush$() //订阅推送监听

    2.xviewReceiveJPush$() //订阅点击监听

    3.jiGuangTuiSong(alias: string) //设置别名

    4.cancelJPushAlias(alias?: string) //取消别名

	//demo.js

	constructor(jpush: JPushService) {

    }
    
## 2.支付分享登陆

### (1) PayShareLogin

	1.weChatLogin(): Promise<Result<LoginInfo>> //微信登陆

	2.weiboLogin(): Promise<Result<LoginInfo>> //微博登陆

	3.qqLogin(): Promise<Result<LoginInfo>> //qq 登陆

	4.aliPay(alipaydata: string): Promise<Result<AliPayCb>> //阿里支付

	5.weChatPay(param: WeChatPay): Promise<Result<any>> //微信支付

	6.lianlianPay(param: LianLianPay): Promise<Result<any>> //连连支付

	7.wechatShareToFriend(param, sharetype: ShareType) //分享给好友

	8.wechatShareToCircle(param, sharetype: ShareType) //分享朋友圈

	9.qqShareToFriend(param, sharetype: ShareType) //分享QQ好友

	10.qqShareToZone(param, sharetype: ShareType) //分享QQ空间

	11.weiboShareTo(param, sharetype: ShareType) //分享微博

>   ShareType = 'web' | 'text' | 'image' | 'audio' | 'video'; 

	//demo.js

	constructor(payShareLogin: PayShareLogin) {

    }

	weChatLogin(){
		this.payShareLogin.weChatLogin().then(info => alert(JSON.stringify(info)));
	}

	...

### (2) 指令交互

	1. (wechat-login)="login($event)" checkWechat  //checkWechat 添加该属性会检测是否有微信 没有就影藏该dom

	2. (qq-login)="login($event)"

	3. (weibo-login)="login($event)"


## 3.MediaService

#### (1) MediaService

	1.scanQRCode() //扫描二维码

	2.xviewSaveImageToGallery() //水印图片

	3.xviewRecordVideo() //录制视频 

	4.xviewUploadVideo() //上传视频 

	5.xviewUploadImage() //上传图片

#### (2) 指令交互

	1. (scan-QRcode) = "text($event)"

	text(e){
		console.log(e); 扫描后的二维码数据
	}


## 4.其他手机交互 
	
#### (1).PhoneUtilsService
	
	1.xviewCleanCache() //清除缓存

	2.xviewCallPhone(phone:string) //拨打手机号

	3.getAppVersion():Promise<AppVersion> //获取手机号 

	4.xviewSetStatusBar(color: string) //IOS修改状态栏字体颜色 "white/black"

	5.xviewLocation() //获取定位信息

	6.xviewOpenKeepLocation() //持续定位

	7.xviewStopKeepLocation() //关闭持续定位

	6.pushWebView(param: {
        "url": string;
        "type": string;
        "title": string;
        "color": string;
    }) //跳转到web view

	7.xviewNavigation(data,type:NavigationType) //开车导航

	8.pushWebView() //跳到webview

	9.xviewExitApp(type:string = 'finish') //finish 结束APP 否则最小化 默认 finish

	10.xviewGetAddressBook() //获取电话本

	11.registerBackEvent(appCtrl:App, callback?: Function) // 处理ionic 安卓返回事件
	
>   NavigationType = "driving" | "walking" | "riding";
	
#### (2).指令交互

	1.(call-phone)="$event()" [phone]="手机号"
	//或者
	(call-phone)="demo($event)" [phone]="手机号"
	
	demo(cb){
		cb && cb(phone:string,extra: string = 'tel:')
	}
	
	//拨打手机号 

	2.(clean-cache)="$event()" 
	//清除缓存

	3. ios-bar-color = 'white/black'  //IOS修改状态栏字体颜色

## 5.融云交互

#### (1).RongCloudService 

	1.xviewRongCloudLogin //登陆

	2.xviewRongCloudLogout // 退出登录

	3.xviewCurrentSessionList //会话列表

	4.xviewPushSessionView //进入会话

	5.xviewDeleteSession //删除会话

	6.xviewPop // 移除 

	7.xviewRefreshSessionList$() //实时会话

	8.xviewLoginKicked$() // 踢下线掉的方法

#### (2).指令交互

>   暂无

## 6.蓝牙模块

#### (1).BlueToothService

	1.xviewLinkBLE

	2.xviewSendCommandToBLE

[RongCloudService 参考文档](http://www.xview360.com/rongyun.html)


### 后续loading



