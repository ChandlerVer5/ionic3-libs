import { Config } from './constants';
import { ModuleWithProviders } from '@angular/core';
export declare class NativeModule {
    static config(ops: Config): ModuleWithProviders;
}
