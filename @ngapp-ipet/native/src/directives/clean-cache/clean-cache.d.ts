import { PhoneUtilsService } from './../../service/phone-service/phoneUtils-service';
import { EventEmitter } from '@angular/core';
/**
 * 拨打电话
 */
export declare class CleanCache {
    phoneUtils: PhoneUtilsService;
    cleanCache: EventEmitter<Function>;
    constructor(phoneUtils: PhoneUtilsService);
    callback(e: Event): void;
}
