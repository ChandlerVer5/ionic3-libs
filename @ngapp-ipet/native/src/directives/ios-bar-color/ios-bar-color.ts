import { PhoneUtilsService } from '../../service/phone-service/phoneUtils-service';

import { Directive, Input } from '@angular/core';

@Directive({
    selector: '[ios-bar-color]'
})
export class IosBarColor {

    @Input('ios-bar-color') iosBarColor: string;

    constructor(public phoneUtils: PhoneUtilsService) {

    }

    ngOnInit()  {
        this.phoneUtils.xviewSetStatusBar(this.iosBarColor);
    }
}