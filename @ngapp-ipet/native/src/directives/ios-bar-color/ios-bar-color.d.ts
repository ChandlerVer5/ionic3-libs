import { PhoneUtilsService } from './../../service/phone-service/phoneUtils-service';
export declare class IosBarColor {
    phoneUtils: PhoneUtilsService;
    iosBarColor: string;
    constructor(phoneUtils: PhoneUtilsService);
    ngOnInit(): void;
}
