import { PayShareLogin } from './../../service/pay-share-login/pay-share-login';
import { EventEmitter } from '@angular/core';
export declare class QQLogin {
    payShareLogin: PayShareLogin;
    qqLogin: EventEmitter<any>;
    constructor(payShareLogin: PayShareLogin);
    login(e: Event): void;
}
