import { PhoneUtilsService } from './../../service/phone-service/phoneUtils-service';
import { EventEmitter } from '@angular/core';
/**
 * 拨打电话
 */
export declare class CallPhone {
    phoneUtils: PhoneUtilsService;
    callPhone: EventEmitter<Function>;
    phone: string;
    constructor(phoneUtils: PhoneUtilsService);
    callback(e: Event): void;
}
