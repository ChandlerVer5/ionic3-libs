var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { PhoneUtilsService } from './../../service/phone-service/phoneUtils-service';
import { Directive, Output, HostListener, EventEmitter, Input } from '@angular/core';
/**
 * 拨打电话
 */
var CallPhone = (function () {
    function CallPhone(phoneUtils) {
        this.phoneUtils = phoneUtils;
        this.callPhone = new EventEmitter();
    }
    CallPhone.prototype.callback = function (e) {
        var _this = this;
        e && e.stopPropagation();
        var that = this;
        this.callPhone.emit(function (phone) {
            if (phone === void 0) { phone = _this.phone; }
            that.phoneUtils.xviewCallPhone(phone);
        });
    };
    __decorate([
        Output('call-phone'),
        __metadata("design:type", EventEmitter)
    ], CallPhone.prototype, "callPhone", void 0);
    __decorate([
        Input(),
        __metadata("design:type", String)
    ], CallPhone.prototype, "phone", void 0);
    __decorate([
        HostListener('click', ['$event']),
        __metadata("design:type", Function),
        __metadata("design:paramtypes", [Event]),
        __metadata("design:returntype", void 0)
    ], CallPhone.prototype, "callback", null);
    CallPhone = __decorate([
        Directive({
            selector: '[call-phone]'
        }),
        __metadata("design:paramtypes", [PhoneUtilsService])
    ], CallPhone);
    return CallPhone;
}());
export { CallPhone };
//# sourceMappingURL=call-phone.js.map