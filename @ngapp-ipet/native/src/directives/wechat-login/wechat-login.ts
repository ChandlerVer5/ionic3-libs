import { PayShareLogin } from '../../service/pay-share-login/pay-share-login';
import { Directive, EventEmitter, HostListener, Output, Input, ElementRef } from '@angular/core';
@Directive({
    selector: '[wechat-login]'
})
export class WechatLogin {

    @Output('wechat-login') wechatLogin: EventEmitter<any> = new EventEmitter<any>();

    _type: boolean = false; //检测是否有微信 并隐藏该dom 

    @Input('checkWechat')
    set checkWechat(type: string) {
        this._type = (type != 'false');
    }

    constructor(public payShareLogin: PayShareLogin, private _elementRef: ElementRef) { }


    ngOnInit() {
        if (this._type) {
            this.payShareLogin.xviewIsWXAppInstalled().then(info => {
                if (info.code == '-1') {
                    this._elementRef.nativeElement.style['display'] = 'none';
                }
            });
        }
    }

    @HostListener('click', ['$event'])
    login(e: Event) {
        e && e.stopPropagation();
        const that = this;
        that.payShareLogin.weChatLogin().then(info => {
            that.wechatLogin.emit(info);
        });
    }

}