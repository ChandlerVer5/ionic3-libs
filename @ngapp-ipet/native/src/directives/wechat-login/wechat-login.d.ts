import { PayShareLogin } from './../../service/pay-share-login/pay-share-login';
import { EventEmitter, ElementRef } from '@angular/core';
export declare class WechatLogin {
    payShareLogin: PayShareLogin;
    private _elementRef;
    wechatLogin: EventEmitter<any>;
    _type: boolean;
    checkWechat: string;
    constructor(payShareLogin: PayShareLogin, _elementRef: ElementRef);
    ngOnInit(): void;
    login(e: Event): void;
}
