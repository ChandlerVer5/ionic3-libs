var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { PayShareLogin } from './../../service/pay-share-login/pay-share-login';
import { Directive, EventEmitter, HostListener, Output, Input, ElementRef } from '@angular/core';
var WechatLogin = (function () {
    function WechatLogin(payShareLogin, _elementRef) {
        this.payShareLogin = payShareLogin;
        this._elementRef = _elementRef;
        this.wechatLogin = new EventEmitter();
        this._type = false; //检测是否有微信 并隐藏该dom 
    }
    Object.defineProperty(WechatLogin.prototype, "checkWechat", {
        set: function (type) {
            this._type = (type != 'false');
        },
        enumerable: true,
        configurable: true
    });
    WechatLogin.prototype.ngOnInit = function () {
        var _this = this;
        if (this._type) {
            this.payShareLogin.xviewIsWXAppInstalled().then(function (info) {
                if (info.code == '-1') {
                    _this._elementRef.nativeElement.style['display'] = 'none';
                }
            });
        }
    };
    WechatLogin.prototype.login = function (e) {
        e && e.stopPropagation();
        var that = this;
        that.payShareLogin.weChatLogin().then(function (info) {
            that.wechatLogin.emit(info);
        });
    };
    __decorate([
        Output('wechat-login'),
        __metadata("design:type", EventEmitter)
    ], WechatLogin.prototype, "wechatLogin", void 0);
    __decorate([
        Input('checkWechat'),
        __metadata("design:type", String),
        __metadata("design:paramtypes", [String])
    ], WechatLogin.prototype, "checkWechat", null);
    __decorate([
        HostListener('click', ['$event']),
        __metadata("design:type", Function),
        __metadata("design:paramtypes", [Event]),
        __metadata("design:returntype", void 0)
    ], WechatLogin.prototype, "login", null);
    WechatLogin = __decorate([
        Directive({
            selector: '[wechat-login]'
        }),
        __metadata("design:paramtypes", [PayShareLogin, ElementRef])
    ], WechatLogin);
    return WechatLogin;
}());
export { WechatLogin };
//# sourceMappingURL=wechat-login.js.map