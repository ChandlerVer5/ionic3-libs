var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { RongCloudService } from './../../service/rong-cloud/rong-cloud';
import { Directive, Output, EventEmitter, HostListener, Input } from '@angular/core';
var PopView = (function () {
    function PopView(rongCloudService) {
        this.rongCloudService = rongCloudService;
        this.popView = new EventEmitter();
        this._panelType = '0'; //1: 融云
    }
    Object.defineProperty(PopView.prototype, "rongCloud", {
        get: function () {
            return this._rongCloud;
        },
        set: function (_rongCloud) {
            this._panelType = '1';
            var sessionId = _rongCloud.sessionId, sessionType = _rongCloud.sessionType;
            this._rongCloud = { sessionId: sessionId, sessionType: sessionType };
        },
        enumerable: true,
        configurable: true
    });
    PopView.prototype.callback = function (e) {
        var _this = this;
        e && e.stopPropagation();
        switch (this._panelType) {
            case '1':
                {
                    this.popView.emit(function () {
                        _this.rongCloudService.xviewDeleteSession(_this._rongCloud);
                    });
                }
                ;
                break;
            default:
                break;
        }
    };
    __decorate([
        Output('pop-view'),
        __metadata("design:type", EventEmitter)
    ], PopView.prototype, "popView", void 0);
    __decorate([
        Input('rong-cloud'),
        __metadata("design:type", Object),
        __metadata("design:paramtypes", [Object])
    ], PopView.prototype, "rongCloud", null);
    __decorate([
        HostListener('click', ['$event']),
        __metadata("design:type", Function),
        __metadata("design:paramtypes", [Event]),
        __metadata("design:returntype", void 0)
    ], PopView.prototype, "callback", null);
    PopView = __decorate([
        Directive({
            selector: '[pop-view]'
        }),
        __metadata("design:paramtypes", [RongCloudService])
    ], PopView);
    return PopView;
}());
export { PopView };
//# sourceMappingURL=pop-view.js.map