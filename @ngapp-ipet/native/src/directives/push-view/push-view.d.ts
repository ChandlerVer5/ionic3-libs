import { RongCloudSessionPane } from './../../constants';
import { RongCloudService } from './../../service/rong-cloud/rong-cloud';
import { EventEmitter } from '@angular/core';
export declare class PushView {
    rongCloudService: RongCloudService;
    pushView: EventEmitter<Function>;
    _rongCloud: RongCloudSessionPane;
    _panelType: string;
    rongCloud: RongCloudSessionPane;
    constructor(rongCloudService: RongCloudService);
    callback(e: Event): void;
}
