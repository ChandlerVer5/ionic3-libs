import { MediaService } from './../../service/media-service/media-service';
import { EventEmitter } from '@angular/core';
/**
 * 扫描二维码
 */
export declare class ScanQRCode {
    mediaService: MediaService;
    scanQRCode: EventEmitter<any>;
    constructor(mediaService: MediaService);
    callBack(e: Event): void;
}
