var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { PayShareLogin } from './../../service/pay-share-login/pay-share-login';
import { Directive, EventEmitter, HostListener, Output, } from '@angular/core';
var WeiboLogin = (function () {
    function WeiboLogin(payShareLogin) {
        this.payShareLogin = payShareLogin;
        this.weiboLogin = new EventEmitter();
    }
    WeiboLogin.prototype.login = function (e) {
        e && e.stopPropagation();
        var that = this;
        that.payShareLogin.weiboLogin().then(function (info) {
            that.weiboLogin.emit(info);
        });
    };
    __decorate([
        Output('weibo-login'),
        __metadata("design:type", EventEmitter)
    ], WeiboLogin.prototype, "weiboLogin", void 0);
    __decorate([
        HostListener('click', ['$event']),
        __metadata("design:type", Function),
        __metadata("design:paramtypes", [Event]),
        __metadata("design:returntype", void 0)
    ], WeiboLogin.prototype, "login", null);
    WeiboLogin = __decorate([
        Directive({
            selector: '[weibo-login]'
        }),
        __metadata("design:paramtypes", [PayShareLogin])
    ], WeiboLogin);
    return WeiboLogin;
}());
export { WeiboLogin };
//# sourceMappingURL=weibo-login.js.map