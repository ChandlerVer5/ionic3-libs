import { PayShareLogin } from '../../service/pay-share-login/pay-share-login';
import { Directive, EventEmitter, HostListener, Output, } from '@angular/core';
@Directive({
    selector: '[weibo-login]'
})
export class WeiboLogin {

    @Output('weibo-login') weiboLogin: EventEmitter<any> = new EventEmitter<any>();

    constructor(public payShareLogin: PayShareLogin) { }


    @HostListener('click', ['$event'])
    login(e: Event) {
        e && e.stopPropagation();
        const that = this;
        that.payShareLogin.weiboLogin().then(info => {
            that.weiboLogin.emit(info);
        });
    }

}