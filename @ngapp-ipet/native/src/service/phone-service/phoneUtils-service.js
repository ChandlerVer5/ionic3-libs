var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __assign = (this && this.__assign) || Object.assign || function(t) {
    for (var s, i = 1, n = arguments.length; i < n; i++) {
        s = arguments[i];
        for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
            t[p] = s[p];
    }
    return t;
};
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { XViewConfig } from './../xview-config/XViewConfig';
import { XView } from '../XView';
import { Subject } from 'rxjs';
import { Injectable } from '@angular/core';
var PhoneUtilsService = (function (_super) {
    __extends(PhoneUtilsService, _super);
    function PhoneUtilsService(config) {
        var _this = _super.call(this, config.appConfig) || this;
        _this.xviewObserveAppState = new Subject();
        _this.inFirstTimeToEnter = false;
        var that = _this;
        /**
         * 监听点击推送时间
         */
        window['observeAppState'] = function (param) {
            that.xviewObserveAppState.next(param);
        };
        return _this;
    }
    /**
     * 清除缓存
     */
    PhoneUtilsService.prototype.xviewCleanCache = function () {
        this.xview('xviewCleanCache');
    };
    /**
     * 调用手机号码
     * @param phone  手机号码
     */
    PhoneUtilsService.prototype.xviewCallPhone = function (tel) {
        this.xviewNative('xviewCallPhone', { tel: tel });
    };
    /**
     * 修改字体颜色
     * @param parameter  "white/black"
     */
    PhoneUtilsService.prototype.xviewSetStatusBar = function (color) {
        this.xviewNative('xviewSetStatusBar', { color: color });
    };
    /**
     * 地理定位，获取当前位置
     */
    PhoneUtilsService.prototype.xviewLocation = function () {
        return this.xviewNative('xviewLocation', {});
    };
    /**
     * 持续定位
     */
    PhoneUtilsService.prototype.xviewOpenKeepLocation = function (json) {
        return this.xviewNative('xviewOpenKeepLocation', json);
    };
    /**
     * 关闭持续定位
     */
    PhoneUtilsService.prototype.xviewStopKeepLocation = function () {
        this.xviewNative('xviewStopKeepLocation');
    };
    /**
     * 回调APP 进去前台或者后台
     * active - 后台进入前台 ;background - 前台进入后台
     */
    PhoneUtilsService.prototype.xviewObserveAppState$ = function () {
        if (!this.inFirstTimeToEnter) {
            this.xviewNative('xviewObserveAppState');
            this.inFirstTimeToEnter = true;
        }
        return this.xviewObserveAppState.asObservable();
    };
    /**
     * 复制到剪切板
     * @param param
     */
    PhoneUtilsService.prototype.xviewTextToClipboard = function (param) {
        this.xviewNative('xviewTextToClipboard', param);
    };
    /**
     * 开车导航
     * @param data
     */
    PhoneUtilsService.prototype.xviewNavigationDriving = function (data) {
        return this.xviewNavigation(data, 'driving');
    };
    /**
     * 骑车导航
     * @param data
     */
    PhoneUtilsService.prototype.xviewNavigationRiding = function (data) {
        return this.xviewNavigation(data, 'riding');
    };
    /**
     * 不行导航
     * @param data
     */
    PhoneUtilsService.prototype.xviewNavigationWalking = function (data) {
        return this.xviewNavigation(data, 'walking');
    };
    /**
     * 导航
     * @param data
     * @param type driving/riding/walking
     */
    PhoneUtilsService.prototype.xviewNavigation = function (data, type) {
        if (type === void 0) { type = 'driving'; }
        return this.xviewNative('xviewNavigation', { data: __assign({}, data, (this._config.xunFei)), type: type });
    };
    /**
     * 获取版本信息
     */
    PhoneUtilsService.prototype.getAppVersion = function () {
        return this.xviewNative('xviewAppVersion', {});
    };
    /**
     * 跳转到web view
     * @param param
     */
    PhoneUtilsService.prototype.pushWebView = function (param) {
        this.xviewNative('xviewPushWeb', __assign({}, param));
    };
    /**
     * 退出app
     * @param type  finish 结束APP 否则最小化 默认 finish
     */
    PhoneUtilsService.prototype.xviewExitApp = function (type) {
        if (type === void 0) { type = 'finish'; }
        this.xviewNative('xviewExitApp', { type: type });
    };
    /**
     * 获取电话本
     */
    PhoneUtilsService.prototype.xviewGetAddressBook = function () {
        return this.xviewNative('xviewGetAddressBook', {});
    };
    /**
     * 监听安卓物理返回键
     * @param appCtrl
     * @param callback  return true 不返回  false 返回
     */
    PhoneUtilsService.prototype.registerBackEvent = function (appCtrl, callback) {
        var _this = this;
        window['xviewWebBack'] = function () {
            if (callback) {
                if (callback()) {
                    return;
                }
            }
            var promise = appCtrl.goBack();
            if (promise) {
                promise.then(function (info) {
                    if (!info) {
                        var a = appCtrl._appRoot._getPortal();
                        a.pop();
                    }
                });
            }
            else {
                _this.xviewExitApp();
            }
        };
    };
    PhoneUtilsService = __decorate([
        Injectable(),
        __metadata("design:paramtypes", [XViewConfig])
    ], PhoneUtilsService);
    return PhoneUtilsService;
}(XView));
export { PhoneUtilsService };
//# sourceMappingURL=phoneUtils-service.js.map