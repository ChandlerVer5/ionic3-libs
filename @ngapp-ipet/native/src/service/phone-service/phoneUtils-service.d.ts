import { XViewConfig } from './../xview-config/XViewConfig';
import { XView } from '../XView';
import { AppVersion, Result, AddressLocation, NavigationType } from '../../constants/index';
import { Observable } from 'rxjs';
export declare class PhoneUtilsService extends XView {
    private xviewObserveAppState;
    constructor(config: XViewConfig);
    /**
     * 清除缓存
     */
    xviewCleanCache(): void;
    /**
     * 调用手机号码
     * @param phone  手机号码
     */
    xviewCallPhone(tel: string): void;
    /**
     * 修改字体颜色
     * @param parameter  "white/black"
     */
    xviewSetStatusBar(color: string): void;
    /**
     * 地理定位，获取当前位置
     */
    xviewLocation(): Promise<Result<AddressLocation>>;
    /**
     * 持续定位
     */
    xviewOpenKeepLocation(json: {
        locationUrl: string;
        timer?: string;
        refreshTime: string;
        timeOut: string;
    } | {
        [key: string]: string;
    }): Promise<Result<any>>;
    /**
     * 关闭持续定位
     */
    xviewStopKeepLocation(): void;
    private inFirstTimeToEnter;
    /**
     * 回调APP 进去前台或者后台
     * active - 后台进入前台 ;background - 前台进入后台
     */
    xviewObserveAppState$(): Observable<Result<{
        state: "active" | "background";
    }>>;
    /**
     * 复制到剪切板
     * @param param
     */
    xviewTextToClipboard(param: {
        text: string;
    }): void;
    /**
     * 开车导航
     * @param data
     */
    xviewNavigationDriving(data: {
        startLongitude: number | string;
        startLatitude: number | string;
        endLongitude: number | string;
        endLatitude: number | string;
    }): Promise<Result<any>>;
    /**
     * 骑车导航
     * @param data
     */
    xviewNavigationRiding(data: {
        startLongitude: number | string;
        startLatitude: number | string;
        endLongitude: number | string;
        endLatitude: number | string;
    }): Promise<Result<any>>;
    /**
     * 不行导航
     * @param data
     */
    xviewNavigationWalking(data: {
        startLongitude: number | string;
        startLatitude: number | string;
        endLongitude: number | string;
        endLatitude: number | string;
    }): Promise<Result<any>>;
    /**
     * 导航
     * @param data
     * @param type driving/riding/walking
     */
    xviewNavigation(data: {
        startLongitude: number | string;
        startLatitude: number | string;
        endLongitude: number | string;
        endLatitude: number | string;
    }, type?: NavigationType): Promise<Result<any>>;
    /**
     * 获取版本信息
     */
    getAppVersion(): Promise<Result<AppVersion>>;
    /**
     * 跳转到web view
     * @param param
     */
    pushWebView(param: {
        "url": string;
        "type": 'Web' | 'App';
        "title": string;
        "color": string;
    }): void;
    /**
     * 退出app
     * @param type  finish 结束APP 否则最小化 默认 finish
     */
    xviewExitApp(type?: string): void;
    /**
     * 获取电话本
     */
    xviewGetAddressBook(): Promise<Result<Array<{
        name: string;
        phone: string;
    }>>>;
    /**
     * 监听安卓物理返回键
     * @param appCtrl
     * @param callback  return true 不返回  false 返回
     */
    registerBackEvent(appCtrl: any, callback?: Function): void;
}
