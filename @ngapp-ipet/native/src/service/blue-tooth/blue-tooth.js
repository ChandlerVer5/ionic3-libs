var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { XView } from './../XView';
import { XViewConfig } from './../xview-config/XViewConfig';
import { Injectable } from "@angular/core";
/**
 * 蓝牙
 */
var BlueToothService = (function (_super) {
    __extends(BlueToothService, _super);
    function BlueToothService(config) {
        return _super.call(this, config.appConfig) || this;
    }
    /**
     * 连接蓝牙
     */
    BlueToothService.prototype.xviewLinkBLE = function (json) {
        if (json.bytes instanceof Array) {
            json.bytes = json.bytes.join("&");
        }
        return this.xviewNative('xviewLinkBLE', json);
    };
    /**
     * xviewSendCommandToBLE 发送命令
     */
    BlueToothService.prototype.xviewSendCommandToBLE = function (json) {
        if (json.bytes instanceof Array) {
            json.bytes = json.bytes.join("&");
        }
        return this.xviewNative('xviewSendCommandToBLE', json);
    };
    BlueToothService = __decorate([
        Injectable(),
        __metadata("design:paramtypes", [XViewConfig])
    ], BlueToothService);
    return BlueToothService;
}(XView));
export { BlueToothService };
//# sourceMappingURL=blue-tooth.js.map