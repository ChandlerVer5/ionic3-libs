import { Result } from './../../constants/constants';
import { XView } from './../XView';
import { XViewConfig } from './../xview-config/XViewConfig';
/**
 * 蓝牙
 */
export declare class BlueToothService extends XView {
    constructor(config: XViewConfig);
    /**
     * 连接蓝牙
     */
    xviewLinkBLE(json: {
        "deviceName": string;
        "deviceUUID": string;
        "notifyUUID": string;
        "writeUUID": string;
        "bytes": string | Array<string>;
    }): Promise<Result<{
        result: string;
    }>>;
    /**
     * xviewSendCommandToBLE 发送命令
     */
    xviewSendCommandToBLE(json: {
        "bytes": string | Array<string>;
        "1&1&1&1";
    }): Promise<Result<{
        result: string;
    }>>;
}
