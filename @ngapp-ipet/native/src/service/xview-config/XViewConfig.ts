import { Config, QQ, WeChat, SinaWeibo, XunFei } from '../../constants/constants';
import { XVIEW_CONFIG } from '../../constants/config';
import { Injectable, Inject } from '@angular/core';



@Injectable()
export class XViewConfig {

    config: Config;

    constructor( @Inject(XVIEW_CONFIG) config: Config) {
        this.config = Object.assign({ isDebug: true }, config);
    }


    get appConfig(): Config {
        return this.config;
    }

    get wechatConfig(): WeChat {
        return this.config.weChat;
    }

    get qqConfig(): QQ {
        return this.config.qq;
    }

    get weiboConfig(): SinaWeibo {
        return this.config.weiBo;
    }

    get xunFeiConfig(): XunFei {
        return this.config.xunFei;
    }
}