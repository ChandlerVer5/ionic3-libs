import { Result, JPushParams } from '../../constants/constants';
import { XViewConfig } from '../xview-config/XViewConfig';
import { XView } from '../XView';
import { Injectable } from '@angular/core';
import { Subject, Observable } from 'rxjs';



/**
 * 极光推送
 */
@Injectable()
export class JPushService extends XView {

    private xviewListenJPush: Subject<Result<JPushParams>> = new Subject<Result<JPushParams>>();

    private xviewReceiveJPush: Subject<Result<JPushParams>> = new Subject<Result<JPushParams>>();


    constructor(config: XViewConfig) {

        super(config.appConfig);

        const that = this;
        /**
         * 监听点击推送时间
         */
        window['xviewListenJPush'] = function (param: Result<JPushParams>) {
            that.xviewListenJPush.next(param);
        }

        /**
         * 监听推送并点击事件
         */
        window['xviewReceiveJPush'] = function (param: Result<JPushParams>) {
            that.xviewReceiveJPush.next(param);
        }

    }


    /**
     * 订阅: 接收到极光推送消息, 用户并没有点击,触发
     */
    public xviewListenJPush$(): Observable<Result<JPushParams>> {
        return this.xviewListenJPush.asObservable();
    }

    // /**
    //  * 更新新值到store
    //  * 
    //  * @param param 
    //  */
    // public setListenJPush(param: any): void {
    //     window['xviewListenJPush'](param);
    // }

    /**
     * 接收到极光推送消息, 用户点击触发.
     */
    public xviewReceiveJPush$(): Observable<Result<JPushParams>> {
        return this.xviewReceiveJPush.asObservable();
    }

    // /**
    //  * 更新新值到store
    //  * @param param 
    //  */
    // public setReceiveJPush(param: any): void {
    //     window['xviewReceiveJPush'](param);;
    // }


    /**
     * 设置别名
     * @param alias 
     */
    public jiGuangTuiSong(alias: object | string) {

        if (typeof alias == 'object') {
            this.xviewNative('xviewSetJPushAlias', alias);
        } else {
            this.xviewNative('xviewSetJPushAlias', { alias });
        }
    }

    /**
     * 清除别名
     * @param alias 
     */
    public cancelJPushAlias() {
        this.xviewNative('xviewCancelJPushAlias');
    }

}