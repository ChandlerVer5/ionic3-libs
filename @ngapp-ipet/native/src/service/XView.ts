import { Result, Config } from '../constants/constants';

/**
 * Native 交互方法
 */
export class XView {

    _config: Config;

    constructor(config: Config) {
        this._config = config;
    }

    public xviewNative<T>(methodName: string, param?: any): Promise<Result<T>> {
        const that = this;
        if (param) {
            param.callback = methodName + 'callback';
            const promise = new Promise<Result<T>>((resolve, reject) => {
                try {
                    window[param.callback] = function (result) {
                        const _result = (result as Result<T>);
                        _result.success = (_result.code == '0');
                        resolve(_result);
                        // delete window[param.callback];
                    }
                } catch (e) {
                    resolve({ code: '-2', message: `调用 window.${param.callback} 发生异常,错误信息${e}`, data: {}, success: false } as Result<T>);
                    console.log('====================================');
                    console.log(e.message);
                    console.log('====================================');
                }
            });
            if (that._config.isDebug) {
                console.log('====================================');
                console.log(`methodName ${methodName}`);
                console.log(param);
                console.log('====================================');
            }
            that.xview(methodName, param);
            return promise;
        } else {
            that.xview(methodName);
        }
    }



    public xview(methodName: string, param?: any) {
        // const isIOS = true;
        try {
            if (window['webkit']) {
                window['webkit'].messageHandlers[methodName].postMessage(JSON.stringify(param || {}));
            } else {
                if (param) {
                    window['xview'][methodName](JSON.stringify(param));
                } else {
                    window['xview'][methodName]();
                }
            }
        } catch (e) {
            console.warn("Not Found Method " + methodName);
            console.warn(e.message);
            if (param && this._config.isDebug && param.callback) {
                setTimeout(() => {
                    if (this._config.callback && this._config.callback[methodName]) {
                        //自定义回调
                        window[methodName + 'callback'](this._config.callback[methodName]);
                    } else {
                        window[methodName + 'callback']({
                            code: '-2',
                            message: 'debug 调试',
                            data: {
                                result: e.message
                            }
                        });
                    }
                }, 500);
            }
        }
    }

}