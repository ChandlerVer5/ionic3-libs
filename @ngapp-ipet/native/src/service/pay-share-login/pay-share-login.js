var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __assign = (this && this.__assign) || Object.assign || function(t) {
    for (var s, i = 1, n = arguments.length; i < n; i++) {
        s = arguments[i];
        for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
            t[p] = s[p];
    }
    return t;
};
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { XViewConfig } from './../xview-config/XViewConfig';
import { Injectable } from '@angular/core';
import { XView } from './../XView';
var PayShareLogin = (function (_super) {
    __extends(PayShareLogin, _super);
    function PayShareLogin(config) {
        return _super.call(this, config.config) || this;
    }
    /**
     * 微信登陆
     */
    PayShareLogin.prototype.weChatLogin = function () {
        return this.payShareLogin("weixinLogin", this._config.weChat);
    };
    /**
     * 微博登陆
     */
    PayShareLogin.prototype.weiboLogin = function () {
        return this.payShareLogin("weiboLogin", this._config.weiBo);
    };
    /**
     * QQ 登陆
     */
    PayShareLogin.prototype.qqLogin = function () {
        return this.payShareLogin("qqLogin", this._config.qq);
    };
    /**
     * 微信分享 网页
     *
     * @param param
     */
    PayShareLogin.prototype.wechatShareWebUrlToFriend = function (param) {
        return this.wechatShareToFriend(param, 'web');
    };
    /**
     * 微信 文本分享
     * @param param
     */
    PayShareLogin.prototype.wechatShareTextToFriend = function (param) {
        return this.wechatShareToFriend(param, 'text');
    };
    /**
     * 图片分享
     *
     * @param param
     */
    PayShareLogin.prototype.wechatShareImageToFriend = function (param) {
        return this.wechatShareToFriend(param, 'image');
    };
    /**
     * 分享音乐
     * @param param
     */
    PayShareLogin.prototype.wechatShareAudioToFriend = function (param) {
        return this.wechatShareToFriend(param, 'audio');
    };
    /**
     * 分享视频
     * @param param
     */
    PayShareLogin.prototype.wechatShareVideoToFriend = function (param) {
        return this.wechatShareToFriend(param, 'video');
    };
    /**
     * 分享给朋友
     * @param param
     * @param sharetype
     */
    PayShareLogin.prototype.wechatShareToFriend = function (param, sharetype) {
        return this.share("weixinShare", __assign({}, param, (this._config.weChat), { sharetype: sharetype }));
    };
    /**
    * 微信朋友圈 网页分享
    *
    * @param param
    */
    PayShareLogin.prototype.wechatShareWebUrlToCircle = function (param) {
        return this.wechatShareToCircle(param, 'web');
    };
    /**
     * 微信朋友圈 文本分享
     * @param param
     */
    PayShareLogin.prototype.wechatShareTextToCircle = function (param) {
        return this.wechatShareToCircle(param, 'text');
    };
    /**
     * 图片分享
     *
     * @param param
     */
    PayShareLogin.prototype.wechatShareImageToCircle = function (param) {
        return this.wechatShareToCircle(param, 'image');
    };
    /**
     * 分享音乐
     * @param param
     */
    PayShareLogin.prototype.wechatShareAudioToCircle = function (param) {
        return this.wechatShareToCircle(param, 'audio');
    };
    /**
     * 分享视频
     * @param param
     */
    PayShareLogin.prototype.wechatShareVideoToCircle = function (param) {
        return this.wechatShareToCircle(param, 'video');
    };
    /**
     * 朋友圈
     * @param param
     * @param sharetype
     */
    PayShareLogin.prototype.wechatShareToCircle = function (param, sharetype) {
        return this.share("weixinCircleShare", __assign({}, param, { sharetype: sharetype }, (this._config.weChat)));
    };
    /**
    * qq 网页分享
    *
    * @param param
    */
    PayShareLogin.prototype.qqShareWebUrlToFriend = function (param) {
        return this.qqShareToFriend(param, 'web');
    };
    /**
     * qq 文本分享
     * @param param
     */
    PayShareLogin.prototype.qqShareTextToFriend = function (param) {
        return this.qqShareToFriend(param, 'text');
    };
    /**
     * qq 图片分享
     *
     * @param param
     */
    PayShareLogin.prototype.qqShareImageToFriend = function (param) {
        return this.qqShareToFriend(param, 'image');
    };
    /**
     * qq 分享音乐
     * @param param
     */
    PayShareLogin.prototype.qqShareAudioToFriend = function (param) {
        return this.qqShareToFriend(param, 'audio');
    };
    /**
     * qq 分享视频
     * @param param
     */
    PayShareLogin.prototype.qqShareVideoToFriend = function (param) {
        return this.qqShareToFriend(param, 'video');
    };
    PayShareLogin.prototype.qqShareToFriend = function (param, sharetype) {
        return this.share("qqShare", __assign({}, param, (this._config.qq), { sharetype: sharetype }));
    };
    /**
    * qq 网页分享
    *
    * @param param
    */
    PayShareLogin.prototype.qqShareWebUrlToZone = function (param) {
        return this.qqShareToZone(param, 'web');
    };
    /**
     * qq 文本分享
     * @param param
     */
    PayShareLogin.prototype.qqShareTextToZone = function (param) {
        return this.qqShareToZone(param, 'text');
    };
    /**
     * qq 图片分享
     *
     * @param param
     */
    PayShareLogin.prototype.qqShareImageToZone = function (param) {
        return this.qqShareToZone(param, 'image');
    };
    /**
     * qq 分享音乐
     * @param param
     */
    PayShareLogin.prototype.qqShareAudioToZone = function (param) {
        return this.qqShareToZone(param, 'audio');
    };
    /**
     * qq 分享视频
     * @param param
     */
    PayShareLogin.prototype.qqShareVideoToZone = function (param) {
        return this.qqShareToZone(param, 'video');
    };
    /**
     * qq 空间
     * @param param
     * @param sharetype
     */
    PayShareLogin.prototype.qqShareToZone = function (param, sharetype) {
        return this.share("qqZoneShare", __assign({}, param, { sharetype: sharetype }, (this._config.qq)));
    };
    /**
    * weibo 网页分享
    *
    * @param param
    */
    PayShareLogin.prototype.weiboShareWebUrlTo = function (param) {
        return this.weiboShareTo(param, 'web');
    };
    /**
     * weibo 文本分享
     * @param param
     */
    PayShareLogin.prototype.weiboShareTextTo = function (param) {
        return this.weiboShareTo(param, 'text');
    };
    /**
     * weibo 图片分享
     *
     * @param param
     */
    PayShareLogin.prototype.weiboShareImageTo = function (param) {
        return this.weiboShareTo(param, 'image');
    };
    /**
     * weibo 分享音乐
     * @param param
     */
    PayShareLogin.prototype.weiboShareAudioTo = function (param) {
        return this.weiboShareTo(param, 'audio');
    };
    /**
     * weibo 分享视频
     * @param param
     */
    PayShareLogin.prototype.weiboShareVideoTo = function (param) {
        return this.weiboShareTo(param, 'video');
    };
    /**
     * 微博分享
     * @param param
     * @param sharetype
     */
    PayShareLogin.prototype.weiboShareTo = function (param, sharetype) {
        return this.share("weiboShare", __assign({}, param, (this._config.weiBo), { sharetype: sharetype }));
    };
    PayShareLogin.prototype.share = function (shareType, param) {
        return this.payShareLogin(shareType, param);
    };
    /**
     * 支付支付
     * @param alipaydata
     */
    PayShareLogin.prototype.aliPay = function (alipaydata) {
        return this.payShareLogin("aliPay", { alipaydata: alipaydata });
    };
    /**
     * 不传appid 默认使用 配置文件中wechat的appid
     * @param param
     */
    PayShareLogin.prototype.weChatPay = function (param) {
        if (!param.appid) {
            param.appid = this._config.weChat.wxappid;
        }
        return this.payShareLogin("weixinPay", __assign({}, param, (this._config.weChat)));
    };
    /**
     * 连连支付
     * @param param
     */
    PayShareLogin.prototype.lianlianPay = function (param) {
        return this.payShareLogin("lianlianVerifyPay", param);
    };
    /**
     * 跳到 QQ 界面
     * @param qq
     */
    PayShareLogin.prototype.xviewPushQQ = function (qq) {
        return this.xviewNative("xviewPushQQ", { qq: qq });
    };
    /**
     * 是否安装微信
     */
    PayShareLogin.prototype.xviewIsWXAppInstalled = function () {
        return this.xviewNative("xviewIsWXAppInstalled", {});
    };
    /**
     *
     * @param type
     * @param data
     */
    PayShareLogin.prototype.payShareLogin = function (type, data) {
        return this.xviewNative('xviewLoginPayShare', { type: type, data: data });
    };
    PayShareLogin = __decorate([
        Injectable(),
        __metadata("design:paramtypes", [XViewConfig])
    ], PayShareLogin);
    return PayShareLogin;
}(XView));
export { PayShareLogin };
//# sourceMappingURL=pay-share-login.js.map