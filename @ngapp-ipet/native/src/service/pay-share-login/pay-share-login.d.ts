import { XViewConfig } from './../xview-config/XViewConfig';
import { ShareType, Result, LoginInfo, AliPayCb, LianLianPay, WeChatPay, ShareWeb, ShareAudio, ShareImage, ShareText, ShareVideo } from './../../constants/constants';
import { XView } from './../XView';
export declare class PayShareLogin extends XView {
    constructor(config: XViewConfig);
    /**
     * 微信登陆
     */
    weChatLogin(): Promise<Result<LoginInfo>>;
    /**
     * 微博登陆
     */
    weiboLogin(): Promise<Result<LoginInfo>>;
    /**
     * QQ 登陆
     */
    qqLogin(): Promise<Result<LoginInfo>>;
    /**
     * 微信分享 网页
     *
     * @param param
     */
    wechatShareWebUrlToFriend(param: ShareWeb): Promise<Result<{
        "result": string;
        "type": string;
    }>>;
    /**
     * 微信 文本分享
     * @param param
     */
    wechatShareTextToFriend(param: ShareText): Promise<Result<{
        "result": string;
        "type": string;
    }>>;
    /**
     * 图片分享
     *
     * @param param
     */
    wechatShareImageToFriend(param: ShareImage): Promise<Result<{
        "result": string;
        "type": string;
    }>>;
    /**
     * 分享音乐
     * @param param
     */
    wechatShareAudioToFriend(param: ShareAudio): Promise<Result<{
        "result": string;
        "type": string;
    }>>;
    /**
     * 分享视频
     * @param param
     */
    wechatShareVideoToFriend(param: ShareVideo): Promise<Result<{
        "result": string;
        "type": string;
    }>>;
    /**
     * 分享给朋友
     * @param param
     * @param sharetype
     */
    wechatShareToFriend(param: ShareWeb | ShareAudio | ShareImage | ShareText | ShareVideo, sharetype: ShareType): Promise<Result<{
        "result": string;
        "type": string;
    }>>;
    /**
    * 微信朋友圈 网页分享
    *
    * @param param
    */
    wechatShareWebUrlToCircle(param: ShareWeb): Promise<Result<{
        "result": string;
        "type": string;
    }>>;
    /**
     * 微信朋友圈 文本分享
     * @param param
     */
    wechatShareTextToCircle(param: ShareText): Promise<Result<{
        "result": string;
        "type": string;
    }>>;
    /**
     * 图片分享
     *
     * @param param
     */
    wechatShareImageToCircle(param: ShareImage): Promise<Result<{
        "result": string;
        "type": string;
    }>>;
    /**
     * 分享音乐
     * @param param
     */
    wechatShareAudioToCircle(param: ShareAudio): Promise<Result<{
        "result": string;
        "type": string;
    }>>;
    /**
     * 分享视频
     * @param param
     */
    wechatShareVideoToCircle(param: ShareVideo): Promise<Result<{
        "result": string;
        "type": string;
    }>>;
    /**
     * 朋友圈
     * @param param
     * @param sharetype
     */
    wechatShareToCircle(param: ShareWeb | ShareAudio | ShareImage | ShareText | ShareVideo, sharetype: ShareType): Promise<Result<{
        "result": string;
        "type": string;
    }>>;
    /**
    * qq 网页分享
    *
    * @param param
    */
    qqShareWebUrlToFriend(param: ShareWeb): Promise<Result<{
        "result": string;
        "type": string;
    }>>;
    /**
     * qq 文本分享
     * @param param
     */
    qqShareTextToFriend(param: ShareText): Promise<Result<{
        "result": string;
        "type": string;
    }>>;
    /**
     * qq 图片分享
     *
     * @param param
     */
    qqShareImageToFriend(param: ShareImage): Promise<Result<{
        "result": string;
        "type": string;
    }>>;
    /**
     * qq 分享音乐
     * @param param
     */
    qqShareAudioToFriend(param: ShareAudio): Promise<Result<{
        "result": string;
        "type": string;
    }>>;
    /**
     * qq 分享视频
     * @param param
     */
    qqShareVideoToFriend(param: ShareVideo): Promise<Result<{
        "result": string;
        "type": string;
    }>>;
    qqShareToFriend(param: ShareWeb | ShareAudio | ShareImage | ShareText | ShareVideo, sharetype: ShareType): Promise<Result<{
        "result": string;
        "type": string;
    }>>;
    /**
    * qq 网页分享
    *
    * @param param
    */
    qqShareWebUrlToZone(param: ShareWeb): Promise<Result<{
        "result": string;
        "type": string;
    }>>;
    /**
     * qq 文本分享
     * @param param
     */
    qqShareTextToZone(param: ShareText): Promise<Result<{
        "result": string;
        "type": string;
    }>>;
    /**
     * qq 图片分享
     *
     * @param param
     */
    qqShareImageToZone(param: ShareImage): Promise<Result<{
        "result": string;
        "type": string;
    }>>;
    /**
     * qq 分享音乐
     * @param param
     */
    qqShareAudioToZone(param: ShareAudio): Promise<Result<{
        "result": string;
        "type": string;
    }>>;
    /**
     * qq 分享视频
     * @param param
     */
    qqShareVideoToZone(param: ShareVideo): Promise<Result<{
        "result": string;
        "type": string;
    }>>;
    /**
     * qq 空间
     * @param param
     * @param sharetype
     */
    qqShareToZone(param: ShareWeb | ShareAudio | ShareImage | ShareText | ShareVideo, sharetype: string): Promise<Result<{
        "result": string;
        "type": string;
    }>>;
    /**
    * weibo 网页分享
    *
    * @param param
    */
    weiboShareWebUrlTo(param: ShareWeb): Promise<Result<{
        "result": string;
        "type": string;
    }>>;
    /**
     * weibo 文本分享
     * @param param
     */
    weiboShareTextTo(param: ShareText): Promise<Result<{
        "result": string;
        "type": string;
    }>>;
    /**
     * weibo 图片分享
     *
     * @param param
     */
    weiboShareImageTo(param: ShareImage): Promise<Result<{
        "result": string;
        "type": string;
    }>>;
    /**
     * weibo 分享音乐
     * @param param
     */
    weiboShareAudioTo(param: ShareAudio): Promise<Result<{
        "result": string;
        "type": string;
    }>>;
    /**
     * weibo 分享视频
     * @param param
     */
    weiboShareVideoTo(param: ShareVideo): Promise<Result<{
        "result": string;
        "type": string;
    }>>;
    /**
     * 微博分享
     * @param param
     * @param sharetype
     */
    weiboShareTo(param: ShareWeb | ShareAudio | ShareImage | ShareText | ShareVideo, sharetype: ShareType): Promise<Result<{
        "result": string;
        "type": string;
    }>>;
    private share(shareType, param);
    /**
     * 支付支付
     * @param alipaydata
     */
    aliPay(alipaydata: string): Promise<Result<AliPayCb>>;
    /**
     * 不传appid 默认使用 配置文件中wechat的appid
     * @param param
     */
    weChatPay(param: WeChatPay): Promise<Result<{
        "result": string;
        "type": string;
    }>>;
    /**
     * 连连支付
     * @param param
     */
    lianlianPay(param: LianLianPay): Promise<Result<{
        "ret_code": string;
        "ret_msg": string;
        "type": string;
    }>>;
    /**
     * 跳到 QQ 界面
     * @param qq
     */
    xviewPushQQ(qq: string): Promise<Result<{
        "result": string;
        "type": string;
    }>>;
    /**
     * 是否安装微信
     */
    xviewIsWXAppInstalled(): Promise<Result<{
        "result": string;
        "type": string;
    }>>;
    /**
     *
     * @param type
     * @param data
     */
    private payShareLogin<T>(type, data);
}
