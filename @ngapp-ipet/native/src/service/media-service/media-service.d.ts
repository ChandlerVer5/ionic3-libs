import { XViewConfig } from './../xview-config/XViewConfig';
import { Result } from './../../constants/constants';
import { XView } from '../XView';
export declare class MediaService extends XView {
    constructor(config: XViewConfig);
    /**
     * 扫描二维码
     */
    scanQRCode(): Promise<Result<any>>;
    /**
     * 水印图片
     * @param json
     */
    xviewSaveImageToGallery(json: {
        "url": string;
        "text": string;
    }): Promise<Result<{
        result: string;
    }>>;
    /**
     * 播放音频
     * @param json
     */
    xviewPlayAudio(json: {
        "state": "play" | "stop" | "pause" | "resume";
        "url": string;
    }): Promise<Result<{}>>;
    /**
     * 录制音频
     * @param json
     */
    xviewRecordAudio(json?: {
        time: string;
    }): Promise<Result<{
        "time": string;
        "file_path_audio": string;
    }>>;
    /**
     * 录制视频
     * @param json
     */
    xviewRecordVideo(json: {
        time: string;
    }): Promise<Result<{
        "video": string;
        "image": string;
        "imagebase64": string;
        "result": string;
    }>>;
    /**
     * 上传视频
     * @param json
     */
    xviewUploadVideo(json: {
        "video": string;
        "image": string;
        "url": string;
    }): Promise<Result<any>>;
    /**
     * 从相册中选择视频上传
     * @param json
     */
    xviewSelectVideoUpload(json: {
        url: string;
    }): Promise<Result<any>>;
    /**
     * 上传图片
     * @param json
     */
    xviewUploadImage(json: {
        "color": string;
        "maxnumber": string;
    }): Promise<Result<Array<string>>>;
    /**
     * 上传文件
     * @param json
     */
    xviewUploadFile(json: {
        "url": string;
        "file_path": Array<string>;
    } | {
        [key: string]: any;
    }): Promise<Result<any>>;
}
