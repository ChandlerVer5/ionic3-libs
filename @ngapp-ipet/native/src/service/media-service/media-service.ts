import { XViewConfig } from '../xview-config/XViewConfig';
import { Result } from '../../constants/constants';
import { XView } from '../XView';
import { Injectable } from '@angular/core';


@Injectable()
export class MediaService extends XView {

    constructor(config: XViewConfig) {
        super(config.appConfig);
    }


    /**
     * 扫描二维码
     */
    public scanQRCode(): Promise<Result<any>> {
        return this.xviewNative("xviewScanBarcode", {});
    }

    /**
     * 水印图片
     * @param json 
     */
    public xviewSaveImageToGallery(json: {
        "url": string;// "图片网址",
        "text": string;// "水印文字"
    }): Promise<Result<{ result: string }>> {
        return this.xviewNative('xviewSaveImageToGallery', { ...json });
    }

    /**
     * 播放音频
     * @param json 
     */
    public xviewPlayAudio(json: {
        "state": "play" | "stop" | "pause" | "resume",
        "url": string,
    }) {
        return this.xviewNative('xviewPlayAudio', json);
    }

    /**
     * 录制音频
     * @param json 
     */
    public xviewRecordAudio(json: {
        time: string;
    } = { time: '60' }): Promise<Result<{
        "time": string;
        "file_path_audio": string;
    }>> {
        return this.xviewNative('xviewRecordAudio', json);
    }


    /**
     * 录制视频 
     * @param json 
     */
    public xviewRecordVideo(json: {
        time: string
    }): Promise<Result<{
        "video": string;//"视频在本机路径", 
        "image": string;//"视频截图在本机路径",
        "imagebase64": string;//"视频截图base64编码",
        "result": string;//"success/失败原因"
    }>> {
        return this.xviewNative('xviewRecordVideo', { ...json });
    }

    /**
     * 上传视频 
     * @param json 
     */
    public xviewUploadVideo(json: {
        "video": string;//"视频地址", 
        "image": string;//"截图地址", 
        "url": string;//"接口"
    }): Promise<Result<any>> {
        return this.xviewNative('xviewUploadVideo', json);
    }

    /**
     * 从相册中选择视频上传
     * @param json 
     */
    public xviewSelectVideoUpload(json: {
        url: string
    }): Promise<Result<any>> {
        return this.xviewNative('xviewSelectVideoUpload', json);
    }


    /**
     * 上传图片
     * @param json 
     */
    public xviewUploadImage(json: {
        "color": string;//"导航栏颜色，十六进制，必须为8位字符串，如0xffffff，选填，默认为黑色"
        "maxnumber": string;//""选择图片数量的最大值，最大为9",
    }): Promise<Result<Array<string>>> {
        return this.xviewNative('xviewUploadImage', json);
    }


    /**
     * 上传文件
     * @param json 
     */
    public xviewUploadFile(json: {
        "url": string; //请求地址
        "file_path": Array<string>;//文件地址
    } | { [key: string]: any }): Promise<Result<any>> {
        return this.xviewNative('xviewUploadFile', json);
    }

}