import { Observable } from 'rxjs';
import { XViewConfig } from './../xview-config/XViewConfig';
import { RongCloud, RongCloudCurrentSessionList, Result, RongCloudSession, RongCloudSessionPane } from './../../constants';
import { XView } from './../XView';
/**
 * 融云
 */
export declare class RongCloudService extends XView {
    private xviewRefreshSessionList;
    private xviewLoginKicked;
    /**
     * 储存融云登陆信息
     */
    private rongCloud;
    constructor(config: XViewConfig);
    /**
     * 原生调用js方法, 接收到消息, 返回会话列表消息给js
     */
    xviewRefreshSessionList$(): Observable<Result<RongCloudCurrentSessionList>>;
    /**
     * 融云账号被退出登录时, 原生调用js方法
     */
    xviewLoginKicked$(): Observable<Result<{
        result: string;
    }>>;
    /**
     * 手动触发 刷新会话列表
     */
    setXviewRefreshSessionList(param: RongCloudCurrentSessionList): void;
    /**
     * 融云登陆
     *
     * @param params 参数
     * @param isValidate  是否登陆校验
     */
    xviewRongCloudLogin(params: RongCloud, isValidate?: boolean): Promise<Result<{
        result: string;
    }>>;
    /**
     * 退出融云
     */
    xviewRongCloudLogout(): Promise<Result<{
        result: string;
    }>>;
    /**
     * 获取会话列表
     */
    xviewCurrentSessionList(): Promise<Result<RongCloudCurrentSessionList>>;
    /**
     * 跳到原生界面
     * @param params
     */
    xviewPushSessionView(params: RongCloudSessionPane): Promise<Result<{
        result: string;
    }>>;
    /**
     * 删除会话
     * @param params
     */
    xviewDeleteSession(params: RongCloudSession): Promise<Result<{
        result: string;
    }>>;
    /**
     * pop到之前原生界面
     * @param params default { number: '1' }
     */
    xviewPop(params?: {
        number: string;
    }): Promise<Result<{
        result: string;
    }>>;
}
