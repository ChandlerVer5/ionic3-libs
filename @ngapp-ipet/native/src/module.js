var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
import { PayShareLogin, PhoneUtilsService, JPushService, XViewConfig, MediaService, RongCloudService, BlueToothService } from './service';
import { IosBarColor, CallPhone, CleanCache, ScanQRCode, WeiboLogin, QQLogin, WechatLogin, PushView, PopView } from './directives';
import { XVIEW_CONFIG, xViewConfigFactory } from './constants';
import { NgModule } from '@angular/core';
var NativeModule = (function () {
    function NativeModule() {
    }
    NativeModule_1 = NativeModule;
    NativeModule.config = function (ops) {
        return {
            ngModule: NativeModule_1,
            providers: [
                { provide: XVIEW_CONFIG, useValue: ops },
                { provide: XViewConfig, useFactory: xViewConfigFactory, deps: [XVIEW_CONFIG] },
                JPushService,
                PhoneUtilsService,
                MediaService,
                PayShareLogin,
                RongCloudService,
                BlueToothService,
            ]
        };
    };
    NativeModule = NativeModule_1 = __decorate([
        NgModule({
            declarations: [
                CallPhone,
                CleanCache,
                IosBarColor,
                ScanQRCode,
                WeiboLogin,
                QQLogin,
                WechatLogin,
                PushView,
                PopView,
            ],
            exports: [
                CallPhone,
                CleanCache,
                IosBarColor,
                ScanQRCode,
                WeiboLogin,
                QQLogin,
                WechatLogin,
                PushView,
                PopView,
            ]
        })
    ], NativeModule);
    return NativeModule;
    var NativeModule_1;
}());
export { NativeModule };
//# sourceMappingURL=module.js.map