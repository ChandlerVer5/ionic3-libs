import { XViewConfig } from './../service/xview-config/XViewConfig';
import { InjectionToken } from '@angular/core';
export declare const XVIEW_CONFIG: InjectionToken<{}>;
export declare function xViewConfigFactory(config: any): XViewConfig;
