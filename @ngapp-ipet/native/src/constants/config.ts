import { XViewConfig } from '../service/xview-config/XViewConfig';


import { InjectionToken } from '@angular/core';


export const XVIEW_CONFIG = new InjectionToken('XVIEW_CONFIG');

export function xViewConfigFactory(config) {
    return new XViewConfig(config);
}
