
/**
 * 统一回调返回数据
 */
export interface Result<T> {
    code: string;
    data: T;
    message: string;
    success: boolean;
}

/**
 * 导航方式  "driving" | "walking" | "riding";
 */
export type NavigationType = "driving" | "walking" | "riding";
/**
 * 极光参数
 */
export interface JPushParams {
    aps: {
        sound: string;
        badge: string;
        alert: string;
    };
    _j_uid: string;
    _j_msgid: string;
    _j_business: string;
    // [K in keyof T]:string;
}


export interface Config {
    weChat?: WeChat;
    qq?: QQ;
    weiBo?: SinaWeibo;
    xunFei?: XunFei;
    isDebug?: boolean;
    callback?: any;
}

export interface AppVersion {
    versionCode: string;// "版本号 - android 升级判断使用",
    versionName: string;// "当前版本名称, 如1.0.1, web展示给用户看的" 
}

/**
 * 融云登陆
 */
export interface RongCloud {
    userId: string;//"用户ID", 
    username: string;//"用户名称", 
    portrait: string;//"用户头像的URL", 
    token: string;//"融云个人账户token", 
}

/**
 * 融云会话消息
 */
export interface RongCloudCurrentMessage {
    unreadCount: string;//"未读消息数",
    sessionId: string;//"会话ID",
    sessionType: string;//"会话类型",
    messageType: string;//"消息类型",
    headerImage: string;//"头像网址", //可能不正确，需要web自己去后台获取
    nickname: string;//"昵称", //可能不正确，需要web自己去后台获取
    text: string;//"消息文本",
    sendTime: string;//"发送消息时间"
}

export interface RongCloudSession {
    "sessionId": string;//"",//会话id:单聊时对方的userId，群聊时群组的groupId 
    "sessionType": string;//"",//会话类型 P2P-单聊,Team-群聊
}

/**
 * 会话面板
 */
export interface RongCloudSessionPane extends RongCloudSession {
    "title"?: string;//"",//会话标题
    "nickname"?: string;//
    "path"?: string;// ""//点击原生聊天界面右上角按钮,跳转的路径,空值时则隐藏该按钮
}

/**
 * 融云会话列表
 */
export interface RongCloudCurrentSessionList {
    allUnreadCount: string;//"会话列表中所有的未读消息数量",
    list: Array<RongCloudCurrentMessage>;
}

/**
 * 微信
 */
export interface WeChat {
    wxappid: string;
    wxappsecret: string;
}

/**
 * 分享类型
 */
export type ShareType = 'web' | 'text' | 'image' | 'audio' | 'video';

/**
 * 讯飞
 */
export interface XunFei {
    ttsAppId: string;
}

/**
 * 微信分享
 */
export interface ShareWeb {
    title: string;
    description: string;
    thumburl: string; //"缩略图网址，小于20kb",
    shareurl: string; //"分享网页地址",

}

/**
 * 文字分享
 */
export interface ShareText {
    text: string;//文字内容
}

/**
 * 图片分享
 */
export interface ShareImage {
    thumburl: string;//缩略图网址(小于20kb)
    imageurl: string; //图片网址

}

/**
 * 音乐分享
 */
export interface ShareAudio {

    "title": string;//标题
    "description": string;//描述
    "thumburl": string;//缩略图网址(小于20kb)
    "shareurl": string;//跳转的网页地址(音乐和视频网页内必须有可播放的音视频资源)
    "fileurl": string;//分享音乐时,音乐源文件地址
}

/**
 * 视频分享
 */
export interface ShareVideo {
    "title": string;//标题
    "description": string;//描述
    "thumburl": string;//缩略图网址(小于20kb)
    "shareurl": string;//跳转的网页地址(音乐和视频网页内必须有可播放的音视频资源)
    "fileurl"?: string;//分享音乐时,音乐源文件地址 
}

/**
 * 微信支付参数
 */
export interface WeChatPay {
    appid?: string;//"wx59d5d49c9d5f47df",
    noncestr: string;//"ZbZk5peCk4f2g7eZ",
    package: string;//"Sign=WXPay",
    partnerid: string;//"1286763701",
    prepayid: string;//"wx201707191355284ae4cd86530260144260",
    sign: string;//"19796FE4E7C71068236D98D802A483E0",
    timestamp: string;//"1500443794",
}


/**
 * QQ
 */
export interface QQ {
    qqappid: string;
}
/**
 * QQ 分享
 */
export interface QQShare {
    title: string;
    descrption: string;
    picture: string;
    url: string;
}

/**
 * "title": "分享标题",
"description": "分享描述",
"thumburl": "缩略图网址，小于20kb",
"shareurl": "分享网页地址",

"title": "分享标题",
"description": "分享描述",
"thumburl": "缩略图网址，小于20kb",
"shareurl": "分享网页地址",

"title": "分享标题",
"description": "分享描述",
"thumburl": "缩略图网址，小于20kb",
"shareurl": "分享网页地址",

"title": "分享标题",
"description": "分享描述",
"thumburl": "缩略图网址，小于20kb",
"shareurl": "分享网页地址",

"title": "分享标题",
"description": "分享描述",
"thumburl": "缩略图网址，小于20kb",
"shareurl": "分享网页地址",
 */

/**
 * 新浪配置
 */
export interface SinaWeibo {
    wbappkey: string;
    wbsecret: string;
}

/**
 * 新浪微博
 */
export interface SinaWeiboShare {
    title: string;
    descrption: string;
    picture: string;
    url: string;
    wbRedirectUrl: string;
    wbAuthoUrl: string;
}

/**
 * 微信 微博 QQ 三方登陆返回的用户信息
 */
export interface LoginInfo {
    openid: string;//授权用户唯一标识(微信和qq的openid、微博的uid)
    unionid: string;//仅微信登录有值
    nickname: string;//昵称
    headimgurl: string;//头像地址
    sex: string;//性别:0女 1男 2未知
    access_token: string;//接口调用凭证
    refresh_token: string;//用户刷新access_token
    type: string;//请求xview方法时的type类型
}

/**
 * ali 支付回调参数
 */
export interface AliPayCb {
    result: string;//"成功/失败(包括但不限于"取消")/正在处理中",
    resultStatus: string;//"6001",
    ret_code: string;//"9903",
    ret_msg: string;//"错误信息",
    type: string;//"请求xview方法时的type类型"
}



/**
 * 连连支付需要的参数
 */
export interface LianLianPay {
    "dt_order": string;//商户订单时间,格式：YYYYMMDDH24MISS 14 位数字，精确到秒
    "oid_partner": string;//商户编号
    "sign_type": string;//签名方式
    "sign": string;//签名(需配置到商户后台)
    "busi_partner": string;////商户业务类型,虚拟商品销售：101001,实物商品销售：109001
    "no_order": string;//商户唯一订单号
    "money_order": string;////交易金额(元)
    "user_id": string;//商户用户唯一编号
    "id_type": string;//证件类型:0身份证
    "notify_url": string;////服务器异步通知地址
    "id_no": string;//身份证号码
    "acct_name": string;//银行账号姓名
    "card_no": string;//银行卡号
    "name_goods": string;//商品名称 
    "info_order": string;//订单描述
    "frms_ware_category": string;//商品类目,具体可参见连连接口说明(商品类目代码表)
    "user_info_mercht_userno": string;//商户用户唯一标识
    "user_info_dt_register": string;//注册时间(YYYYMMDDH24MISS)
    "user_info_id_no": string;//用户注册证件号码 
    "user_info_identify_type": string;//是实名认证时，必填1：银行卡认证2：现场认证3：身份证远程认证4：其它认证
    "user_info_identify_state": string;//是否实名认证1:是0:无认证,商户自身是否对用户信息进行实名认证。默认：0
    "user_info_bind_phone": string;//绑定手机号
    "user_info_full_name": string;//用户注册姓名 
}

/**
 * 地理位置信息
 */
export interface AddressLocation {
    "result": string;//"success/失败原因",
    "address": string;//"格式化地址",
    "country": string;//"国家",
    "province": string;//"省/直辖市",
    "city": string;//"市",
    "district": string;//"区",
    "street": string;//"街道名称",
    "number": string;//"门牌号",
    "POIName": string;// "兴趣点名称",
    "AOIName": string;//"所属兴趣点名称",
    "latitude": string;//"经度",
    "longitude": string;//"纬度",
}
