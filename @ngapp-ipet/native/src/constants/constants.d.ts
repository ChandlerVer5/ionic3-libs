/**
 * 统一回调返回数据
 */
export interface Result<T> {
    code: string;
    data: T;
    message: string;
    success: boolean;
}
/**
 * 导航方式  "driving" | "walking" | "riding";
 */
export declare type NavigationType = "driving" | "walking" | "riding";
/**
 * 极光参数
 */
export interface JPushParams {
    aps: {
        sound: string;
        badge: string;
        alert: string;
    };
    _j_uid: string;
    _j_msgid: string;
    _j_business: string;
}
export interface Config {
    weChat?: WeChat;
    qq?: QQ;
    weiBo?: SinaWeibo;
    xunFei?: XunFei;
    isDebug?: boolean;
    callback?: any;
}
export interface AppVersion {
    versionCode: string;
    versionName: string;
}
/**
 * 融云登陆
 */
export interface RongCloud {
    userId: string;
    username: string;
    portrait: string;
    token: string;
}
/**
 * 融云会话消息
 */
export interface RongCloudCurrentMessage {
    unreadCount: string;
    sessionId: string;
    sessionType: string;
    messageType: string;
    headerImage: string;
    nickname: string;
    text: string;
    sendTime: string;
}
export interface RongCloudSession {
    "sessionId": string;
    "sessionType": string;
}
/**
 * 会话面板
 */
export interface RongCloudSessionPane extends RongCloudSession {
    "title"?: string;
    "nickname"?: string;
    "path"?: string;
}
/**
 * 融云会话列表
 */
export interface RongCloudCurrentSessionList {
    allUnreadCount: string;
    list: Array<RongCloudCurrentMessage>;
}
/**
 * 微信
 */
export interface WeChat {
    wxappid: string;
    wxappsecret: string;
}
/**
 * 分享类型
 */
export declare type ShareType = 'web' | 'text' | 'image' | 'audio' | 'video';
/**
 * 讯飞
 */
export interface XunFei {
    ttsAppId: string;
}
/**
 * 微信分享
 */
export interface ShareWeb {
    title: string;
    description: string;
    thumburl: string;
    shareurl: string;
}
/**
 * 文字分享
 */
export interface ShareText {
    text: string;
}
/**
 * 图片分享
 */
export interface ShareImage {
    thumburl: string;
    imageurl: string;
}
/**
 * 音乐分享
 */
export interface ShareAudio {
    "title": string;
    "description": string;
    "thumburl": string;
    "shareurl": string;
    "fileurl": string;
}
/**
 * 视频分享
 */
export interface ShareVideo {
    "title": string;
    "description": string;
    "thumburl": string;
    "shareurl": string;
    "fileurl"?: string;
}
/**
 * 微信支付参数
 */
export interface WeChatPay {
    appid?: string;
    noncestr: string;
    package: string;
    partnerid: string;
    prepayid: string;
    sign: string;
    timestamp: string;
}
/**
 * QQ
 */
export interface QQ {
    qqappid: string;
}
/**
 * QQ 分享
 */
export interface QQShare {
    title: string;
    descrption: string;
    picture: string;
    url: string;
}
/**
 * "title": "分享标题",
"description": "分享描述",
"thumburl": "缩略图网址，小于20kb",
"shareurl": "分享网页地址",

"title": "分享标题",
"description": "分享描述",
"thumburl": "缩略图网址，小于20kb",
"shareurl": "分享网页地址",

"title": "分享标题",
"description": "分享描述",
"thumburl": "缩略图网址，小于20kb",
"shareurl": "分享网页地址",

"title": "分享标题",
"description": "分享描述",
"thumburl": "缩略图网址，小于20kb",
"shareurl": "分享网页地址",

"title": "分享标题",
"description": "分享描述",
"thumburl": "缩略图网址，小于20kb",
"shareurl": "分享网页地址",
 */
/**
 * 新浪配置
 */
export interface SinaWeibo {
    wbappkey: string;
    wbsecret: string;
}
/**
 * 新浪微博
 */
export interface SinaWeiboShare {
    title: string;
    descrption: string;
    picture: string;
    url: string;
    wbRedirectUrl: string;
    wbAuthoUrl: string;
}
/**
 * 微信 微博 QQ 三方登陆返回的用户信息
 */
export interface LoginInfo {
    openid: string;
    unionid: string;
    nickname: string;
    headimgurl: string;
    sex: string;
    access_token: string;
    refresh_token: string;
    type: string;
}
/**
 * ali 支付回调参数
 */
export interface AliPayCb {
    result: string;
    resultStatus: string;
    ret_code: string;
    ret_msg: string;
    type: string;
}
/**
 * 连连支付需要的参数
 */
export interface LianLianPay {
    "dt_order": string;
    "oid_partner": string;
    "sign_type": string;
    "sign": string;
    "busi_partner": string;
    "no_order": string;
    "money_order": string;
    "user_id": string;
    "id_type": string;
    "notify_url": string;
    "id_no": string;
    "acct_name": string;
    "card_no": string;
    "name_goods": string;
    "info_order": string;
    "frms_ware_category": string;
    "user_info_mercht_userno": string;
    "user_info_dt_register": string;
    "user_info_id_no": string;
    "user_info_identify_type": string;
    "user_info_identify_state": string;
    "user_info_bind_phone": string;
    "user_info_full_name": string;
}
/**
 * 地理位置信息
 */
export interface AddressLocation {
    "result": string;
    "address": string;
    "country": string;
    "province": string;
    "city": string;
    "district": string;
    "street": string;
    "number": string;
    "POIName": string;
    "AOIName": string;
    "latitude": string;
    "longitude": string;
}
