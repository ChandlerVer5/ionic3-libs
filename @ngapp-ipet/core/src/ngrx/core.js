var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
import { NgModule, Optional, SkipSelf } from '@angular/core';
import { StoreModule, _INITIAL_REDUCERS } from '@ngrx/store';
import { combineReducersFactory, nullReducersProvider, STORE_PROVIDER_TOKEN, INIT_REDUCER_TOKEN } from './factory';
export * from './factory';
export { ReducersChildModule } from './childStore';
export { Store } from '@ngrx/store';
var ReducersModule = /** @class */ (function () {
    function ReducersModule(parentModule) {
        if (parentModule) {
            throw new Error('ReducersModule is already loaded. Import it in the AppModule only');
        }
    }
    ReducersModule_1 = ReducersModule;
    /**
     * 设置根reducer
     * @param initReducers
     */
    ReducersModule.rootReducer = function (initReducers) {
        if (initReducers === void 0) { initReducers = {}; }
        return {
            ngModule: ReducersModule_1,
            providers: [{
                    provide: INIT_REDUCER_TOKEN,
                    useValue: initReducers
                },
                {
                    provide: STORE_PROVIDER_TOKEN,
                    useFactory: nullReducersProvider,
                    multi: true
                },
                {
                    provide: _INITIAL_REDUCERS,
                    deps: [STORE_PROVIDER_TOKEN, INIT_REDUCER_TOKEN],
                    useFactory: combineReducersFactory
                }]
        };
    };
    ReducersModule = ReducersModule_1 = __decorate([
        NgModule({
            imports: [
                StoreModule.forRoot({})
            ]
        }),
        __param(0, Optional()), __param(0, SkipSelf()),
        __metadata("design:paramtypes", [ReducersModule])
    ], ReducersModule);
    return ReducersModule;
    var ReducersModule_1;
}());
export { ReducersModule };
//# sourceMappingURL=core.js.map