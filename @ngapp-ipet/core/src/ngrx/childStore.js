var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
import { STORE_PROVIDER_TOKEN } from './factory';
import { NgModule } from '@angular/core';
var ReducersChildModule = /** @class */ (function () {
    function ReducersChildModule() {
    }
    ReducersChildModule_1 = ReducersChildModule;
    ReducersChildModule.child = function (reducersProvider) {
        return {
            ngModule: ReducersChildModule_1,
            providers: [
                {
                    provide: STORE_PROVIDER_TOKEN,
                    useFactory: reducersProvider,
                    multi: true
                }
            ]
        };
    };
    ReducersChildModule = ReducersChildModule_1 = __decorate([
        NgModule()
    ], ReducersChildModule);
    return ReducersChildModule;
    var ReducersChildModule_1;
}());
export { ReducersChildModule };
//# sourceMappingURL=childStore.js.map