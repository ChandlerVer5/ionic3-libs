import { NgModule, Optional, SkipSelf, ModuleWithProviders } from '@angular/core';
import { StoreModule, _INITIAL_REDUCERS } from '@ngrx/store';
import { combineReducersFactory, nullReducersProvider, STORE_PROVIDER_TOKEN, INIT_REDUCER_TOKEN } from './factory';

export * from './factory';
export { ReducersChildModule } from './childStore';
export { Store, Action } from '@ngrx/store';

@NgModule({
    imports: [
        StoreModule.forRoot({})
    ]
})
export class ReducersModule {
    constructor( @Optional() @SkipSelf() parentModule: ReducersModule) {
        if (parentModule) {
            throw new Error(
                'ReducersModule is already loaded. Import it in the AppModule only');
        }
    }

    /**
     * 设置根reducer
     * @param initReducers
     */
    static rootReducer(initReducers: { [key: string]: Function } = {}): ModuleWithProviders {
        return {
            ngModule: ReducersModule,
            providers: [{
                provide: INIT_REDUCER_TOKEN,
                useValue: initReducers
            },
            {
                provide: STORE_PROVIDER_TOKEN,
                useFactory: nullReducersProvider,
                multi: true
            },
            {
                provide: _INITIAL_REDUCERS,
                deps: [STORE_PROVIDER_TOKEN, INIT_REDUCER_TOKEN],
                useFactory: combineReducersFactory
            }]
        }
    }
}


