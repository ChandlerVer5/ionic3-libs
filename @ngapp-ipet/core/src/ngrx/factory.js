import { combineReducers } from '@ngrx/store';
import { InjectionToken } from '@angular/core';
export var STORE_PROVIDER_TOKEN = new InjectionToken('StoreProvider');
export var INIT_REDUCER_TOKEN = new InjectionToken('INIT_REDUCER_TOKEN');
var StoreProvider = /** @class */ (function () {
    function StoreProvider() {
    }
    return StoreProvider;
}());
export { StoreProvider };
export function combineReducersFactory(providers, init) {
    if (init === void 0) { init = {}; }
    var obj = providers.reduce(function (o, provider) {
        if (provider.name) {
            o[provider.name()] = provider.reducer;
        }
        return o;
    }, {});
    obj = Object.assign(obj, init);
    return combineReducers(obj);
}
export function nullReducersProvider() {
    return {};
}
//# sourceMappingURL=factory.js.map