import { ModuleWithProviders } from '@angular/core';
export * from './factory';
export { ReducersChildModule } from './childStore';
export { Store, Action } from '@ngrx/store';
export declare class ReducersModule {
    constructor(parentModule: ReducersModule);
    /**
     * 设置根reducer
     * @param initReducers
     */
    static rootReducer(initReducers?: {
        [key: string]: Function;
    }): ModuleWithProviders;
}
