import { Action } from '@ngrx/store';
import { InjectionToken } from '@angular/core';
export declare const STORE_PROVIDER_TOKEN: InjectionToken<{}>;
export declare const INIT_REDUCER_TOKEN: InjectionToken<{}>;
export declare abstract class StoreProvider<T> {
    abstract name(): string;
    abstract reducer(state: T, action: Action): any;
}
export declare function combineReducersFactory(providers: StoreProvider<any>[], init?: {}): any;
export declare function nullReducersProvider(): {};
