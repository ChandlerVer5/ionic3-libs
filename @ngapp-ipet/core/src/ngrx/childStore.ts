import { STORE_PROVIDER_TOKEN } from './factory';
import { NgModule, ModuleWithProviders } from '@angular/core';


@NgModule()
export class ReducersChildModule {

    static child(reducersProvider): ModuleWithProviders {
        return {
            ngModule: ReducersChildModule,
            providers: [
                {
                    provide: STORE_PROVIDER_TOKEN,
                    useFactory: reducersProvider,
                    multi: true
                }
            ]
        }
    }
}