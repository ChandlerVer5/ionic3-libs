import { ModuleWithProviders } from '@angular/core';
export declare class ReducersChildModule {
    static child(reducersProvider: any): ModuleWithProviders;
}
