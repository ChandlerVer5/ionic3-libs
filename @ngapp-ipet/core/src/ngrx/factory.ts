import { Action, combineReducers } from '@ngrx/store';
import { InjectionToken } from '@angular/core';

export const STORE_PROVIDER_TOKEN = new InjectionToken('StoreProvider');

export const INIT_REDUCER_TOKEN = new InjectionToken('INIT_REDUCER_TOKEN');

export abstract class StoreProvider<T> {
    abstract name(): string;
    abstract reducer(state: T, action: Action);
}


export function combineReducersFactory(providers: StoreProvider<any>[], init = {}): any {
    let obj = providers.reduce((o, provider) => {
        if (provider.name) {
            o[provider.name()] = provider.reducer;
        }
        return o;
    }, {});
    obj = Object.assign(obj, init);
    return combineReducers(obj);
}


export function nullReducersProvider() {
    return {}
}