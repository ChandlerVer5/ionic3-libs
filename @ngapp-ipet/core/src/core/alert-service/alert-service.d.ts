import { AlertController, Alert, ToastController, LoadingController, Loading, ModalController, ActionSheet, ActionSheetController } from 'ionic-angular';
import { ModalOptions } from "ionic-angular/components/modal/modal-options";
/**
 * 弹出框方法
 */
export declare class AlertService {
    private alertCtrl;
    private actionSheetCtrl;
    private toastCtrl;
    private loadingCtrl;
    private modalCtrl;
    constructor(alertCtrl: AlertController, actionSheetCtrl: ActionSheetController, toastCtrl: ToastController, loadingCtrl: LoadingController, modalCtrl: ModalController);
    /**
     * actionSheet
     * @param param
     */
    actionSheet(param: any): ActionSheet;
    /**
     * 加载框
     * @param param
     */
    loading(param?: string): Loading;
    loading(param?: {
        content: string;
        duration?: number;
    }): any;
    /**
     * Modal
     * @param 参考modal
     */
    modal(component: any, data?: any, opts?: ModalOptions): void;
    /**
     * 吐司
     *
     * @param tip
     */
    toast(tip: string): void;
    toast(tip: {
        duration?: number;
        message: string;
        position?: string;
        fn?: Function;
    }): void;
    _alert: Alert;
    /**
     * 弹出框
     */
    alert(tip: string): Promise<any>;
    alert(tip: {
        title?: string;
        message: string;
        fn?: {
            text: string;
            fn?: Function;
        } | Function;
    }): Promise<any>;
    alert(tip: {
        title?: string;
        message: string;
        success: {
            text: string;
            fn?: Function;
        } | Function;
        error?: {
            text: string;
            fn?: Function;
        } | Function;
    }): Promise<any>;
}
