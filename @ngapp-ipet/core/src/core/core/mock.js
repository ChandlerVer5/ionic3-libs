var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
import { MOCK_HTTP_TOKEN } from './other';
import { NgModule } from '@angular/core';
var ChildMockHttpProvides = /** @class */ (function () {
    function ChildMockHttpProvides() {
    }
    ChildMockHttpProvides_1 = ChildMockHttpProvides;
    ChildMockHttpProvides.childMock = function (mockHttpProvides) {
        return {
            ngModule: ChildMockHttpProvides_1,
            providers: [
                {
                    provide: MOCK_HTTP_TOKEN,
                    useFactory: mockHttpProvides,
                    multi: true
                }
            ]
        };
    };
    ChildMockHttpProvides = ChildMockHttpProvides_1 = __decorate([
        NgModule()
    ], ChildMockHttpProvides);
    return ChildMockHttpProvides;
    var ChildMockHttpProvides_1;
}());
export { ChildMockHttpProvides };
//# sourceMappingURL=mock.js.map