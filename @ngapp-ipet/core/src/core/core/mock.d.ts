import { ModuleWithProviders } from '@angular/core';
export declare class ChildMockHttpProvides {
    static childMock(mockHttpProvides: Function): ModuleWithProviders;
}
