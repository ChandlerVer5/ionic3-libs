import { Config } from '../constants/constatns';
import { ModuleWithProviders } from '@angular/core';
export declare class AppCoreModule {
    static config(options?: Config): ModuleWithProviders;
}
