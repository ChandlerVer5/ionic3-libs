import { Config } from '../constants/constatns';
import { FormValidate } from '../form-validate/form-validate';
import { CodeService } from '../code-service/code-service';
import { HttpService } from '../http-service/http-service';
import { AlertService } from '../alert-service/alert-service';
import { UtilsService } from '../utils-service/utils-service';
import { ConfigToken } from './other';
import { AppConfig } from '../config/config';
import { HttpClientModule } from '@angular/common/http';
import { NgModule, ModuleWithProviders } from '@angular/core';
import { IonicModule } from 'ionic-angular';





@NgModule({
    imports: [
      HttpClientModule,
        IonicModule
    ]
})
export class AppCoreModule {

    static config(options: Config = {}): ModuleWithProviders {
        return {
            ngModule: AppCoreModule,
            providers: [
                { provide: ConfigToken, useValue: options },
                AppConfig,
                UtilsService,
                AlertService,
                HttpService,
                CodeService,
                FormValidate,
            ]
        }
    }
}




