import { AppConfig } from '../config/config';
import { HttpCode } from '../constants/constatns';
import { InjectionToken } from '@angular/core';
export declare const MOCK_HTTP_TOKEN: InjectionToken<{}>;
export declare const ConfigToken: InjectionToken<{}>;
export declare abstract class MockHttp {
    abstract get(url: string, param: any): HttpCode<any>;
    abstract post(url: string, param: any): HttpCode<any>;
}
export declare function appConfigFactory(config: any, providers: any): AppConfig;
export declare function nullMockHttpProvides(): {};
