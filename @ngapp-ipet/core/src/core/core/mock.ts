import { MOCK_HTTP_TOKEN } from './other';
import { ModuleWithProviders, NgModule } from '@angular/core';

@NgModule()
export class ChildMockHttpProvides {

    static childMock(mockHttpProvides: Function): ModuleWithProviders {
        return {
            ngModule: ChildMockHttpProvides,
            providers: [
                {
                    provide: MOCK_HTTP_TOKEN,
                    useFactory: mockHttpProvides,
                    multi: true
                }
            ]
        }
    }
}