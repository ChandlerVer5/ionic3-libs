import { AppConfig } from '../config/config';
import { InjectionToken } from '@angular/core';
export var MOCK_HTTP_TOKEN = new InjectionToken('MockHttpToken');
export var ConfigToken = new InjectionToken('DON_APPTOKEN');
var MockHttp = /** @class */ (function () {
    function MockHttp() {
    }
    return MockHttp;
}());
export { MockHttp };
export function appConfigFactory(config, providers) {
    return new AppConfig(config, providers);
}
export function nullMockHttpProvides() {
    return {};
}
//# sourceMappingURL=other.js.map