import { AppConfig } from '../config/config';
import { HttpCode } from '../constants/constatns';
import { InjectionToken } from '@angular/core';


export const MOCK_HTTP_TOKEN = new InjectionToken('MockHttpToken');

export const ConfigToken = new InjectionToken('DON_APPTOKEN');

export abstract class MockHttp {
    abstract get(url: string, param: any): HttpCode<any>;
    abstract post(url: string, param: any): HttpCode<any>;
}

export function appConfigFactory(config, providers) {
    return new AppConfig(config, providers);
}


export function nullMockHttpProvides() {
    return {};
}


