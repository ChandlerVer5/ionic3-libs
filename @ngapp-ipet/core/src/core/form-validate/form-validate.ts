import { AlertService } from '../alert-service/alert-service';
import { ValidateAttr } from './form-validate';
import { UtilsService } from '../utils-service/utils-service';
import { Injectable } from '@angular/core';


@Injectable()
export class FormValidate {

  constructor(
    private utilsService: UtilsService, private alertService: AlertService) {
  }


  /**
   * 校验 根据属性名校验
   * @param param 
   * @param validate 
   */
  public validate = (param: Object, ...validate: Validate[]): boolean => {
    let value: string, validateValue: ValidateValue[], params: any;
    const that = this;
    for (let _info of validate) {
      value = param[_info.name];
      validateValue = _info.toChecked;

      for (let _item of validateValue) {
        params = _item.param;
        if (!that.validateFn(_item.fn)(value, ...params)) {
          that.alert(_item.tip);
          return true;
        }
      }
    }
    return false;
  }

  /**
   * 根据表单值检验
   * @param validateAttr 
   */
  public validateValue = (...validateAttr: ValidateAttr[]): boolean => {
    let validateValue: ValidateValue[], params: any;
    const that = this;
    for (let _info of validateAttr) {
      validateValue = _info.toChecked;
      for (let _item of validateValue) {
        params = _item.param;
        if (!that.validateFn(_item.fn)(_info.value, ...params)) {
          that.alert(_item.tip);
          return true;
        }
      }
    }
    return false;
  }

  /**
   * 判断是不是空
   * @param value 
   * @return false 为空
   */
  private isNotBlanK = (value: string): boolean => {
    return this.utilsService.isNotEmpty(value);
  }

  /**
   * 判断是不是手机号
   */
  private isPhone = (value: string): boolean => {
    return this.utilsService.validatePhone(value);
  }

  /**
   * 判断是不是密码  6-18
   */
  private isPassWord = (value: string, min: number = 6, max: number = 18) => {
    return this.utilsService.validatePassWord(value, min, max);
  }

  /**
   * 判断array 大小
   */
  private arrayLength = (value: Array<any>, op: string = '>', length: number = 0): boolean => {
    switch (op) {
      case '>':
        return value.length > length;
      case '=':
        return value.length == length;
      case '>=':
        return value.length >= length;
      default:
        return value.length > length;
    }
  }

  /**
   * 等于
   */
  private equals = (value: string, another: string): boolean => {
    return value == another;
  }

  /**
   * 校验车牌号
   */
  private isVehicleNumber = (carNum: string): boolean => {
    return this.utilsService.isVehicleNumber(carNum);
  }


  /**
   * 空方法
   */
  private nullFun(): boolean {
    console.error("this is null function");
    return false;
  }

  /**
   *  检验制造商
   * @param type 
   * @return 函数制造商 返回true 表示校验通过 否则 不通过 提示不通过原因
   */
  private validateFn(type: ValidateType): Function {
    switch (type) {
      case ValidateType.NotBlank:
        return this.isNotBlanK;
      case ValidateType.isPhone:
        return this.isPhone;
      case ValidateType.isPassWord:
        return this.isPassWord;
      case ValidateType.arrayLength:
        return this.arrayLength;
      case ValidateType.equals:
        return this.equals;
      case ValidateType.isVehicleNumber:
        return this.isVehicleNumber;
      default:
        return this.nullFun;
    }
  }

  /**
   * 弹框
   * 
   * @private
   * @param {string} message 
   * @param {string} [title='警告'] 
   * 
   */
  private alert(message: string, title: string = '警告') {
    this.alertService.alert({ message, title });
  }

}


export enum ValidateType {
  /**
   * 不等于空
   */
  NotBlank,
  /**
   * 是不是手机号
   */
  isPhone,
  /**
   * 是不是密码
   */
  isPassWord,

  /**
   * 数组大小是否大于0
   */
  arrayLength,
  /**
   * 是否等于某个值
   */
  equals,
  /**
   * 是否是车牌号
   */
  isVehicleNumber,
}


export interface Validate {
  name: string;
  toChecked: ValidateValue[];
  //isCheck?: boolean; //是否进行校验 默认true 进行校验
}

export interface ValidateAttr {
  value: string | number | Array<any>;
  //isCheck?: boolean; //是否进行校验 默认true 进行校验
  toChecked: ValidateValue[];
}

export interface ValidateValue {
  fn: ValidateType;
  tip: string;
  param?: any[];
}