

import { Injectable } from '@angular/core';
/**
 * Utils类存放和业务无关的公共方法
 * @description
 */
@Injectable()
export class UtilsService {

  constructor() {

  }



  extend(target?:any, source?:any):any{
    let isObjFunc = function (name) {
      var toString = Object.prototype.toString
      return function (ss?:any) {
        return toString.call(arguments[0]) === '[object ' + name + ']'
      }
    }
    let isObject = isObjFunc('Object'),
      isArray = isObjFunc('Array'),
      isBoolean = isObjFunc('Boolean')
    var index = 0, isDeep = false, obj, copy, destination, source, i
    if (isBoolean(arguments[0])) {
      index = 1
      isDeep = arguments[0]
    }
    for (i = arguments.length - 1; i > index; i--) {
      destination = arguments[i - 1]
      source = arguments[i]
      destination = source
    }
    return destination
  }

  /**
   * 校检手机号
   * @param phone
   */
  validatePhone(phone: string) {
    const reg = /^1[8|7|5|4|3][0-9]\d{8}$/;
    return reg.test(phone);
  }

  /**
   * 校验身份证
   * @param IDCard
   */
  validateIDCard(IDCard: string) {
    const reg = /^(\d{15}$|^\d{18}$|^\d{17}(\d|X|x))$/; // 身份证
    return reg.test(IDCard);
  }

  /**
   * 检验密码  6-18
   * @param passWord
   */
  validatePassWord(passWord: string, min: number = 6, max: number = 18) {
    const reg = new RegExp(`\\b(^[a-zA-Z0-9]{${min},${max}}$)\\b`)
    return reg.test(passWord);
  };



  /**
   * 验证邮箱地址
   * @param Email
   */
  validateEmail(email: string) {
    const reg = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/; // 邮箱地址
    return reg.test(email);
  };

  /**
   * 校验车牌
   * @param carNum
   */
  isVehicleNumber(carNum: string) {
    if (carNum.length == 7) {
      const express = /^[京津沪渝冀豫云辽黑湘皖鲁新苏浙赣鄂桂甘晋蒙陕吉闽贵粤青藏川宁琼使领A-Z]{1}[A-Z]{1}[A-Z0-9]{4}[A-Z0-9挂学警港澳]{1}$/;
      return express.test(carNum);
    }
    return false;
  }

  /**
   * 检验银行卡
   * @param bankCard
   */
  validateBankCard(bankCard: string) {
    const reg = /^\d{16}|\d{19}$/;
    return reg.test(bankCard);
  };

  /**
   * 通过GET请求将图片装换为 BASE64
   * @param url
   */
  public convertHttpFileToDataURLviaFileReader(url): Promise<any> {
    return new Promise<any>((resolve, reject) => {
      let xhr = new XMLHttpRequest();
      xhr.responseType = 'blob';
      xhr.onload = function () {
        var reader = new FileReader();
        reader.onloadend = function () {
          resolve(reader.result);
        }
        reader.onerror = function () {
          reject();
        }
        reader.readAsDataURL(xhr.response);
      };
      xhr.open('GET', url);
      xhr.send();
    });
  }


  /**
   * 是不是空
   * @param value
   */
  public isEmpty(value): boolean {
    return value == null || typeof value === 'string' && value.length === 0;
  }

  /**
   * 不是空
   * @param value
   */
  public isNotEmpty(value): boolean {
    return !this.isEmpty(value);
  }

  /**
   * 获取当前月的天数
   * @param date
   */
  public getDays(date: Date = new Date()): number {
    var year = date.getFullYear();
    var m = date.getMonth() + 1;
    if (m == 2) {
      return ((year % 4 == 0) && (year % 100 != 0 || year % 400 == 0)) ? 29 : 28;
    } else if (m == 1 || m == 3 || m == 5 || m == 7 || m == 8 || m == 10 || m == 12) {
      return 31;
    } else {
      return 30;
    }
  }

  /**
   * 获取时间{num}前|后 日期
   *   有问题!!!!!!!!!!!!!
   * @param num  + 往后几天  -往前几天
   * @param date  参照日期
   *
   */
  public getDate(num: number, date: Date = new Date()): Date {
    let currentDate = date.getDate();
    date.setDate(currentDate + num);
    return date;
    // if ((currentDate + num) > day || (currentDate + num) <= 0) {//不在本月
    //   if ((currentDate + num) > day) {
    //     _day = ((currentDate + num) - day);
    //     date.setDate(1);
    //     date.setMonth(month + 1);
    //     if (month >= 11) {
    //       date.setFullYear(date.getFullYear() + 1);
    //     }
    //     if (_day > 1) {
    //       return this.getDate(_day, date);
    //     }
    //   } else {
    //     _day = (currentDate + num);
    //     date.setDate(this.getDays(date));
    //     date.setMonth(month - 1);
    //     if (month < 0) {
    //       date.setFullYear(date.getFullYear() - 1);
    //     }
    //     if (_day < 0) {
    //       return this.getDate(_day, date);
    //     }
    //   }
    //   return date;
    // } else {
    //   date.setDate(currentDate + num);
    //   return date;
    // }
  }


  /**
   * 日期对象转为日期字符串
   * @param date 需要格式化的日期对象
   * @param sFormat 输出格式,默认为yyyy-MM-dd                         年：y，月：M，日：d，时：h，分：m，秒：s
   * @example  dateFormat(new Date())                                "2017-02-28"
   * @example  dateFormat(new Date(),'yyyy-MM-dd')                   "2017-02-28"
   * @example  dateFormat(new Date(),'yyyy-MM-dd hh:mm:ss')         "2017-02-28 09:24:00"
   * @example  dateFormat(new Date(),'hh:mm')                       "09:24"
   * @example  dateFormat(new Date(),'yyyy-MM-ddThh:mm:ss+08:00')   "2017-02-28T09:24:00+08:00"
   * @returns {string}
   */
  public dateFormat(date: Date, sFormat: string = 'yyyy-MM-dd'): string {
    let time = {
      Year: 0,
      TYear: '0',
      Month: 0,
      TMonth: '0',
      Day: 0,
      TDay: '0',
      Hour: 0,
      THour: '0',
      hour: 0,
      Thour: '0',
      Minute: 0,
      TMinute: '0',
      Second: 0,
      TSecond: '0',
      Millisecond: 0
    };
    time.Year = date.getFullYear();
    time.TYear = String(time.Year).substr(2);
    time.Month = date.getMonth() + 1;
    time.TMonth = time.Month < 10 ? "0" + time.Month : String(time.Month);
    time.Day = date.getDate();
    time.TDay = time.Day < 10 ? "0" + time.Day : String(time.Day);
    time.Hour = date.getHours();
    time.THour = time.Hour < 10 ? "0" + time.Hour : String(time.Hour);
    time.hour = time.Hour < 13 ? time.Hour : time.Hour - 12;
    time.Thour = time.hour < 10 ? "0" + time.hour : String(time.hour);
    time.Minute = date.getMinutes();
    time.TMinute = time.Minute < 10 ? "0" + time.Minute : String(time.Minute);
    time.Second = date.getSeconds();
    time.TSecond = time.Second < 10 ? "0" + time.Second : String(time.Second);
    time.Millisecond = date.getMilliseconds();

    return sFormat.replace(/yyyy/ig, String(time.Year))
      .replace(/yyy/ig, String(time.Year))
      .replace(/yy/ig, time.TYear)
      .replace(/y/ig, time.TYear)
      .replace(/MM/g, time.TMonth)
      .replace(/M/g, String(time.Month))
      .replace(/dd/ig, time.TDay)
      .replace(/d/ig, String(time.Day))
      .replace(/HH/g, time.THour)
      .replace(/H/g, String(time.Hour))
      .replace(/hh/g, time.Thour)
      .replace(/h/g, String(time.hour))
      .replace(/mm/g, time.TMinute)
      .replace(/m/g, String(time.Minute))
      .replace(/ss/ig, time.TSecond)
      .replace(/s/ig, String(time.Second))
      .replace(/fff/ig, String(time.Millisecond))
  }

}
