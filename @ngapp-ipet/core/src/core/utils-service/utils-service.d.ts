/**
 * Utils类存放和业务无关的公共方法
 * @description
 */
export declare class UtilsService {
    constructor();
    extend(target?: any, source?: any): any;
    /**
     * 校检手机号
     * @param phone
     */
    validatePhone(phone: string): boolean;
    /**
     * 校验身份证
     * @param IDCard
     */
    validateIDCard(IDCard: string): boolean;
    /**
     * 检验密码  6-18
     * @param passWord
     */
    validatePassWord(passWord: string, min?: number, max?: number): boolean;
    /**
     * 验证邮箱地址
     * @param Email
     */
    validateEmail(email: string): boolean;
    /**
     * 校验车牌
     * @param carNum
     */
    isVehicleNumber(carNum: string): boolean;
    /**
     * 检验银行卡
     * @param bankCard
     */
    validateBankCard(bankCard: string): boolean;
    /**
     * 通过GET请求将图片装换为 BASE64
     * @param url
     */
    convertHttpFileToDataURLviaFileReader(url: any): Promise<any>;
    /**
     * 是不是空
     * @param value
     */
    isEmpty(value: any): boolean;
    /**
     * 不是空
     * @param value
     */
    isNotEmpty(value: any): boolean;
    /**
     * 获取当前月的天数
     * @param date
     */
    getDays(date?: Date): number;
    /**
     * 获取时间{num}前|后 日期
     *   有问题!!!!!!!!!!!!!
     * @param num  + 往后几天  -往前几天
     * @param date  参照日期
     *
     */
    getDate(num: number, date?: Date): Date;
    /**
     * 日期对象转为日期字符串
     * @param date 需要格式化的日期对象
     * @param sFormat 输出格式,默认为yyyy-MM-dd                         年：y，月：M，日：d，时：h，分：m，秒：s
     * @example  dateFormat(new Date())                                "2017-02-28"
     * @example  dateFormat(new Date(),'yyyy-MM-dd')                   "2017-02-28"
     * @example  dateFormat(new Date(),'yyyy-MM-dd hh:mm:ss')         "2017-02-28 09:24:00"
     * @example  dateFormat(new Date(),'hh:mm')                       "09:24"
     * @example  dateFormat(new Date(),'yyyy-MM-ddThh:mm:ss+08:00')   "2017-02-28T09:24:00+08:00"
     * @returns {string}
     */
    dateFormat(date: Date, sFormat?: string): string;
}
