import { HttpCode } from '../constants/constatns';
import { CodeService } from '../code-service/code-service';
import { AppConfig } from '../config/config';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { Loading, Platform } from 'ionic-angular';
import { AlertService } from '../alert-service/alert-service';
import { HTTP } from '@ionic-native/http';
import { Storage } from "@ionic/storage";
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/timeout';
import 'rxjs/add/observable/fromPromise';
import { UtilsService } from '../utils-service/utils-service';
export declare class HttpService {
    private util;
    config: AppConfig;
    http: HttpClient;
    private nativeHttp;
    private storage;
    loadingCtrl: AlertService;
    alertService: AlertService;
    plt: Platform;
    codeService: CodeService;
    _ApiPath: string;
    _loadingText: string;
    loader: Loading;
    _TokenName: string;
    _debug: boolean;
    _headers: HttpHeaders;
    constructor(util: UtilsService, config: AppConfig, http: HttpClient, nativeHttp: HTTP, storage: Storage, loadingCtrl: AlertService, alertService: AlertService, plt: Platform, codeService: CodeService);
    /**
     * 转换
     * @param obj
     */
    private buildHttpParams(obj);
    /**
     * for NativeHttp
     */
    NhttpGetParams(paramRef: any): object;
    /**
 * for NativeHttp
 */
    NhttpPostParams(paramRef: any): object;
    /**
     *
     * @param name 改变loading状态
     * @param status  true or false 是否显示
     */
    private changeLoadingStatus(name, status, message?);
    /**
     * 上传文件流
     * @param url  请求地址
     * @param param  参数
     * @param token  是否携带TOKEN  默认false
     * @param loading 是否显示loading框  默认false
     * @param timeout  超时时间  15 * 1000 ms
     */
    httpUpload(url: any, param: FormData, token?: boolean, loading?: boolean, timeout?: number): Observable<HttpCode<any>>;
    /**
     * post 请求
     * @param url  请求地址
     * @param param  参数
     * @param httpOption responseType設置
     * @param token  是否携带TOKEN  默认false
     * @param loading 是否显示loading框  默认false
     * @param timeout  超时时间  15 * 1000 ms
     */
    httpPost(url: any, param?: any, token?: boolean, loading?: boolean, timeout?: number): Observable<HttpCode<any>>;
    /**
     * get 请求
     * @param url  请求地址
     *@param param   请求参数
     * @param token  是否携带TOKEN  默认false 只有添加额外请求的时候才会加
     * @param loading 是否显示loading框  默认false
     * @param timeout  超时时间  10 * 1000 ms
     */
    httpGet(url: any, param?: any, token?: boolean, loading?: boolean, timeout?: number): Observable<HttpCode<any>>;
    /**
     * 判断错误类型
     *
     * @param error
     */
    private backErrorMsgHandle(error);
    /**
     * 错误提示
     */
    errorHandler: (error: HttpCode<any>) => void;
}
