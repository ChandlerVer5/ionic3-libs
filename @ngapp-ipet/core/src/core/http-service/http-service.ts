import {HttpCode} from '../constants/constatns';
import {CodeService} from '../code-service/code-service';
import {AppConfig} from '../config/config';
import {HttpClient, HttpParams, HttpHeaders} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {Observable} from 'rxjs/Observable';

import { Loading, Platform} from 'ionic-angular';
import { AlertService } from '../alert-service/alert-service';

import {HTTP} from '@ionic-native/http';     // 需要先加载该插件，原生 Http访问！防止CORS
import {Storage} from "@ionic/storage";

import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/timeout';
import 'rxjs/add/observable/fromPromise';
import { UtilsService } from '../utils-service/utils-service';

@Injectable()
export class HttpService {

  _ApiPath: string;
  _loadingText: string;
  loader: Loading = null;
  _TokenName: string;
  _debug: boolean;
  _headers: HttpHeaders;

  constructor(
    private util:UtilsService,
    public config: AppConfig,
              public http: HttpClient,
              private nativeHttp: HTTP,
              private storage: Storage,
              public loadingCtrl: AlertService,
              public alertService: AlertService,
              public plt: Platform,
              public codeService: CodeService) {

    this._ApiPath = this.config.API_Path();
    this._loadingText = this.config.loadingText();
    this._TokenName = this.config.tokenName();
    this._debug = this.config.getDebug();
    //头设置
    if (!this._debug) {
      // initialize plugin
      // this.nativeHttp.setHeader('*',  'Content','application/x-www-form-urlencoded; charset=UTF-8');
      this.nativeHttp.setDataSerializer('urlencoded');
      this.nativeHttp.setRequestTimeout(this.config.HttpTimeOut / 1000);
    } else {
      this._headers = new HttpHeaders({'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'});
    }
    if (this.config.getExtraHeader()) {
      this._headers.append("version", this.config.getVersion());
      //android  ios windows
      if (this.plt.is('android')) {
        this._headers.append("platform", "1");
      } else if (this.plt.is('ios')) {
        this._headers.append("platform", "2");
      } else if (this.plt.is('windows')) {
        this._headers.append("platform", "3");
      } else {
        this._headers.append("platform", "0");
      }
    }
  }

  /**
   * 转换
   * @param obj
   */
  private buildHttpParams(obj: any): HttpParams {
    // obj = JSON.parse(JSON.stringify(obj));
    let params = {};
    for (let key in obj) {
      let val = obj[key];
      if (typeof val == 'object') {
        val = JSON.stringify(val);
      }
      if (typeof val == 'undefined') {
        val = '';
        continue;
      }
      params[key] = val;
    }
    return new HttpParams({fromObject: params});
  }

  /**
   * for NativeHttp
   */
  public NhttpGetParams(paramRef): object {
    if (!paramRef) {
      return {};
    }
    for (const key in paramRef) {
      if (paramRef.hasOwnProperty(key)) {
        if (paramRef[key] === undefined || paramRef[key] === null) {
          paramRef[key] = '';
          continue;
        }
        paramRef[key] = paramRef[key].toString();
      }
    }
    return paramRef;
  }

      /**
   * for NativeHttp
   */
  public NhttpPostParams(paramRef): object {
    let params = {};
    for (const key in paramRef) {
      let val = paramRef[key];
      if (paramRef.hasOwnProperty(key)) {
        if (typeof val == 'object') {
          val = JSON.stringify(val);
        }
        if (val === undefined || val === null) {
          val = '';
          continue;
        }
      }
      params[key] = val;
    }
    return params;
  }
  /**
   *
   * @param name 改变loading状态
   * @param status  true or false 是否显示
   */
  private changeLoadingStatus(name: string, status: boolean, message?:string) {
    if (status) {
      this.loader && this.loader.dismiss();
      return this.loader = this.loadingCtrl.loading({
        content: this._loadingText || message
      });
    } else {
      if (this.loader) {
        return this.loader.dismiss().then(res => {
          this.loader = null;
        });
      }
    }
  }

  /**
   * 上传文件流
   * @param url  请求地址
   * @param param  参数
   * @param token  是否携带TOKEN  默认false
   * @param loading 是否显示loading框  默认false
   * @param timeout  超时时间  15 * 1000 ms
   */
  public httpUpload(url, param: FormData, token: boolean = false, loading: boolean = false, timeout: number = this.config.HttpTimeOut): Observable<HttpCode<any>> {
    loading && this.changeLoadingStatus(url, true);//加载中

    if (this.config.getExtraHeader()) { //额外请求头
      this._headers.delete(this._TokenName);
      token && (this._headers.append(this._TokenName, window.localStorage[this._TokenName]));
    } else {
      if (token) {
        param.append(this._TokenName, window.localStorage[this._TokenName]);
      }
    }
    return this.http.post(this._ApiPath + url, param, {
      headers: this._headers,
      ...(this.config.getHttpOptions())
    }).map(response => {
      loading && this.changeLoadingStatus(url, false);//加载中
      let httpCode = response as HttpCode<any>;
      if (!httpCode.success) {
        throw Observable.throw(httpCode);
      }
      return httpCode;
    }).timeout(timeout).catch((error: any) => {
      loading && this.changeLoadingStatus(url, false);//加载中
      return this.backErrorMsgHandle(error);
    });
  }

  /**
   * post 请求
   * @param url  请求地址
   * @param param  参数
   * @param httpOption responseType設置
   * @param token  是否携带TOKEN  默认false
   * @param loading 是否显示loading框  默认false
   * @param timeout  超时时间  15 * 1000 ms
   */
  public httpPost(url, param: any = {},token: boolean = false, loading: boolean = false, timeout: number = this.config.HttpTimeOut): Observable<HttpCode<any>> {
    loading && this.changeLoadingStatus(url, true);//加载中

    if (this.config.getExtraHeader()) { //额外请求头
      this._headers.delete(this._TokenName);
      token && (this._headers.append(this._TokenName, window.localStorage[this._TokenName]));
    } else {
      token && (param[this._TokenName] = window.localStorage[this._TokenName]); // 是否添加TOKEN
    }

    // DEBUG为true时，会启用mock数据源！，这里解决CORS问题，启用 原生Http请求！ modified
    if (this._debug) {
      /*      let result = this.config.post(url, Object.assign({}, param, {token: window.localStorage[this._TokenName]}));
            if (result) {
              return result;
            }*/
      return this.http.post(this._ApiPath + url,
        this.buildHttpParams(param), {
          headers: this._headers,
          ...this.config.getHttpOptions()
        }).map(response => {
        loading && this.changeLoadingStatus(url, false);//加载中
        let httpCode = response as HttpCode<any>;
        /*if (!response['ok']) {
            throw Observable.throw(httpCode);
        }*/
        return httpCode;
      }).timeout(timeout).catch((error: any) => {
        loading && this.changeLoadingStatus(url, false);//加载中
        return this.backErrorMsgHandle(error);
      });
    }


    return Observable.fromPromise(this.plt.ready().then((ready) => {
      // alert("POSTDATA"+ JSON.stringify(this.NhttpPostParams(param)));
      return this.nativeHttp.post(this._ApiPath + url, this.NhttpPostParams(param), {})
    }))
      .map(response => {
        //alert('POSTCookies::'+this.nativeHttp.getCookieString(this._ApiPath+url));
          console.log("nativeHttp+POST++",JSON.stringify(response));
        loading && this.changeLoadingStatus(url, false);//加载中
        // let httpCode = JSON.parse(response) as HttpCode<any>;
        /*if (!response['ok']) {
            throw Observable.throw(httpCode);
        }*/
        return JSON.parse(response['data']);
      }).catch((error: any) => {
        loading && this.changeLoadingStatus(url, false);//加载中
        return this.backErrorMsgHandle(error);
      });
  }

  /**
   * get 请求
   * @param url  请求地址
   *@param param   请求参数
   * @param token  是否携带TOKEN  默认false 只有添加额外请求的时候才会加
   * @param loading 是否显示loading框  默认false
   * @param timeout  超时时间  10 * 1000 ms
   */
  public httpGet(url, param: any = {}, token: boolean = false, loading: boolean = false, timeout: number = this.config.HttpTimeOut): Observable<HttpCode<any>> {
    loading && this.changeLoadingStatus(url, true);//加载中

    if (this.config.getExtraHeader()) {//额外请求头
      this._headers.delete(this._TokenName);
      token && (this._headers.append(this._TokenName, window.localStorage[this._TokenName]));
    }

    if (this._debug) {
      /*let result = this.config.get(url, {});
      if (result) {
        return result;
      }*/
      return this.http.get(this._ApiPath + url, {
        headers: this._headers,
        params: this.buildHttpParams(param),
        ...(this.config.getHttpOptions())
      }).map(response => {
        loading && this.changeLoadingStatus(url, false);//加载中
        let httpCode = response as HttpCode<any>;
        /*      if (!response['ok']) {
                throw Observable.throw(httpCode);
              }*/
        return httpCode;
      }).timeout(timeout).catch((error: any) => {
        loading && this.changeLoadingStatus(url, false);//加载中
        return this.backErrorMsgHandle(error);
      });
    }

    return Observable.fromPromise(this.plt.ready().then((ready) => {
      return this.nativeHttp.get(this._ApiPath + url, this.NhttpGetParams(param), {})
    }))
      .map(response => {
        //初设 cookie
        if(url.indexOf('memberinfo/getUserInfo.do') >=0 && response.headers['set-cookie']){
          this.nativeHttp.setCookie(this._ApiPath,this.nativeHttp.getCookieString(this._ApiPath+url))
        }
        console.log("nativeHttp+GET++", JSON.stringify(response));
        loading && this.changeLoadingStatus(url, false);//加载中
        // let httpCode = response.json() as HttpCode<any>;
        /*if (!response['ok']) {
            throw Observable.throw(httpCode);
        }*/
        return JSON.parse(response['data']);
      }).catch((error: any) => {
        loading && this.changeLoadingStatus(url, false);//加载中
        return this.backErrorMsgHandle(error);
      });
  }


  /**
   * 判断错误类型
   *
   * @param error
   */
  private backErrorMsgHandle(error: any) {
    console.error("❌error",error);
    if (error.error && !error.status) {
        // return Observable.fromPromise(this.alertService.alert(error.message));
      return Observable.throw(error.error);
    }
    const data = {status: error.status, error};
    if (error.status === 400) {
      return Observable.throw({code: '400', message: '请求参数出现未知错误', data});
    }
    if (error.status === 401) {
      return Observable.throw({code: '401', message: '未经授权，访问被拒绝', data});
    }
    if (error.status === 404) {
      return Observable.throw({code: '404', message: '请求链接不存在，请联系管理员', data});
    }
    if (error.status === 500) {
      return Observable.throw({code: '500', message: '服务器出错，请稍后再试', data});
    }
    if (error.status === 502) {
      return Observable.throw({code: '502', message: '服务器维护，请稍后再试', data});
    }
    if (error.status === 0) {
      return Observable.throw({code: '0', message: '请求响应错误，请检查网络', data});
    }
    if (!this._debug && error.status === 1) {
      return Observable.throw({code: '506', message: '服务请求超时', data});
    }
    if (error.name = "TimeoutError") {
      return Observable.throw({code: '506', message: '服务请求超时', data});
    }
    return Observable.throw({code: '600', message: '未知错误，请检查网络', data});
  }

  /**
   * 错误提示
   */
  public errorHandler = (error: HttpCode<any>) => {
    this.codeService.httpCodeHandle(error);//根据返回码响应
  }

}
