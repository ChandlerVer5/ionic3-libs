var __assign = (this && this.__assign) || Object.assign || function(t) {
    for (var s, i = 1, n = arguments.length; i < n; i++) {
        s = arguments[i];
        for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
            t[p] = s[p];
    }
    return t;
};
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
import { CodeService } from '../code-service/code-service';
import { AppConfig } from '../config/config';
import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Platform } from 'ionic-angular';
import { AlertService } from '../alert-service/alert-service';
import { HTTP } from '@ionic-native/http'; // 需要先加载该插件，原生 Http访问！防止CORS
import { Storage } from "@ionic/storage";
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/timeout';
import 'rxjs/add/observable/fromPromise';
import { UtilsService } from '../utils-service/utils-service';
var HttpService = /** @class */ (function () {
    function HttpService(util, config, http, nativeHttp, storage, loadingCtrl, alertService, plt, codeService) {
        var _this = this;
        this.util = util;
        this.config = config;
        this.http = http;
        this.nativeHttp = nativeHttp;
        this.storage = storage;
        this.loadingCtrl = loadingCtrl;
        this.alertService = alertService;
        this.plt = plt;
        this.codeService = codeService;
        this.loader = null;
        /**
         * 错误提示
         */
        this.errorHandler = function (error) {
            _this.codeService.httpCodeHandle(error); //根据返回码响应
        };
        this._ApiPath = this.config.API_Path();
        this._loadingText = this.config.loadingText();
        this._TokenName = this.config.tokenName();
        this._debug = this.config.getDebug();
        //头设置
        if (!this._debug) {
            // initialize plugin
            // this.nativeHttp.setHeader('*',  'Content','application/x-www-form-urlencoded; charset=UTF-8');
            this.nativeHttp.setDataSerializer('urlencoded');
            this.nativeHttp.setRequestTimeout(this.config.HttpTimeOut / 1000);
        }
        else {
            this._headers = new HttpHeaders({ 'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8' });
        }
        if (this.config.getExtraHeader()) {
            this._headers.append("version", this.config.getVersion());
            //android  ios windows
            if (this.plt.is('android')) {
                this._headers.append("platform", "1");
            }
            else if (this.plt.is('ios')) {
                this._headers.append("platform", "2");
            }
            else if (this.plt.is('windows')) {
                this._headers.append("platform", "3");
            }
            else {
                this._headers.append("platform", "0");
            }
        }
    }
    /**
     * 转换
     * @param obj
     */
    HttpService.prototype.buildHttpParams = function (obj) {
        // obj = JSON.parse(JSON.stringify(obj));
        var params = {};
        for (var key in obj) {
            var val = obj[key];
            if (typeof val == 'object') {
                val = JSON.stringify(val);
            }
            if (typeof val == 'undefined') {
                val = '';
                continue;
            }
            params[key] = val;
        }
        return new HttpParams({ fromObject: params });
    };
    /**
     * for NativeHttp
     */
    HttpService.prototype.NhttpGetParams = function (paramRef) {
        if (!paramRef) {
            return {};
        }
        for (var key in paramRef) {
            if (paramRef.hasOwnProperty(key)) {
                if (paramRef[key] === undefined || paramRef[key] === null) {
                    paramRef[key] = '';
                    continue;
                }
                paramRef[key] = paramRef[key].toString();
            }
        }
        return paramRef;
    };
    /**
 * for NativeHttp
 */
    HttpService.prototype.NhttpPostParams = function (paramRef) {
        var params = {};
        for (var key in paramRef) {
            var val = paramRef[key];
            if (paramRef.hasOwnProperty(key)) {
                if (typeof val == 'object') {
                    val = JSON.stringify(val);
                }
                if (val === undefined || val === null) {
                    val = '';
                    continue;
                }
            }
            params[key] = val;
        }
        return params;
    };
    /**
     *
     * @param name 改变loading状态
     * @param status  true or false 是否显示
     */
    HttpService.prototype.changeLoadingStatus = function (name, status, message) {
        var _this = this;
        if (status) {
            this.loader && this.loader.dismiss();
            return this.loader = this.loadingCtrl.loading({
                content: this._loadingText || message
            });
        }
        else {
            if (this.loader) {
                return this.loader.dismiss().then(function (res) {
                    _this.loader = null;
                });
            }
        }
    };
    /**
     * 上传文件流
     * @param url  请求地址
     * @param param  参数
     * @param token  是否携带TOKEN  默认false
     * @param loading 是否显示loading框  默认false
     * @param timeout  超时时间  15 * 1000 ms
     */
    HttpService.prototype.httpUpload = function (url, param, token, loading, timeout) {
        var _this = this;
        if (token === void 0) { token = false; }
        if (loading === void 0) { loading = false; }
        if (timeout === void 0) { timeout = this.config.HttpTimeOut; }
        loading && this.changeLoadingStatus(url, true); //加载中
        if (this.config.getExtraHeader()) {
            this._headers.delete(this._TokenName);
            token && (this._headers.append(this._TokenName, window.localStorage[this._TokenName]));
        }
        else {
            if (token) {
                param.append(this._TokenName, window.localStorage[this._TokenName]);
            }
        }
        return this.http.post(this._ApiPath + url, param, __assign({ headers: this._headers }, (this.config.getHttpOptions()))).map(function (response) {
            loading && _this.changeLoadingStatus(url, false); //加载中
            var httpCode = response;
            if (!httpCode.success) {
                throw Observable.throw(httpCode);
            }
            return httpCode;
        }).timeout(timeout).catch(function (error) {
            loading && _this.changeLoadingStatus(url, false); //加载中
            return _this.backErrorMsgHandle(error);
        });
    };
    /**
     * post 请求
     * @param url  请求地址
     * @param param  参数
     * @param httpOption responseType設置
     * @param token  是否携带TOKEN  默认false
     * @param loading 是否显示loading框  默认false
     * @param timeout  超时时间  15 * 1000 ms
     */
    HttpService.prototype.httpPost = function (url, param, token, loading, timeout) {
        var _this = this;
        if (param === void 0) { param = {}; }
        if (token === void 0) { token = false; }
        if (loading === void 0) { loading = false; }
        if (timeout === void 0) { timeout = this.config.HttpTimeOut; }
        loading && this.changeLoadingStatus(url, true); //加载中
        if (this.config.getExtraHeader()) {
            this._headers.delete(this._TokenName);
            token && (this._headers.append(this._TokenName, window.localStorage[this._TokenName]));
        }
        else {
            token && (param[this._TokenName] = window.localStorage[this._TokenName]); // 是否添加TOKEN
        }
        // DEBUG为true时，会启用mock数据源！，这里解决CORS问题，启用 原生Http请求！ modified
        if (this._debug) {
            /*      let result = this.config.post(url, Object.assign({}, param, {token: window.localStorage[this._TokenName]}));
                  if (result) {
                    return result;
                  }*/
            return this.http.post(this._ApiPath + url, this.buildHttpParams(param), __assign({ headers: this._headers }, this.config.getHttpOptions())).map(function (response) {
                loading && _this.changeLoadingStatus(url, false); //加载中
                var httpCode = response;
                /*if (!response['ok']) {
                    throw Observable.throw(httpCode);
                }*/
                return httpCode;
            }).timeout(timeout).catch(function (error) {
                loading && _this.changeLoadingStatus(url, false); //加载中
                return _this.backErrorMsgHandle(error);
            });
        }
        return Observable.fromPromise(this.plt.ready().then(function (ready) {
            // alert("POSTDATA"+ JSON.stringify(this.NhttpPostParams(param)));
            return _this.nativeHttp.post(_this._ApiPath + url, _this.NhttpPostParams(param), {});
        }))
            .map(function (response) {
            //alert('POSTCookies::'+this.nativeHttp.getCookieString(this._ApiPath+url));
            console.log("nativeHttp+POST++", JSON.stringify(response));
            loading && _this.changeLoadingStatus(url, false); //加载中
            // let httpCode = JSON.parse(response) as HttpCode<any>;
            /*if (!response['ok']) {
                throw Observable.throw(httpCode);
            }*/
            return JSON.parse(response['data']);
        }).catch(function (error) {
            loading && _this.changeLoadingStatus(url, false); //加载中
            return _this.backErrorMsgHandle(error);
        });
    };
    /**
     * get 请求
     * @param url  请求地址
     *@param param   请求参数
     * @param token  是否携带TOKEN  默认false 只有添加额外请求的时候才会加
     * @param loading 是否显示loading框  默认false
     * @param timeout  超时时间  10 * 1000 ms
     */
    HttpService.prototype.httpGet = function (url, param, token, loading, timeout) {
        var _this = this;
        if (param === void 0) { param = {}; }
        if (token === void 0) { token = false; }
        if (loading === void 0) { loading = false; }
        if (timeout === void 0) { timeout = this.config.HttpTimeOut; }
        loading && this.changeLoadingStatus(url, true); //加载中
        if (this.config.getExtraHeader()) {
            this._headers.delete(this._TokenName);
            token && (this._headers.append(this._TokenName, window.localStorage[this._TokenName]));
        }
        if (this._debug) {
            /*let result = this.config.get(url, {});
            if (result) {
              return result;
            }*/
            return this.http.get(this._ApiPath + url, __assign({ headers: this._headers, params: this.buildHttpParams(param) }, (this.config.getHttpOptions()))).map(function (response) {
                loading && _this.changeLoadingStatus(url, false); //加载中
                var httpCode = response;
                /*      if (!response['ok']) {
                        throw Observable.throw(httpCode);
                      }*/
                return httpCode;
            }).timeout(timeout).catch(function (error) {
                loading && _this.changeLoadingStatus(url, false); //加载中
                return _this.backErrorMsgHandle(error);
            });
        }
        return Observable.fromPromise(this.plt.ready().then(function (ready) {
            return _this.nativeHttp.get(_this._ApiPath + url, _this.NhttpGetParams(param), {});
        }))
            .map(function (response) {
            //初设 cookie
            if (url.indexOf('memberinfo/getUserInfo.do') >= 0 && response.headers['set-cookie']) {
                _this.nativeHttp.setCookie(_this._ApiPath, _this.nativeHttp.getCookieString(_this._ApiPath + url));
            }
            console.log("nativeHttp+GET++", JSON.stringify(response));
            loading && _this.changeLoadingStatus(url, false); //加载中
            // let httpCode = response.json() as HttpCode<any>;
            /*if (!response['ok']) {
                throw Observable.throw(httpCode);
            }*/
            return JSON.parse(response['data']);
        }).catch(function (error) {
            loading && _this.changeLoadingStatus(url, false); //加载中
            return _this.backErrorMsgHandle(error);
        });
    };
    /**
     * 判断错误类型
     *
     * @param error
     */
    HttpService.prototype.backErrorMsgHandle = function (error) {
        console.error("❌error", error);
        if (error.error && !error.status) {
            // return Observable.fromPromise(this.alertService.alert(error.message));
            return Observable.throw(error.error);
        }
        var data = { status: error.status, error: error };
        if (error.status === 400) {
            return Observable.throw({ code: '400', message: '请求参数出现未知错误', data: data });
        }
        if (error.status === 401) {
            return Observable.throw({ code: '401', message: '未经授权，访问被拒绝', data: data });
        }
        if (error.status === 404) {
            return Observable.throw({ code: '404', message: '请求链接不存在，请联系管理员', data: data });
        }
        if (error.status === 500) {
            return Observable.throw({ code: '500', message: '服务器出错，请稍后再试', data: data });
        }
        if (error.status === 502) {
            return Observable.throw({ code: '502', message: '服务器维护，请稍后再试', data: data });
        }
        if (error.status === 0) {
            return Observable.throw({ code: '0', message: '请求响应错误，请检查网络', data: data });
        }
        if (!this._debug && error.status === 1) {
            return Observable.throw({ code: '506', message: '服务请求超时', data: data });
        }
        if (error.name = "TimeoutError") {
            return Observable.throw({ code: '506', message: '服务请求超时', data: data });
        }
        return Observable.throw({ code: '600', message: '未知错误，请检查网络', data: data });
    };
    HttpService = __decorate([
        Injectable(),
        __metadata("design:paramtypes", [UtilsService,
            AppConfig,
            HttpClient,
            HTTP,
            Storage,
            AlertService,
            AlertService,
            Platform,
            CodeService])
    ], HttpService);
    return HttpService;
}());
export { HttpService };
//# sourceMappingURL=http-service.js.map