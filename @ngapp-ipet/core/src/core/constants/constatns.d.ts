/**
 * 返回码
 */
export declare class HttpCode<T> {
    /**
        * The status number of the response
        */
    status: number;
    /**
     * The headers of the response
     */
    headers: any;
    /**
     * The URL of the response. This property will be the final URL obtained after any redirects.
     */
    url: string;
    code: string;
    message?: string;
    data?: T;
    error?: string;
    success?: boolean;
}
/**
 * 分页
 */
export declare class Page<T> {
    page: number;
    pageNumber: number;
    list: T[];
    firstPage: boolean;
    lastPage: boolean;
    pageSize: number;
    totalPage: number;
    totalRow: number;
    constructor();
}
export interface Config {
    "BASE_DOMAIN"?: string;
    "BASE_API"?: string;
    "LOADING_TEXT"?: string;
    "TOKEN_NAME"?: string;
    "HTTP_DEBUG"?: boolean;
    "EXTRA_HEARDE"?: boolean;
    "APP_VERSION"?: string;
    "HTTP_OPTIONS"?: {
        "withCredentials"?: boolean;
    };
    "HTTP_TIME_OUT"?: number;
}
/**
 * 数据处理工具
 */
export declare class DateUtils {
    /**
     * 处理分页数据
     *
     *
     *
     *
     * @param oldPage 老分页数据
     * @param newPage 新的分页数据
     */
    static renderPageData<T>(oldPage: Page<T>, newPage: Page<T>): Page<T>;
    /**
     * 处理分页(并将数组内的元素转为对应类型)
     *
     * @param oldPage
     * @param newPage
     */
    static renderPage<T>(oldPage: Page<T>, newPage: Page<T>, newInatance: {
        new (): T;
    }): Page<T>;
}
export declare class Constants {
    /**
     * 服务器地址
     */
    static BASE_DOMAIN: string;
    /**
     * 接口地址
     */
    static BASE_API: string;
    /**
     * 加载框文字
     */
    static LOADING_TEXT: string;
    /**
     * POST 默认TOKEN的名字
     *
     * POST 默认传递参数 [TOKEN_NAME]=window.localStorage[TOKEN_NAME]
     */
    static TOKEN_NAME: string;
    /**
     * debug
     */
    static HTTP_DEBUG: string;
    /**
     * 版本号
     */
    static APP_VERSION: string;
    /**
     * 是否启用额外请求头
     */
    static EXTRA_HEARDE: string;
}
