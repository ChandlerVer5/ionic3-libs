var __assign = (this && this.__assign) || Object.assign || function(t) {
    for (var s, i = 1, n = arguments.length; i < n; i++) {
        s = arguments[i];
        for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
            t[p] = s[p];
    }
    return t;
};
/**
 * 返回码
 */
var HttpCode = /** @class */ (function () {
    function HttpCode() {
    }
    return HttpCode;
}());
export { HttpCode };
/**
 * 分页
 */
var Page = /** @class */ (function () {
    function Page() {
        this.page = 1;
        this.pageNumber = 1;
        this.list = [];
        this.firstPage = true;
        this.lastPage = true;
        this.pageSize = 10;
        this.totalPage = 1;
        this.totalRow = 0;
    }
    return Page;
}());
export { Page };
/**
 * 数据处理工具
 */
var DateUtils = /** @class */ (function () {
    function DateUtils() {
    }
    /**
     * 处理分页数据
     *
     *
     *
     *
     * @param oldPage 老分页数据
     * @param newPage 新的分页数据
     */
    DateUtils.renderPageData = function (oldPage, newPage) {
        var list = newPage.list, page = newPage.page, pageNumber = newPage.pageNumber, pageSize = newPage.pageSize, firstPage = newPage.firstPage, lastPage = newPage.lastPage, totalPage = newPage.totalPage, totalRow = newPage.totalRow;
        oldPage = oldPage || new Page();
        isNaN(page) && (page = pageNumber);
        if (1 == page) {
            oldPage.list = [];
        }
        if (oldPage.page == page) {
        }
        return __assign({}, oldPage, { list: oldPage.list.concat(list), page: page,
            pageNumber: pageNumber, pageSize: pageSize, firstPage: firstPage, lastPage: lastPage, totalPage: totalPage, totalRow: totalRow });
    };
    /**
     * 处理分页(并将数组内的元素转为对应类型)
     *
     * @param oldPage
     * @param newPage
     */
    DateUtils.renderPage = function (oldPage, newPage, newInatance) {
        var list = newPage.list, pageNumber = newPage.pageNumber, pageSize = newPage.pageSize, firstPage = newPage.firstPage, lastPage = newPage.lastPage, totalPage = newPage.totalPage, totalRow = newPage.totalRow;
        oldPage = oldPage || new Page();
        if (1 == pageNumber) {
            oldPage.list = new Array();
        }
        if (oldPage.pageNumber == pageNumber) {
        }
        return __assign({}, oldPage, { list: oldPage.list.concat(list.reduce(function (prev, cur) {
                prev.push(Object.assign(new newInatance(), cur));
                return prev;
            }, [])), pageNumber: pageNumber, pageSize: pageSize, firstPage: firstPage, lastPage: lastPage, totalPage: totalPage, totalRow: totalRow });
    };
    return DateUtils;
}());
export { DateUtils };
var Constants = /** @class */ (function () {
    function Constants() {
    }
    /**
     * 服务器地址
     */
    Constants.BASE_DOMAIN = "BASE_DOMAIN";
    /**
     * 接口地址
     */
    Constants.BASE_API = 'BASE_API';
    /**
     * 加载框文字
     */
    Constants.LOADING_TEXT = "LOADING_TEXT";
    /**
     * POST 默认TOKEN的名字
     *
     * POST 默认传递参数 [TOKEN_NAME]=window.localStorage[TOKEN_NAME]
     */
    Constants.TOKEN_NAME = "TOKEN_NAME";
    /**
     * debug
     */
    Constants.HTTP_DEBUG = "HTTP_DEBUG";
    /**
     * 版本号
     */
    Constants.APP_VERSION = "APP_VERSION";
    /**
     * 是否启用额外请求头
     */
    Constants.EXTRA_HEARDE = "EXTRA_HEARDE";
    return Constants;
}());
export { Constants };
//# sourceMappingURL=constatns.js.map