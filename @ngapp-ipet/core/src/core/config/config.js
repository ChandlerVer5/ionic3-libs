var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __param = (this && this.__param) || function (paramIndex, decorator) {
    return function (target, key) { decorator(target, key, paramIndex); }
};
import { Observable } from 'rxjs/Observable';
import { ConfigToken, MOCK_HTTP_TOKEN } from '../core/other';
import { Constants } from '../constants/constatns';
import { Injectable, Inject, Optional } from '@angular/core';
import { isUndefined } from '../../utils/utils';
/**
 * 默认值
 */
var _initConfig = {
    "BASE_DOMAIN": 'http://localhost/',
    "BASE_API": 'api/',
    "LOADING_TEXT": '',
    "TOKEN_NAME": 'TOKEN',
    "HTTP_DEBUG": false,
    "EXTRA_HEARDE": true,
    "HTTP_OPTIONS": {
        "withCredentials": false
    },
    "HTTP_TIME_OUT": 10000
};
var AppConfig = /** @class */ (function () {
    function AppConfig(config, providers) {
        var _this = this;
        this.providers = [];
        this._config = Object.assign(_initConfig, config);
        if (!this.getVersion()) {
            this.getAppVersion().then(function (version) {
                console.log(JSON.stringify(version));
                _this._config[Constants.APP_VERSION] = version.data.versionCode;
            });
        }
        if (providers) {
            this.providers = providers.filter(function (info) { return info.get && info.post; });
        }
    }
    AppConfig.prototype.getAppVersion = function () {
        return new Promise(function (resolve) {
            var json = {
                "callback": "xviewAppVersionCallback"
            };
            window['xviewAppVersionCallback'] = function (param) {
                resolve(param);
            };
            try {
                window['xview'].xviewAppVersion(JSON.stringify(json));
            }
            catch (e) {
                console.warn("Not Found xviewAppVersion Method");
                window['xviewAppVersionCallback']({ code: '0', data: { version: '1.0.0', bundle: '测试版本' }, message: '测试版本' });
            }
        });
    };
    AppConfig.prototype.get = function (url, param) {
        var httpCode;
        for (var _i = 0, _a = this.providers; _i < _a.length; _i++) {
            var provider = _a[_i];
            httpCode = provider.get(url, param);
            if (httpCode) {
                return Observable.create(function (observer) {
                    setTimeout(function () { return observer.next(httpCode); }, 500);
                });
            }
        }
        return null;
    };
    AppConfig.prototype.post = function (url, param) {
        var httpCode;
        for (var _i = 0, _a = this.providers; _i < _a.length; _i++) {
            var provider = _a[_i];
            httpCode = provider.post(url, param);
            if (httpCode) {
                return Observable.create(function (observer) {
                    setTimeout(function () { return observer.next(httpCode); }, 500);
                });
            }
        }
        return null;
    };
    AppConfig.prototype.domain = function () {
        return this._config.BASE_DOMAIN;
    };
    AppConfig.prototype.API_Path = function () {
        return this.domain() + this._config.BASE_API;
    };
    AppConfig.prototype.getDebug = function () {
        return this._config.HTTP_DEBUG;
    };
    AppConfig.prototype.loadingText = function () {
        return this._config.LOADING_TEXT;
    };
    AppConfig.prototype.tokenName = function () {
        return this._config.TOKEN_NAME;
    };
    AppConfig.prototype.getValue = function (key, defaultValue) {
        if (isUndefined(this._config[key]))
            return defaultValue;
        return this._config[key];
    };
    AppConfig.prototype.getVersion = function () {
        return this._config.APP_VERSION;
    };
    AppConfig.prototype.getExtraHeader = function () {
        return this._config.EXTRA_HEARDE;
    };
    AppConfig.prototype.getHttpOptions = function () {
        return this._config.HTTP_OPTIONS;
    };
    Object.defineProperty(AppConfig.prototype, "HttpTimeOut", {
        get: function () {
            return this._config.HTTP_TIME_OUT;
        },
        enumerable: true,
        configurable: true
    });
    AppConfig = __decorate([
        Injectable(),
        __param(0, Inject(ConfigToken)), __param(1, Optional()), __param(1, Inject(MOCK_HTTP_TOKEN)),
        __metadata("design:paramtypes", [Object, Array])
    ], AppConfig);
    return AppConfig;
}());
export { AppConfig };
//# sourceMappingURL=config.js.map