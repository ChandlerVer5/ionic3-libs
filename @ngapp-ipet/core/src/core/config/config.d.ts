import { Observable } from 'rxjs/Observable';
import { MockHttp } from '../core/other';
import { HttpCode, Config } from '../constants/constatns';
export declare class AppConfig {
    _config: Config;
    providers: MockHttp[];
    constructor(config: Config, providers: MockHttp[]);
    getAppVersion(): Promise<{
        code: string;
        data: {
            versionCode: string;
            versionName: string;
        };
        message: string;
    }>;
    get(url: any, param: any): Observable<HttpCode<any>>;
    post(url: any, param: any): Observable<HttpCode<any>>;
    domain(): string;
    API_Path(): string;
    getDebug(): boolean;
    loadingText(): string;
    tokenName(): string;
    getValue(key: string, defaultValue?: any): any;
    getVersion(): string;
    getExtraHeader(): boolean;
    getHttpOptions(): {
        "withCredentials"?: boolean;
    };
    readonly HttpTimeOut: number;
}
