import { Observable } from 'rxjs/Observable';
import { MockHttp, ConfigToken, MOCK_HTTP_TOKEN } from '../core/other';
import { Constants, HttpCode, Config } from '../constants/constatns';
import { Injectable, Inject, Optional } from '@angular/core';
import { isUndefined } from '../../utils/utils';

/**
 * 默认值
 */
const _initConfig: Config = {
    "BASE_DOMAIN": 'http://localhost/',
    "BASE_API": 'api/',
    "LOADING_TEXT": '',
    "TOKEN_NAME": 'TOKEN',
    "HTTP_DEBUG": false,
    "EXTRA_HEARDE": true,
    "HTTP_OPTIONS": {
        "withCredentials": false
    },
    "HTTP_TIME_OUT": 10000
}

@Injectable()
export class AppConfig {
    _config: Config;

    providers: MockHttp[] = [];

    constructor( @Inject(ConfigToken) config: Config, @Optional() @Inject(MOCK_HTTP_TOKEN) providers: MockHttp[]) {
        this._config = Object.assign(_initConfig, config);
        if (!this.getVersion()) {
            this.getAppVersion().then(version => {
                console.log(JSON.stringify(version));
                this._config[Constants.APP_VERSION] = version.data.versionCode;
            });
        }
        if (providers) {
            this.providers = providers.filter((info) => info.get && info.post);
        }

    }


    public getAppVersion(): Promise<{
        code: string;
        data: {
            versionCode: string;// "版本号 - android 升级判断使用",
            versionName: string;
        };
        message: string
    }> {
        return new Promise((resolve) => {
            let json = {
                "callback": "xviewAppVersionCallback"
            };
            window['xviewAppVersionCallback'] = function (param) {
                resolve(param as {
                    code: string;
                    data: {
                        versionCode: string;// "版本号 - android 升级判断使用",
                        versionName: string;
                    }; message: string
                });
            };
            try {
                window['xview'].xviewAppVersion(JSON.stringify(json));
            }
            catch (e) {
                console.warn("Not Found xviewAppVersion Method");
                window['xviewAppVersionCallback']({ code: '0', data: { version: '1.0.0', bundle: '测试版本' }, message: '测试版本' });
            }
        });
    }

    get(url, param): Observable<HttpCode<any>> {
        let httpCode: HttpCode<any>;
        for (let provider of this.providers) {
            httpCode = provider.get(url, param);
            if (httpCode) {
                return Observable.create((observer) => {
                    setTimeout(() => observer.next(httpCode), 500);
                });
            }
        }
        return null;
    }

    post(url, param): Observable<HttpCode<any>> {
        let httpCode: HttpCode<any>;
        for (let provider of this.providers) {
            httpCode = provider.post(url, param);
            if (httpCode) {
                return Observable.create((observer) => {
                    setTimeout(() => observer.next(httpCode), 500);
                });
            }
        }
        return null;
    }

    domain() {
        return this._config.BASE_DOMAIN;
    }

    API_Path(): string {
        return this.domain() + this._config.BASE_API;
    }

    getDebug(): boolean {
        return this._config.HTTP_DEBUG;
    }

    loadingText() {
        return this._config.LOADING_TEXT;
    }

    tokenName() {
        return this._config.TOKEN_NAME;
    }

    getValue(key: string, defaultValue?: any): any {
        if (isUndefined(this._config[key])) return defaultValue;
        return this._config[key];
    }

    getVersion() {
        return this._config.APP_VERSION;
    }

    getExtraHeader() {
        return this._config.EXTRA_HEARDE;
    }

    getHttpOptions() {
        return this._config.HTTP_OPTIONS;
    }

    get HttpTimeOut() {
        return this._config.HTTP_TIME_OUT;
    }
}
