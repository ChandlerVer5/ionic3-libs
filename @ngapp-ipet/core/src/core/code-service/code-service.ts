import { Subject } from 'rxjs/Subject';
import { HttpCode } from '../constants/constatns';
import { AlertService } from '../alert-service/alert-service';
import { Injectable } from '@angular/core';
import 'rxjs/add/operator/debounceTime'

/**
 * code event
 */
export interface CodeEvent {
    code: string;
    message: string;
    fn?: Function;
}

export interface CodeHander {

    httpCodeHandle(error: HttpCode<any>);
}
/**
 * Http 状态
 */
@Injectable()
export class CodeService implements CodeHander {

    subject: Subject<HttpCode<any>> = new Subject<HttpCode<any>>();

    constructor(public alertService: AlertService) {
        this.subject.asObservable().debounceTime(800).subscribe(error => {
            this.alertService.alert(error.message);
        });
    }

    /**
     * 处理Http 返回码
     * @param code
     */
    public httpCodeHandle(error: HttpCode<any>) {
        this.subject.next(error);
    }
}