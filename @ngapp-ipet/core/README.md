


# @ngapp/core

有一些公共的方法

# 使用指南

## 安装 `@ngapp/core`

    npm i @ngapp/core --save

## 使用 `@ngapp/core`

### AppCoreModule 的引入
在app.module.ts imports内 引入 `AppCoreModule`
 
    AppCoreModule.config({
      BASE_API: 'api/',
      BASE_DOMAIN: 'http://192.168.2.139:3000/',
      HTTP_DEBUG: true,
      APP_VERSION: '1.0.0',
      EXTRA_HEARDE: true, 
    })

config 默认配置如下

    const _initConfig: { [key: string]: any } = {
    	BASE_DOMAIN: 'http://test.com/',
        BASE_API: 'api/',
        LOADING_TEXT: 'loading...',
        TOKEN_NAME: 'TOKEN',
        HTTP_DEBUG: false,
        EXTRA_HEARDE: true,
	}


> 设置 `HTTP_DEBUG: true` 将启用mock 模拟数据源.下面会有简单介绍

> 设置 `EXTRA_HEARDE: true` 将添加额外的请求头 platform version token

> `APP_VERSION='1.0.0'` 当前版本号,用于请求头.默认调用手机方法获取版本号
                
    
### ReducersModule 的引入 
ReducersModule 是存储数据的仓库,用来存储数据
在app.module.ts imports内 引入ReducersModule
    
    const initialState: User = {
        TOKEN: '',
        userName: '',
        passWord: '',
        userMoney: 0.00,
        userSex: '0',
        userPic: '',
        realName: '保密'
    }

    const UserReducer = (state: User = initialState, action: Action) => {
        switch (action.type) {
                case USER_ACTION.STORAGE_USERINFO:
            return Object.assign({}, initialState, action.payload);
                case USER_ACTION.UPDATE_USERINFO:
            return Object.assign({}, state, action.payload);
                default:
            return state;
        }
    }
    const reducerArrays = {
        user:UserReducers 
    };
    ReducersModule.rootReducer(reducerArrays)

在额外的子模块内 添加reducer

    // reducer.ts
    import { USER_ACTION, User } from './../constants/index';
	import { Action } from '@ngapp/core';
	const user: User = new User(window.localStorage['userToken']);
	
	export function userReducer() {
	    return {
	        name:function(){
				return 'user';
			}, 
			reducer:function(state: User = user, action: Action) {
			    const result = action.payload as User;
			
			    switch (action.type) {
			        case USER_ACTION.STORAGE_USERINFO:
			            window.localStorage['userToken'] = result.userToken;
			            return Object.assign({}, user, result);
			        case USER_ACTION.UPDATE_USERINFO:
			            if (result.userToken)
			                window.localStorage['userToken'] = result.userToken;
			            return Object.assign({}, state, result);
			        case USER_ACTION.USER_LOGIN_OUT:
			            delete window.localStorage['userToken'];
			            return Object.assign({}, user);
			        default:
			            return state;
			    }
			}
	    };
	}
	
	//module.ts
    ReducersChildModule.child(userReducer);

> 简述 reducer 开发思路 
>
> 主要是三个东西组成,页面(`component`),服务(`service`),数据仓库(`reducer`).
 
> 请参考这个文章[Redux你的Angular 2应用--ngRx使用体验](http://www.jianshu.com/p/0deec21d728f)

### ChildMockHttpProvides mock模拟数据源

> 声明一个`class`继承`MockHttp`

	class UserMock extends MockHttp {

	  get(url, param): HttpCode {
		if(url == 'user/login'){
			return {'code':'200',message:'请求成功',success:true};
		}
	    return null;
	  }
	
	  post(url, param): HttpCode {
	    return null;
	  }
	}

	const function userMock(){
		return new UserMock();
	}

> 使用 `ChildMockHttpProvides.childMock(Function)`, 加入到 `providers` 里面

> 可以声明多个继承`MockHttp`的`class`,依次使用 `ChildMockHttpProvides` 加入

	 providers: [
	    ChildMockHttpProvides.childMock(userMock),
		...
	 ]

> 启用后 发送HTTP请求都将会走继承了 `MockHttp`且加入到 `providers` 的`class`. 直到有返回值,请求结束.

### AlertService

	1.alert 弹出框

	2.toast 吐司

### HttpService

	1.httpGet 

	2.httpPost 



